<?php
/*
 Theme Name:   Twenty sixteen Child
 Description:  Twenty Sixteen Child Theme
 Author:       Ongraph
 Template:     twentysixteen
 Text Domain:  twenty-sixteen-child
*/



/*
*  	@Pallet Possibilities:- 
*  	Redirect subscriber user on about-us page after login with wp-admin login form.
*  
*/

function admin_default_page() {
	if ( is_user_logged_in() ) {
		wp_get_current_user();
	}
		return '/about-us';
}
add_filter('login_redirect', 'admin_default_page');

/*
*  	@Pallet Possibilities:- 
*  	Add as current-menu-item class on specific "Left Side Menu" instead of all menu.
*  
*/

function special_nav_class($classes, $item){

    if(is_single() && $item->menu == 'Left Side Menu'){

        $classes[] = 'current-menu-item';

    }

    return $classes;
}

/*
*  	@Pallet Possibilities:- 
*  	Use nav menu walker class to add custom class on menu li.
*  
*/

class themeslug_walker_nav_menu extends Walker_Nav_Menu {
  

function start_lvl( &$output, $depth ) {
    // depth dependent classes
    $indent = ( $depth > 0  ? str_repeat( "\t", $depth ) : '' ); // code indent
    $display_depth = ( $depth + 1); // because it counts the first submenu as 0
    $classes = array(
        'sub-menu',
        ( $display_depth % 2  ? 'menu-odd' : 'menu-even' ),
        ( $display_depth >=2 ? 'sub-sub-menu' : '' ),
        'menu-depth-' . $display_depth
        );
    $class_names = implode( ' ', $classes );
  
    // build html
    $output .= "\n" . $indent . '<ul class="' . $class_names . '">' . "\n";
}
  
// add main/sub classes to li's and links
 function start_el( &$output, $item, $depth, $args ) {
    global $wp_query;
    $indent = ( $depth > 0 ? str_repeat( "\t", $depth ) : '' ); // code indent
  
    // depth dependent classes
    $depth_classes = array(
        ( $depth == 0 ? 'main-menu-item' : 'sub-menu-item' ),
        ( $depth >=2 ? 'sub-sub-menu-item' : '' ),
        ( $depth % 2 ? 'menu-item-odd' : 'menu-item-even' ),
        'menu-item-depth-' . $depth
    );
    $depth_class_names = esc_attr( implode( ' ', $depth_classes ) );
  
    // passed classes
    $classes = empty( $item->classes ) ? array() : (array) $item->classes;
    $class_names = esc_attr( implode( ' ', apply_filters( 'nav_menu_css_class', array_filter( $classes ), $item ) ) );
  
    // build html
    $output .= $indent . '<li id="nav-menu-item-'. $item->ID . '" class="' . $depth_class_names . ' ' . $class_names . '">';
  
    // link attributes
    $attributes  = ! empty( $item->attr_title ) ? ' title="'  . esc_attr( $item->attr_title ) .'"' : '';
    $attributes .= ! empty( $item->target )     ? ' target="' . esc_attr( $item->target     ) .'"' : '';
    $attributes .= ! empty( $item->xfn )        ? ' rel="'    . esc_attr( $item->xfn        ) .'"' : '';
    $attributes .= ! empty( $item->url )        ? ' href="'   . esc_attr( $item->url        ) .'"' : '';
    $attributes .= ' class="menu-link ' . ( $depth > 0 ? 'sub-menu-link' : 'main-menu-link' ) . '"';
  
    $item_output = sprintf( '%1$s<a%2$s>%3$s%4$s%5$s</a>%6$s',
        $args->before,
        $attributes,
        $args->link_before,
        apply_filters( 'the_title', $item->title, $item->ID ),
        $args->link_after,
        $args->after
    );
  
    // build html
    $output .= apply_filters( 'walker_nav_menu_start_el', $item_output, $item, $depth, $args );
	}
}

/*
*  	@Pallet Possibilities:- 
*  	Hide wordpress admin bar for Logged In user if user role is subscriber.
*  
*/

add_action('after_setup_theme', 'remove_admin_bar');
function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
	  	show_admin_bar(false);
	}
}


/*
*  	@Pallet Possibilities:- 
*  	This is a ajax hook for sale ad post to populate subcategories on selection of main category when user add a sale ad post.
*  
*/


add_action( 'wp_ajax_getsubcategories', 'getsubcategories' );
add_action( 'wp_ajax_nopriv_getsubcategories', 'getsubcategories' );

function getsubcategories(){
	$cat_id = $_POST['cat_id'];
	$sale_ad_child_categories = get_terms( 'sale_ad_taxonomy', array( 'hide_empty' => false, 'parent' => $cat_id, 'orderby' => 'term_id' ) );
	$options = '<option value="0">Choose sub category</p>';
	for ($i=0; $i < count($sale_ad_child_categories); $i++) { 
		$options .= '<option value='.$sale_ad_child_categories[$i]->term_id.' >'.$sale_ad_child_categories[$i]->name.' </option>';
	}
	echo $options;
	die;
}


/*
*  	@Pallet Possibilities:- 
*  	DeleteAd function is called when Logged In user want to delete post created by user.
*  
*/

add_action( 'wp_ajax_deleteAd', 'deleteAd' );
function deleteAd(){
	wp_delete_post( $_POST['id'] );	
}

/*
*  	@Pallet Possibilities:- 
*  	Delete image function is used to delete image added to user created post.
*  
*/

add_action( 'wp_ajax_deleteImg', 'deleteImg' );
function deleteImg(){
	$post_id = $_POST['post_id'];
	wp_delete_attachment( $_POST['attachment_id'],$force_delete );
	$image_attachments=get_attached_media( $type, $post_id );
	$html="";
	$i=1;
	foreach($image_attachments as $images){
        $html   .=	'<tr>
        				<td>Image'. $i. '</td>
	                	<td width="20px">&nbsp;</td>
	                	<td><input type="file" accept="image/*"  name="image[]"  />&nbsp;&nbsp;(Max size 32MB)</td>
	                	<td>'.wp_get_attachment_image( $images->ID,  $size = "thumbnail").'</td>';

	    $html   .=		'<td><a href="#" rel="'.$images->ID.'" onclick="ajaxDeleteImg('.$images->ID.','. $post_id.' );"><img src="'.get_template_directory_uri().'/images/delete.png"></a></td>
            		</tr>';
           		$i++;
           	}

        $current_attachment= count($image_attachments);
        if( $current_attachment < 3){

        	for($i=1;$i<=3-$current_attachment;$i++){
        		 $html   .=	'<tr>
        		 				<td>Image'. ($current_attachment+$i). '</td>
				                <td width="20px">&nbsp;</td>
				                <td><input type="file" accept="image/*"  name="image[]"  />&nbsp;&nbsp;(Max size 32MB)</td>
	               
            				</tr>';
        	}
        }
    echo $html;exit;
}







