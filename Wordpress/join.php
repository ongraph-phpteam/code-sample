<?php
/*
*	Template Name: Join New
*	Description: Join template contains new user Sign UP functionality for Pallet Possibilities
*/


$error_message = "";
//Check if join form was submitted and get all value filled by user on form.
if(isset($_POST['joinSubmitButton'])){ 
	$actual_captcha			= $_SESSION['captcha_code'];
	$inserted_captcha		= $_POST['captcha'];
	
	//	Check if user inserted captcha code is equal to actual captcha or not? If both captcha are equal then new user would be registered as a subscriber. 
	if($actual_captcha == $inserted_captcha){
		$first_name             = $_POST['first_name'];
		$last_name              = $_POST['last_name'];
		$user_email             = $_POST['email'];
		$user_pass              = $_POST['password'];
		$phone 		    		= $_POST['phone'];
		$company_name           = $_POST['company_name'];
		$fax                    = $_POST['fax'];
		$cellPhone              = $_POST['cellPhone'];
		$billing_address1       = $_POST['billing_address1'];
		$billing_address2       = $_POST['billing_address2'];
		$billing_country_id     = $_POST['billing_country_id'];
		$billing_state_id       = $_POST['billing_state_id'];
		$billing_city           = $_POST['billing_city'];
		$billing_zip           	= $_POST['billing_zip'];
		$shipping_company_name  = $_POST['shipping_company_name'];
		$shipping_address1      = $_POST['shipping_address1'];
		$shipping_address2      = $_POST['shipping_address2'];
		$shipping_country_id    = $_POST['shipping_country_id'];
		$shipping_state_id      = $_POST['shipping_state_id'];
		$shipping_city          = $_POST['shipping_city'];
		$shipping_zip           = $_POST['shipping_zip'];
		$shipping_contact_name  = $_POST['shipping_contact_name'];
		$shipping_phone         = $_POST['shipping_phone'];
		$shipping         		= $_POST['shipping'];


		$userdata = array(
			'user_login'  		=> $user_email,
			'first_name'    	=> $first_name,
			'last_name'   		=> $last_name,
			'user_email'  		=> $user_email,
			'user_pass'    		=> $user_pass,
			'role'    			=> 'subscriber'
		);
		$user_id = wp_insert_user( $userdata ) ;

		//Check last registered user id is number or not. If it is number then all meta information will be insert for new created user otherwise user will be remain on same join page in case of condition will be false.
		if(gettype($user_id) != 'integer'){
			$_SESSION['error'] = "Please use another username or email address to register";
			wp_redirect( site_url('/join/'));
			exit;
		}else{
			update_user_meta($user_id,'first_name',$first_name);
			update_user_meta($user_id,'last_name',$last_name);
			update_user_meta($user_id,'company_name',$company_name);
			update_user_meta($user_id,'fax',$fax);
			update_user_meta($user_id,'phone',$phone);
			update_user_meta($user_id,'cellPhone',$cellPhone);
			update_user_meta($user_id,'billing_address1',$billing_address1);
			update_user_meta($user_id,'billing_address2',$billing_address2);
			update_user_meta($user_id,'billing_country_id',$billing_country_id);
			update_user_meta($user_id,'billing_state_id',$billing_state_id);
			update_user_meta($user_id,'billing_city',$billing_city);
			update_user_meta($user_id,'billing_zip',$billing_zip);
			update_user_meta($user_id,'shipping_company_name',$shipping_company_name);
			update_user_meta($user_id,'shipping_address2',$shipping_address2);
			update_user_meta($user_id,'shipping_country_id',$shipping_country_id);
			update_user_meta($user_id,'shipping_state_id',$shipping_state_id);
			update_user_meta($user_id,'shipping_city',$shipping_city);
			update_user_meta($user_id,'shipping_zip',$shipping_zip);
			update_user_meta($user_id,'shipping_contact_name',$shipping_contact_name);
			update_user_meta($user_id,'shipping_phone',$shipping_phone);
			update_user_meta($user_id,'shipping',$shipping);

			//Make user successfully auto login after registration and redirect user on welcome page.
	        $creds = array();
	        $creds['user_login'] = $user_email;
	        $creds['user_password'] = $user_pass;
	        if ( !empty( $remember ) ){ 
	          $creds['remember'] = true;
	        }

	        $user = wp_signon( $creds, true );

	        $creds = array();
	        $creds['user_login'] = $user_email;
	        $creds['user_password'] = $user_pass;
	        $creds['remember'] = true;

	        $user = wp_signon( $creds, false );
	        if ( is_wp_error($user) ){
	        	echo $user->get_error_message();
	        }else{
	        	wp_redirect( site_url().'/welcome/' );
	        }
    	}
}else{
      	$error_message="Captcha does not match. Please try again";
  	}
}
?>

