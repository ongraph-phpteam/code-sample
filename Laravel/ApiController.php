<?php namespace App\Http\Controllers;

//Controller for all API based actions (ONEmotion app and Platform Integration)
//Contains testing routes for indoor localization

use Carbon\Carbon;
use Request;
use stdClass;
use App\User;
use App\AppToken;
use App\AppUser;
use App\ActivationPoint;
use App\ActivationPointLog;
use App\HeatmapCapture;
use App\HeatmapData;
use App\Functions;
use Agent;
use Auth;
use Genderize;
use App\SubUser;
use App\ApiLog;

class ApiController extends Controller {

	//SDK Token verification
	public function authToken(){

		try{
			$input  = Request::all();
			//check if valid sdk token
			$at = AppToken::where('api_key',$input['api_key'])->first();

			if(isset($at) && $at->valid){

				if($at->activated){
					if($at->app != $input['app']){ //verify whether already activated token corresponds to same app
						$apilog = new Apilog();
						$apilog->user_id = $at->user_id;
						$apilog->type = 'authtoken';
						$apilog->message = 'Duplicate API key in use';
						$apilog->details = 'Token: '.$input['api_key'].' --AppName: '.$input['app'];
						$apilog->save();
						return response()->json(array('success'=>false,'message'=>'Duplicate API key in use!'));
					}
				}
				elseif($input['app'] == ''){ //Already Activated token is invalid if app name is not sent
					$apilog = new Apilog();
					$apilog->user_id = $at->user_id;
					$apilog->type = 'authtoken';
					$apilog->message = 'Invalid App Name';
					$apilog->details = 'Token: '.$input['api_key'];
					$apilog->save();
					return response()->json(array('success'=>false,'message'=>'Invalid App'));
				}
				else{
					$at->app = $input['app'];
					$at->activated = true;
					$at->save();

					$apilog = new Apilog();
					$apilog->user_id = $at->user_id;
					$apilog->type = 'authtoken';
					$apilog->message = 'Successful Integration';
					$apilog->details = 'Token: '.$input['api_key'].' --AppName: '.$input['app'];
					$apilog->save();
				}
			}
			else{
				$apilog = new Apilog();
				$apilog->user_id = null;
				$apilog->type = 'authtoken';
				$apilog->message = 'API key is invalid';
				$apilog->details = 'Token: '.$input['api_key'].' --AppName: '.$input['app'];
				$apilog->save();
				return response()->json(array('success'=>false,'message'=>'API key is invalid!'));
			}

			return response()->json(array('success'=>true));
		}
		catch(\Exception $e){
			return response()->json(array('success'=>false,'message'=>'Internal Error'));
		}
	}

	//return app user info to oneMotion
	public function getInfo(){

		try{
			$input  = Request::all();
			//if deviceID is empty generate new unique one
			if(empty($input['deviceID'])){
				$check = true;
				while ($check != null) {
					$device_id = $this->generateRandomString();
					$check = AppUser::where('device_id','=',$device_id)->first();
				}
				$input['deviceID'] = $device_id;
			}

			$au = AppUser::where('device_id','=',$input['deviceID'])->first(array('id','first_name','last_name','email','gender','age_group_id'));
			//if new deviceID save user to database
			if(!isset($au)){
				$appuser = new AppUser();
				$appuser->device_id = $input['deviceID'];
				$appuser->device_platform = 'Mobile';
				$appuser->device_name = Agent::device();
				$appuser->device_os = Agent::platform();
				$appuser->device_os_version = Agent::version($appuser->device_os);
				$appuser->first_name = '';
				$appuser->last_name = '';
				$appuser->email = '';
				$appuser->gender = '';
				$appuser->save();

				$appuser->utm_param = 'au'.$appuser->id;
				$appuser->save();

				$au = new stdClass();
				$au->id = $appuser->id;
				$au->first_name = $appuser->first_name;
				$au->last_name = $appuser->last_name;
				$au->email = $appuser->email;
				$au->gender = $appuser->gender;
				$au->age_group_id = $appuser->age_group_id;

			}

			/* if(is_null($appuser))
				return response()->json(array('success'=>false,'message'=>'Invalid')); */

			return response()->json(array('success'=>true,'deviceID'=>$input['deviceID'],'data'=>$au));
		}
		catch(\Exception $e){
			return response()->json(array('success'=>false,'message'=>'Internal Error'));
		}
	}

	//save or update oneMotion user details
	public function setInfo(){

	  try{
			$input  = Request::all();

			$appuser = AppUser::where('device_id','=',$input['deviceID'])->first();

			if(is_null($appuser))
				return response()->json(array('success'=>false,'message'=>'Invalid'));

			$name = explode(" ", $input['name'], 2);
			$firstName = $name[0];
			if(sizeof($name) == 2)
				$lastName = $name[1];
			else
				$lastName = '';

			// if($appuser->first_name != $firstName){
			// 	$g = Genderize::name($firstName)->get();
			//
			// 	if(!is_null($g->result[0]->gender))
			// 		$input['gender'] = $g->result[0]->gender;
			//
			// }

			$appuser->first_name = $firstName;
			$appuser->last_name = $lastName;
			$appuser->email = $input['email'];

			if(array_key_exists('account', $input) )
				$appuser->account = $input['account'];
			if(array_key_exists('gender', $input) && $input['gender'] != ''){

				if(strtoupper($input['gender']) == 'M' || strtoupper($input['gender']) == 'MALE')
					$appuser->gender = 'M';
				elseif(strtoupper($input['gender']) == 'F' || strtoupper($input['gender']) == 'FEMALE')
					$appuser->gender = 'F';
				else
					$appuser->gender = '';
			}
			if(array_key_exists('age', $input) && $input['age'] != '-1'){   //currently used
				$appuser->age_group_id = $input['age'];
			}
			if(array_key_exists('age_group_id', $input) && $input['age_group_id'] != '-1'){
				$appuser->age_group_id = $input['age_group_id'];
			}
			if(!empty($appuser->gender) && !empty($appuser->age_group_id)){
				if($appuser->gender == 'M'){
					if($appuser->age_group_id < 4)
						$appuser->personality = 2;
					else
						$appuser->personality = 4;
				}
				elseif($appuser->gender == 'F'){
					if($appuser->age_group_id < 4)
						$appuser->personality = 1;
					else
						$appuser->personality = 3;
				}
			}
			$appuser->save();

			return response()->json(array('success'=>true,'userID'=>$appuser->id));
		}
		catch(\Exception $e){
			return response()->json(array('success'=>false,'message'=>'Internal Error'));
		}
	}

	//return app user interaction history
	public function history(){

		try{
			$input  = Request::all();

			$appuser = AppUser::where('device_id','=',$input['deviceID'])->first();

			if(is_null($appuser))
				return response()->json(array('success'=>false,'message'=>'Invalid'));

			$history = ActivationPointLog::where('appuser_id','=',$appuser->id)->with('activationPoints')->orderBy('created_at','DESC')->get(array('id','activation_point_id','type','function','value','created_at'));

			return response()->json(array('success'=>true,'data'=>$history));
		}
		catch(\Exception $e){
			return response()->json(array('success'=>false,'message'=>'Internal Error'));
		}
	}


//v1 API for platform integration
	public function editbeacon($beacon_id){

		try{
			$input  = Request::all();

			$apikey = $input['apikey'];
			$value = $input['value'];

			$user = User::where('api_key',$apikey)->first();

			if(isset($user)){
				$beacon = ActivationPoint::where('uid',$beacon_id)->where('user_id',$user->id)->first();

				if(!isset($beacon))
					return response()->json(array('success'=>false, 'message'=>'Beacon does not exist'));

				$beacon->value = $value;
				$beacon->shake_value = $value;
				$beacon->group_override = false;
				$beacon->save();

				return response()->json(array('success'=>true));
			}
			else{
				return response()->json(array('success'=>false, 'message'=>'Unauthorized'));
			}
		}
		catch(\Exception $e){
			return response()->json(array('success'=>false,'message'=>'Internal Error'));
		}

	}

	//API Zomongo Get Beacon Details
	public function beacondetails($beacon_id){

		try{
			$input  = Request::all();

			$apikey = $input['api_key'];

			$user = User::where('api_key',$apikey)->first();

			if(isset($user)){
				$beacon = ActivationPoint::where('serial_number',$beacon_id)->where('user_id',$user->id)->first();

				if(!isset($beacon))
					return response()->json(array('success'=>false, 'message'=>'Beacon does not exist'));

				$data = new stdClass();
				$data->tap_function = $beacon->functions->name;
				$data->tap_value = $beacon->value;
				$data->shake_function = $beacon->shakefunctions->name;
				$data->shake_value = $beacon->shake_value;
				$data->enter_function = $beacon->enterfunctions->name;
				$data->enter_value = $beacon->enter_value;
				$data->custom_enter_message = $beacon->enter_message;
				$data->exit_function = $beacon->exitfunctions->name;
				$data->exit_value = $beacon->exit_value;
				$data->custom_exit_message = $beacon->exit_message;
				$data->enabled = $beacon->active;
				$data->push_limit = $beacon->push_limit;
				$data->name = $beacon->name;
				$data->google_id = $beacon->google_id;
				$data->latitude = $beacon->latitude;
				$data->longitude = $beacon->longitude;
				$data->serial_number = $beacon->serial_number;

				$api_log = new ApiLog();
				$api_log->user_id = $user->id;
				$api_log->ap_id = $beacon->id;
				$api_log->type = 'beacondetails';
				$api_log->serial = $beacon->serial_number;
				$api_log->save();

				return response()->json(array('success'=>true,'data'=>$data));
			}
			else{
				return response()->json(array('success'=>false, 'message'=>'Unauthorized'));
			}
		}
		catch(\Exception $e){
			return response()->json(array('success'=>false,'message'=>'Internal Error'));
		}

	}

	//API Zomongo Update Beacon
	public function updatebeacon($beacon_id){

		try{
			$input  = Request::all();

			$apikey = $input['api_key'];

			$user = User::where('api_key',$apikey)->first();

			if(isset($user)){
				$beacon = ActivationPoint::where('serial_number',$beacon_id)->where('user_id',$user->id)->first();

				if(!isset($beacon))
					return response()->json(array('success'=>false, 'message'=>'Beacon does not exist'));

				if(array_key_exists('tap_function',$input)){
					$tf = Functions::where('name',$input['tap_function'])->first();
					if(isset($tf))
						$beacon->function_id = $tf->id;
				}
				if(array_key_exists('tap_value',$input))
					$beacon->value = $input['tap_value'];

				if(array_key_exists('shake_function',$input)){
					$sf = Functions::where('name',$input['shake_function'])->first();
					if(isset($sf))
						$beacon->shake_function_id = $sf->id;
				}
				if(array_key_exists('shake_value',$input))
					$beacon->shake_value = $input['shake_value'];

				if(array_key_exists('enter_function',$input)){
					$ef = Functions::where('name',$input['enter_function'])->first();
					if(isset($ef))
						$beacon->enter_function_id = $ef->id;
				}
				if(array_key_exists('enter_value',$input))
					$beacon->enter_value = $input['enter_value'];
				if(array_key_exists('custom_enter_message',$input))
					$beacon->enter_message = $input['custom_enter_message'];

				if(array_key_exists('exit_function',$input)){
					$exf =Functions::where('name',$input['exit_function'])->first();
					if(isset($exf))
						$beacon->exit_function_id = $exf->id;
				}
				if(array_key_exists('exit_value',$input))
					$beacon->exit_value = $input['exit_value'];
				if(array_key_exists('custom_exit_message',$input))
					$beacon->exit_message = $input['custom_exit_message'];

				if(array_key_exists('enabled',$input))
					$beacon->active = $input['enabled'];
				if(array_key_exists('push_limit',$input))
					$beacon->push_limit = $input['push_limit'];
				if(array_key_exists('name',$input))
					$beacon->name = $input['name'];
				if(array_key_exists('google_id',$input))
					$beacon->google_id = $input['google_id'];
				if(array_key_exists('latitude',$input))
					$beacon->latitude = $input['latitude'];
				if(array_key_exists('longitude',$input))
					$beacon->longitude = $input['longitude'];

				$beacon->save();

				$api_log = new ApiLog();
				$api_log->user_id = $user->id;
				$api_log->ap_id = $beacon->id;
				$api_log->type = 'updatebeacon';
				$api_log->serial = $beacon->serial_number;
				$api_log->save();

				return response()->json(array('success'=>true));
			}
			else{
				return response()->json(array('success'=>false, 'message'=>'Unauthorized'));
			}
		}
		catch(\Exception $e){
			//return response()->json(array('success'=>false,'message'=>$e->getMessage().$e->getLine()));
			return response()->json(array('success'=>false,'message'=>'Invalid input'));
		}

	}

	////Admin App APIs

	public function appauth(){

		$input  = Request::all();

		if($input['api_key'] == '0V3R-A1R-7247'){
			if(Auth::validate(array('username' => $input['email'], 'password' => $input['password'], 'active'=> 1,'deleted_at'=> null))) {
				$u = User::where('username',$input['email'])->first();

				if($u->id == 2){
					return response()->json(array('success'=>true, 'admin'=>true, 'user_id'=>$u->id));
				}
				else{
					return response()->json(array('success'=>true, 'admin'=>false, 'user_id'=>$u->id));
				}
			}
			else {
				return response()->json(array('success'=>false, 'message'=>'Unauthorized'));
			}
		}
		else {
			return response()->json(array('success'=>false, 'message'=>'Unauthorized'));
		}
	}

	public function adminBcnInfo(){

		$input  = Request::all();

		if($input['api_key'] == '0V3R-A1R-7247'){

			$user = User::find($input['user_id']);

			if(!isset($user))
				return response()->json(array('success'=>false, 'message'=>'Invalid account'));

			if($user->id == 2){
				$aps = ActivationPoint::where('type_id',2)->lists('uid')->all();
				$serials = ActivationPoint::where('type_id',2)->with('stats','last')->get(array('uid','serial_number','name'));
			}
			elseif($user->sub_users){
				$subusers = SubUser::where('user_id',$user->id)->lists('sub_user_id')->all();
				array_push($subusers, $user->id);
				$aps = ActivationPoint::whereIn('user_id',$subusers)->where('type_id',2)->lists('uid')->all();
				$serials = ActivationPoint::whereIn('user_id',$subusers)->where('type_id',2)->with('stats','last')->get(array('uid','serial_number','name'));
			}
			else{
				$aps = ActivationPoint::where('user_id',$input['user_id'])->where('type_id',2)->lists('uid')->all();
				$serials = ActivationPoint::where('user_id',$input['user_id'])->where('type_id',2)->with('stats','last')->get(array('id','uid','serial_number','name'));
			}

			return response()->json(array('success'=>true, 'beacons'=>$aps, 'serials'=>$serials));
		}
		else {
			return response()->json(array('success'=>false, 'message'=>'Unauthorized'));
		}

	}

	public function appVersion(){

		$input  = Request::all();

		if($input['app_name'] == 'ONEmotion Admin')
			if(version_compare($input['app_version'], env('ADMINAPP_V','1.0.0'),'<'))
				return response()->json(array('success'=>false, 'version'=>env('ADMINAPP_V')));

		return response()->json(array('success'=>true));
	}
	///////////


	function generateRandomString($length = 16) {
		$characters = 'ABCDFGHJKLMNPQRSTVWXYZ0123456789';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	function standard_deviation($sample){
		if(is_array($sample)){
			$mean = array_sum($sample) / count($sample);
			foreach($sample as $key => $num) $devs[$key] = pow($num - $mean, 2);
			return sqrt(array_sum($devs) / (count($devs) - 1));
		}
	}


	public function thresholds(){

		/* $input  = Request::all();

		switch ($input['sensorID']) {

			case 1: //accelerometer
				$threshold = 150;
				break;
			case 2: //gyroscope
				$threshold = 200;
				break;
			case 3: //magnetic
				$threshold = 300;
				break;
			case 4: //light
				$threshold = 400;
				break;


		} */

		//$threshold = array(1,0.38,60,400);

		$threshold = array([0.18,0.80,0.54],[0.09,0.04,0.01],60,400);

		$accelerometer = array('x'=>0.18,'y'=>0.80,'z'=>0.54);
		$gyroscope = array('x'=>0.09,'y'=>0.04,'z'=>0.01);
		$magnetic = 60;
		$light = 400;


		$threshold = array('accelerometer'=>$accelerometer,'gyroscope'=>$gyroscope,'magnetic'=>$magnetic,'light'=>$light);

		return response()->json(array('success'=>true,'threshold'=>$threshold));


	}

	public function capture(){

		$input  = Request::all();

		//input['captureValues']

		return response()->json(array('success'=>true));

	}




}