<?php namespace App\Http\Controllers;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Request;
use App\AppUser;
use App\ActivationPoint;
use App\ActivationPointLog;
use App\Personalization;
use DateTime;
use DateTimeZone;
use App\AgeGroup;
use App\Scheduler;
use GeoIP;
use Agent;
use Route;
use App\Functions;
use App\Group;
use URL;
use Session;
use Pusher;

class RoutingController extends Controller {

	//route for returning action value based on BLE/Wifi ID detected
	public function beaconAction(){

		$input  = Request::all();

		//get ONEmotion user
		$au = AppUser::where('device_id','=',$input['deviceID'])->first();

		if(!isset($au)){
			$response = array('success' => false, 'message' => 'No device found');
			return response()->json($response);
		}

		//check if POST params are sent
		if(array_key_exists('deviceOS',$input))
			$deviceOS = $input['deviceOS'];
		else
			$deviceOS = '0';

		if(array_key_exists('deviceOS_version',$input))
			$deviceOS_version = $input['deviceOS_version'];
		else
			$deviceOS_version = '';

		if(array_key_exists('deviceName',$input))
			$device_name = $input['deviceName'];
		else
			$device_name = '';

		if(array_key_exists('appName',$input))
			$app_name = $input['appName'];
		else
			$app_name = '';

		if(array_key_exists('appVersion',$input))
			$app_version = $input['appVersion'];
		else
			$app_version = '';

		if(array_key_exists('deviceCarrier',$input))
			$device_carrier = strtoupper($input['deviceCarrier']);
		else
			$device_carrier = '';

		if(array_key_exists('rssi',$input))
			$rssi = $input['rssi'];
		else
			$rssi = 0;

		if(array_key_exists('language',$input))
			$language = $input['language'];
		else
			$language = '';

		if(array_key_exists('languageCode',$input))
			$language_code = $input['languageCode'];
		else
			$language_code = '';

		if(array_key_exists('sdk',$input))
			$sdk = $input['sdk'];
		else
			$sdk = NULL;

		if(array_key_exists('timezone',$input)){
			$timezone = $input['timezone'];
		}
		else{
			$timezone = 'America/Toronto';
		}

		if(array_key_exists('timestamp',$input)){
			try {
				$dt = new DateTime('now', new DateTimeZone($timezone));//date("Y-m-d H:i:s",floatval($input['timestamp'])/1000);
				$dt->setTimestamp(floatval($input['timestamp'])/1000);
				$timestamp = $dt->format("Y-m-d H:i:s");
			}
			catch (\Exception $e){
				$timestamp = Carbon::now();
			}
		}
		else{
			$timestamp = Carbon::now();
		}

		if(array_key_exists('trigger',$input)){
			$trigger = strtolower($input['trigger']);
		}
		else{
			$trigger = '';
		}

		//use last entered beacon for exit trigger
		if($trigger=='exit'){
			$l = ActivationPointLog::where('appuser_id',$au->id)->where('trigger','enter')->orderBy('created_at', 'desc')->first();
			if(isset($l)){
				$ap = ActivationPoint::find($l->activation_point_id);
				if(!$ap->active){
					$response = array('success' => false, 'message' => '');
					return response()->json($response);
				}
			}
			else{
				$response = array('success' => false, 'message' => '');
				return response()->json($response);
			}
		}
		else{

			if(array_key_exists('beaconUUID',$input)){
				$uuid = $input['beaconUUID'];
				$ap = ActivationPoint::where('uid','=',$input['beaconID'])->where('uuid',$uuid)->first();
			}
			else{
				$ap = ActivationPoint::where('uid','=',$input['beaconID'])->first();
			}

			if(!isset($ap) || !$ap->active){
				$response = array('success' => false, 'message' => '');
				return response()->json($response);
			}
		}

		//update beacon battery level
		if(array_key_exists('battery',$input)){
			$ap->battery = $input['battery'];
			$ap->save();
		}

		//initialize variable
		$notif_body = $ap->notification_body;

		//set function / value based on trigger
		if($trigger=='shake' && !is_null($ap->shake_function_id)){
			$funct = $ap->shakefunctions->name;
			$value = $ap->shake_value;
		}
		elseif($trigger=='dwell' && !is_null($ap->dwell_function_id)){
			$funct = $ap->dwellfunctions->name;
			$value = $ap->dwell_value;
			$notif_body = $ap->dwell_message;
		}
		elseif($trigger=='enter'){
			if(!$ap->push || is_null($ap->enter_function_id))
				return response()->json(array('success' => false, 'message' => ''));
			if($ap->push_limit){
				$c = ActivationPointLog::where('appuser_id',$au->id)->where('activation_point_id',$ap->id)->where('trigger','enter')->where('created_at','>', Carbon::today())->count();
				if($c >= $ap->push_limit){
					return response()->json(array('success' => false, 'message' => ''));
				}
			}
			$funct = $ap->enterfunctions->name;
			$value = $ap->enter_value;
			$notif_body = $ap->enter_message;
		}
		elseif($trigger=='exit'){
			if(!$ap->push || is_null($ap->exit_function_id))
				return response()->json(array('success' => false, 'message' => ''));
			if($ap->push_limit){
				$c = ActivationPointLog::where('appuser_id',$au->id)->where('activation_point_id',$ap->id)->where('trigger','exit')->where('created_at','>', Carbon::today())->count();
				if($c >= $ap->push_limit){
					return response()->json(array('success' => false, 'message' => ''));
				}
			}

			$funct = $ap->exitfunctions->name;
			$value = $ap->exit_value;
			$notif_body = $ap->exit_message;
		}
		elseif($trigger=='seek' && !is_null($ap->shake_function_id)){
			$funct = $ap->shakefunctions->name;
			$value = $ap->shake_value;
		}
		else{
			$funct = $ap->functions->name;
			$value = $ap->value;
		}

		//check if age is set
		if(is_null($au->age_group_id)){
			$age = '';
		}
		else{
			$age = AgeGroup::find($au->age_group_id)->range;
		}

		//////////////MAKE SCHEDULE FUNCTION

		//check if belongs to schedule, find correct function value
		$sched = Scheduler::where('activation_point_id','=',$ap->id)->with('schedules')->get();
		$now = new DateTime();
		$schedAct = false;
		$sc;

		if(!$sched->isEmpty()){
			foreach($sched as $s){
				switch($s->schedules->repeat) {
					case "once":
						if(new DateTime($s->schedules->start) < $now && new DateTime($s->schedules->end) > $now){
							$schedAct = true;
							$sc = $s->schedules;
						}
						break;
					default:
						break;
				}
				if($schedAct){
					break;
				}
			}
		}

		if($schedAct){
			switch($trigger) {
				case "shake":
					if(!is_null($sc->shake_function_id)){
						$funct = $sc->shakefunction->name;
						$value = $sc->shake_value;
					}
					break;
				case "enter":
					if(!is_null($sc->enter_function_id)){
						$funct = $sc->enterfunction->name;
						$value = $sc->enter_value;
					}
					break;
				case "exit":
					if(!is_null($sc->exit_function_id)){
						$funct = $sc->exitfunction->name;
						$value = $sc->exit_value;
					}
					break;
				default:
					if(!is_null($sc->function_id)){
						$funct = $sc->functions->name;
						$value = $sc->value;
					}
					break;
			}
		}

		//////////////

		//update personalization screens
		if($funct == 'SAMSUNGDEMO'){
			//update samsung screen through socket channel push
			$data = array( 'first_name' => $au->first_name,'last_name' => $au->last_name,'email' => $au->email,'gender' => $au->gender,'age' => $age,'image' => $this->personalizeSamsung($au->personality, $device_name,'screen'),'trigger'=>$trigger,'device_name'=>$device_name,'lang'=>$language);

			$pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'));
			$pusher->trigger('samsung'.$ap->serial_number, 'user-shake', $data);
			//end pusher
			//if($trigger !='proximity')
				$value = $this->personalizeSamsung($au->personality,$device_name,'device');
		}
		else{
			//update screen through socket channel push
			$data = array( 'first_name' => $au->first_name,'last_name' => $au->last_name,'email' => $au->email,'gender' => $au->gender,'age' => $age,'image' => $this->personalize($au->personality,$ap->experience_id,'screen',1),'trigger'=>$trigger);

			$pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'));
			$pusher->trigger('screen'.$ap->serial_number, 'user-shake', $data);
			//end pusher
		}

		//if personalization function, call function to get personalized image
		if($funct == 'PERSONALIZATION'){
			//if($trigger !='proximity')
			$value = $this->personalize($au->personality,$ap->experience_id,'mobile',1);
		}

		//if using integration script to update screen
		if($ap->user->integration){
			//update screen through socket channel push
			$data = array( 'first_name' => $au->first_name,'last_name' => $au->last_name,'trigger'=>$trigger,'user_id'=>$au->device_id);

			$pusher = new Pusher(env('PUSHER_APP_KEY'), env('PUSHER_APP_SECRET'), env('PUSHER_APP_ID'));
			$pusher->trigger('oa-'.$ap->serial_number.$ap->user->utm_id, 'user-trigger', $data);
			//end pusher
		}

		//use GeoIP package to determine location
		$location = GeoIP::getLocation( Request::getClientIp() );
		if($location["postal_code"] == "06510"){ //check if unable to get correct location
			$location["country"] = "";
			$location["city"] = "";
			$location["state"] = "";
			$location["lat"] = 0;
			$location["lon"] = 0;
		}

		if($location["country"] == null){
			$location["country"] = "";
		}
		if($location["city"] == null){
			$location["city"] = "";
		}
		if($location["state"] == null){
			$location["state"] = "";
		}

		//check app version, prompt for update
		if($app_name=='ONEmotion' && $deviceOS == 'Android' && $app_version != '' && version_compare($app_version, env('ONEMOTION_V_ANDROID','1.0.0'),'<')){
			$funct = 'URL';
			$value = 'https://play.google.com/store/apps/details?id=ca.overair.onemotion2&hl=en';
		}
		elseif($app_name=='ONEmotion' && $deviceOS == 'iOS' && $app_version != '' && version_compare($app_version, env('ONEMOTION_V_IOS','1.0.0'),'<')){
			$funct = 'URL';
			$value = 'https://itunes.apple.com/ca/app/onemotion/id1087457008?mt=8';
		}

		//save app user device info
		$au->device_os = $deviceOS;
		$au->device_os_version = $deviceOS_version;
		$au->device_name = $device_name;
		$au->save();

		//log interaction
		$log = new ActivationPointLog();
		$log->user_id = $ap->user_id;
		$log->activation_point_id = $ap->id;
		$log->appuser_id = $au->id;
		$log->type = $ap->type->name;
		$log->function = $funct;
		$log->value = $value;
		$log->group_id = $ap->group_id;
		if($log->group_id)
			$log->group_name = 	$ap->group->group_name;
		$log->trigger = $trigger;
		$log->rssi = $rssi;
		$log->ip_address = Request::getClientIp();
		$log->latitude = $location["lat"];
		$log->longitude = $location["lon"];
		$log->country = $location["country"];
		$log->city = $location["city"];
		$log->state = $location["state"];
		$log->language = $language;
		$log->language_code = $language_code;
		$log->device_platform = 'Mobile';
		$log->device_os = $deviceOS;
		$log->device_os_version = $deviceOS_version;
		$log->device_name = $device_name;
		$log->device_carrier = $device_carrier;
		$log->sdk = $sdk;
		$log->app_name = $app_name;
		$log->app_version = $app_version;
		$log->timezone = $timezone;
		$log->timestamp = $timestamp;
		$log->save();

		//check if dwell enabled
		$dwell_time = 0;
		if($ap->dwell && !is_null($ap->dwell_function_id) && ($trigger != 'dwell' && $trigger !='exit')){
			$dc = ActivationPointLog::where('appuser_id',$au->id)->where('activation_point_id',$ap->id)->where('trigger','dwell')->where('created_at','>', Carbon::today())->count();
			if($dc == 0){
				$dwell_time = $ap->dwell_time*1000;
			}
		}

		$notification = ['icon'=>$ap->notification_icon,'title'=>$ap->notification_title,'text'=>$notif_body];


		//if function not App, return the URL with the hash for that beacon, else return the data for the app
		if($funct != "APP" && $funct != "NOTIFICATION"){
			$log->loaded = false;
			$log->save();
			return response()->json(array('success'=>true,'action'=>'URL','data'=>URL::to('/').'/ap/'.$ap->hash.'/'.$log->id,'notification'=>$notification,'dwell'=>$dwell_time,'dwell_range'=>$ap->dwell_range));
		}
		else{
			return response()->json(array('success'=>true,'action'=>$funct,'data'=>$value,'notification'=>$notification,'dwell'=>$dwell_time,'dwell_range'=>$ap->dwell_range));
		}

	}

	//main hash routing function
	public function hashRoute($hash) {
		$activationPoint = ActivationPoint::where('hash', '=', $hash)->first();

		//Invalid hash
		if(empty($activationPoint)) {
			return view("pageNotFound",array(), 404);
		}
		//Valid hash
		else {
			$log = new ActivationPointLog();
			$log->user_id = $activationPoint->user_id;
			$log->activation_point_id = $activationPoint->id;
			$log->ip_address = Request::getClientIp();
			$log->type = $activationPoint->type->name;
			$log->device_browser = Agent::browser();

			//Mobile
			if(Agent::isMobile()) {
				$log->device_platform = 'Mobile';
				if(Agent::platform() == "AndroidOS")
					$log->device_os = "Android";
				else
					$log->device_os = Agent::platform();
			}
			//PC
			else {
				$log->device_platform = 'Desktop';
				$log->device_os = Agent::platform().' '.Agent::version(Agent::platform());
			}

			//check if belongs to schedule, find correct function value
			$sched = Scheduler::where('activation_point_id','=',$activationPoint->id)->with('schedules')->get();
			$personalize = false;
			$now = new DateTime();
			$schedAct = false;
			$sc;

			if(!$sched->isEmpty()){
				foreach($sched as $s){
					switch($s->schedules->repeat) {
						case "once":
							if(new DateTime($s->schedules->start) < $now && new DateTime($s->schedules->end) > $now){
								$funct = $s->schedules->function_id;
								$value = $s->schedules->value;
								$data = $s->schedules->data;
								$schedAct = true;
								$sc = $s->schedules;
							}
							break;
						default:
							break;
					}
					if($schedAct){
						break;
					}
				}
			}

			if(!$schedAct){
				$funct = $activationPoint->function_id;
				$value = $activationPoint->value;
				$data = $activationPoint->data;
			}

			$log->group_id = $activationPoint->group_id;
			if($log->group_id)
				$log->group_name = 	$activationPoint->group->group_name;

			$props = Route::input('props');
		 	if(isset($props)) {
				//Ignore Logs as this is a preview
				if($props=='preview'){
						return redirect($this->addValueHeader($funct, $value));
				}
				elseif(is_numeric($props)){
					$log = ActivationPointLog::find(intval($props));
					if(is_null($log))
						return redirect('https://overair.ca');
					$trig = $log->trigger;
					$au = AppUser::find(intval($log->appuser_id));
					$log->loaded = true;
					$log->device_browser = Agent::browser();


					$funct = Functions::where('name',$log->function)->first()->id;
					$value = $log->value;
					if($funct == 8){
						//if($trig != 'proximity')
							$personalize= true;
					}
					if($funct == 9)	{
						$personalize= true;
					}

				}
			}

			//set utm params for google analytics tracking
			$utm_source = $activationPoint->type->name;
			$utm_campaign = $activationPoint->user->utm_id;

			if(isset($au)){
				$utm_term = $au->utm_param;
				$utm_params = "?utm_source=$utm_source&utm_medium=$activationPoint->id&utm_campaign=$utm_campaign&utm_content=$value&utm_term=$utm_term";
			}
			else{
				$utm_params = "?utm_source=$utm_source&utm_medium=$activationPoint->id&utm_campaign=$utm_campaign&utm_content=$value";

				//if NFC get location
				$location = GeoIP::getLocation( Request::getClientIp() );
				if($location["postal_code"] == "06510"){ //check if unable to get correct location
					$location["country"] = "";
					$location["city"] = "";
					$location["state"] = "";
					$location["lat"] = 0;
					$location["lon"] = 0;
				}
				if($location["country"] == null){
					$location["country"] = "";
				}
				if($location["city"] == null){
					$location["city"] = "";
				}
				if($location["state"] == null){
					$location["state"] = "";
				}
				$log->latitude = $location["lat"];
				$log->longitude = $location["lon"];
				$log->country = $location["country"];
				$log->city = $location["city"];
				$log->state = $location["state"];
			}

			$log->function = Functions::find($funct)->name;
			$log->value = $value;

			$log->save();

			if(($log->user_id ==26) && $funct == 2){ //26
				$funct = 0;
				return redirect(URL::to('/').'/load'.$utm_params)->with('url',$this->addValueHeader($funct, $value))->with('funct',$funct)->with('data',$data);
			}
			if($funct == 10){
				$log->value = $activationPoint->name;
				$log->save();

				$data = $activationPoint->id;
				return redirect(URL::to('/').'/load'.$utm_params)->with('url',$this->addValueHeader($funct, $value))->with('funct',$funct)->with('data',$data);
			}
			if($personalize){
				if($funct == 8){

					if($activationPoint->experience_id != null){
						$receipt = $activationPoint->experience->receipt;
						$brand = $activationPoint->experience->brand;
					}
					else{
						$receipt = false;
						$brand = '';
					}

					return redirect(URL::to('/').'/personalize'.$utm_params)->with('image',$value)->with('name',$au->first_name)->with('utm',$au->utm_param)->with('receipt',$receipt)->with('brand',$brand);

				}
				elseif($funct == 9)
					return redirect(URL::to('/').'/personalize'.$utm_params)->with('image',$value)->with('name',$au->first_name)->with('utm',$au->utm_param)->with('brand','samsung');
			}

			return redirect(URL::to('/').'/load'.$utm_params)->with('url',$this->addValueHeader($funct, $value))->with('funct',$funct);

		}
	}

	public function showLoad() {
		return view('redirect');
	}

	public function showVid() {

		$ap_id = Route::input('ap');

		if(isset($ap_id)) {

			$ap = ActivationPoint::where('id',$ap_id)->where('function_id',10)->first();

			if(isset($ap))
				return view('video')->with('vid',$ap->value)->with('link',$ap->data)->with('group',$ap->group_id);
		}

		return redirect('http://bestbuy.ca');
		//return view('video')->with('vid','https://storage.bhs1.cloud.ovh.net/v1/AUTH_2f5df0562e134bb3a635322c8ea809cf/Cpress%20Videos/01-BTS_BBY_Hero-Pitch-Acer-R5.mp4')->with('link','http://bestbuy.ca')->with('id',9460); //change
	}

	public function vidCount(){

		$input  = Request::all();

		$group = Group::find($input['group']);
		$group->exit_value = intval($group->exit_value)+1;
		$group->save();

		return response()->json(array('success'=>true));

	}

	public function showPersonalize() {
		Session::reflash();
		return view('personalize')->with('image',Session::get('image'))->with('name',Session::get('name'));
	}

	public function showReceipt() {

		if(Session::has('utm')){
			$au = AppUser::where('utm_param',Session::get('utm'))->first();

			if(Session::has('brand') && Session::get('brand') == 'pepsi')
				$image = 'https://overair.ca/demo/images/pepsi_page2.jpg';
			else
				$image = 'https://overair.ca/demo/images/coke_page2.jpg';

			return view('receipt')->with('email',$au->email)->with('name',$au->first_name)->with('image',$image);
		}
		else
			return view('receipt')->with('email','')->with('name','')->with('image','https://overair.ca/demo/images/pepsi_page2.jpg');

	}

	public function personalize($p_id,$exp_id,$disp,$user_id){

		//in future filter Experience by user_id first
		$v = Personalization::where('experience_id',$exp_id)->where('personality',$p_id)->where('display',$disp)->first();

		if(isset($v))
			return $v->value;
		else{
			if($disp == 'screen')
				return "https://www.overair.ca/demo/images/coke_background.jpg";
			else
				return "https://www.overair.ca/demo/images/coke_personality.jpg";
		}

	}

	//samsung demo personalization
	public function personalizeSamsung($pid,$device,$type){
		if($type == 'screen'){
			switch ($pid) {
				case 1:
					if($device == 'Galaxy Note5')	//new device
						return "https://www.overair.ca/demo/images/samsung/samsung_background_new_yf.jpg";
					elseif(strpos($device, 'Galaxy') !== FALSE || strpos($device, 'Samsung') !== FALSE)	//older device
						return "https://www.overair.ca/demo/images/samsung/samsung_background_old_yf.jpg";
					else	//competitor
						return "https://www.overair.ca/demo/images/samsung/samsung_background_competitor_yf.jpg";
					break;
				case 2:
					if($device == 'Galaxy Note5')	//new device
						return "https://www.overair.ca/demo/images/samsung/samsung_background_new_ym.jpg";
					elseif(strpos($device, 'Galaxy') !== FALSE || strpos($device, 'Samsung') !== FALSE)	//older device
						return "https://www.overair.ca/demo/images/samsung/samsung_background_old_ym.jpg";
					else	//competitor
						return "https://www.overair.ca/demo/images/samsung/samsung_background_competitor_ym.jpg";
					break;
				case 3:
					if($device == 'Galaxy Note5')	//new device
						return "https://www.overair.ca/demo/images/samsung/samsung_background_new_mf.jpg";
					elseif(strpos($device, 'Galaxy') !== FALSE || strpos($device, 'Samsung') !== FALSE)	//older device
						return "https://www.overair.ca/demo/images/samsung/samsung_background_old_mf.jpg";
					else	//competitor
						return "https://www.overair.ca/demo/images/samsung/samsung_background_competitor_mf.jpg";
					break;
				case 4:
					if($device == 'Galaxy Note5')	//new device
						return "https://www.overair.ca/demo/images/samsung/samsung_background_new_mm.jpg";
					elseif(strpos($device, 'Galaxy') !== FALSE || strpos($device, 'Samsung') !== FALSE)	//older device
						return "https://www.overair.ca/demo/images/samsung/samsung_background_old_mm.jpg";
					else	//competitor
						return "https://www.overair.ca/demo/images/samsung/samsung_background_competitor_mm.jpg";
					break;
				default:
					return "https://www.overair.ca/demo/images/samsung_background_old_ym.jpg";
					break;
			}
		}
		elseif($type == 'device'){
			switch ($pid) {
				case 1:
					if($device == 'Galaxy Note5')	//new device
						return "https://www.overair.ca/demo/images/samsung/background1.jpg";
					elseif(strpos($device, 'Galaxy') !== FALSE || strpos($device, 'Samsung') !== FALSE)	//older device
						return "https://www.overair.ca/demo/images/samsung/background2.jpg";
					else	//competitor
						return "https://www.overair.ca/demo/images/samsung/background3.jpg";
					break;
				case 2:
					if($device == 'Galaxy Note5')	//new device
						return "https://www.overair.ca/demo/images/samsung/background4.jpg";
					elseif(strpos($device, 'Galaxy') !== FALSE || strpos($device, 'Samsung') !== FALSE)	//older device
						return "https://www.overair.ca/demo/images/samsung/background5.jpg";
					else	//competitor
						return "https://www.overair.ca/demo/images/samsung/background6.jpg";
					break;
				case 3:
					if($device == 'Galaxy Note5')	//new device
						return "https://www.overair.ca/demo/images/samsung/background7.jpg";
					elseif(strpos($device, 'Galaxy') !== FALSE || strpos($device, 'Samsung') !== FALSE)	//older device
						return "https://www.overair.ca/demo/images/samsung/background8.jpg";
					else	//competitor
						return "https://www.overair.ca/demo/images/samsung/background9.jpg";
					break;
				case 4:
					if($device == 'Galaxy Note5')	//new device
						return "https://www.overair.ca/demo/images/samsung/background10.jpg";
					elseif(strpos($device, 'Galaxy') !== FALSE || strpos($device, 'Samsung') !== FALSE)	//older device
						return "https://www.overair.ca/demo/images/samsung/background11.jpg";
					else	//competitor
						return "https://www.overair.ca/demo/images/samsung/background12.jpg";
					break;
				default:
					return "https://www.overair.ca/demo/images/samsung/background5.jpg";
					break;
			}
		}
	}

	public function loadSMS($url,$data) {
		return redirect(URL::to('/').'/app/sms')->with('url',$url)->with('data',$data);

	}

	public function showSMS() {
		return view('sendSMS');
	}

	//add appropriate header to value based on function
	public function addValueHeader($funct,$value){

		switch($funct) {
			case 1:
				return $value; //'http://'.$value;
				break;
			case 2:
				return 'sms:'.$value;
				break;
			case 3:
				return 'tel:'.$value;
				break;
			case 4:
				return 'mailto:'.$value;
				break;
			default:
				return $value;
				break;
		}

	}

	//old screen
	public function personalizedScreen($serial){

		/* $beacon = ActivationPoint::where('serial_number',$serial)->where('user_id',Auth::id())->first();

		if(is_null($beacon))
			return redirect('https://overair.ca');

		return view('personalizedscreen')->with('serial',$serial); */

		return redirect('/s/'.$serial); //redirect to new screen
	}

	public function genderize(){
		$client = new GuzzleHttp\Client();
		$request = $client->createRequest('GET', 'https://api.genderize.io/?name=jad');

		$response = $client->send($request);

		dd($response->json());
	}

}
