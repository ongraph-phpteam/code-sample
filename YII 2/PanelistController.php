<?php

namespace api\modules\v1\controllers;

use Yii;
use yii\rest\ActiveController;
use common\models\panel\Panel;
use common\models\panelist\Panelist;
use common\models\panelist\DefaultAnswers;
use common\models\panelist\Panelistprofile;
use common\models\panelist\PanelistSearch;
use common\models\panelist\PanelistGetCountV1;
use common\models\panelist\PanelistGetCountV2;
use common\models\panelist\PanelistGetCountV3;
use common\models\panelist\PanelistGetCountV4;
use common\models\panelist\PanelistTransaction;
use common\models\panelist\Panelistmeta;
use common\models\coreg\Coreg;
use common\models\importpanelist\ImportPanelist;
use common\models\panelist\PanelistLoginAttempts;
use common\models\card\Card;
use common\models\survey\SurveyActivity;
use common\models\survey\SurveyActivitySearch;
use api\modules\v1\components\Controller;
use common\models\Countries;
use common\models\base\BaseModel;
use linslin\yii2\curl;
use common\models\dailystreak\DailyStreakDetails;


/**
 * Panelist Controller API
 * Models used : Panelist.php, Panelistaddress.php, Panelistmeta.php, Panelistname.php under panels/common
 */
class PanelistController extends Controller
{

    public $modelClass = 'common\models\panelist\Panelist';

    /* Declare actions supported by APIs (Added in api/modules/v1/components/controller.php too) */
    public function actions(){
        $actions = parent::actions();
        unset($actions['create']);
        unset($actions['update']);
        unset($actions['delete']);
        unset($actions['view']);
        unset($actions['index']);
        return $actions;
    }

    /* Declare methods supported by APIs */
    protected function verbs(){
        return [
            'create' => ['POST'],
            'update' => ['PUT', 'PATCH','POST'],
            'delete' => ['DELETE'],
            'view' => ['GET'],
            'index'=>['GET'],
        ];
    }

    /**
     * Create Panelist API
     * Panelists will create under panels
     * panel_id : indicates to Panel ID
     * @Method : POST
     * @Required Fields: panel_id, password, email, gender, dob, phone, first, last, street, city, country, state, zip
     * @Status set to Active(1) by default
     * @email_verified set to email_not_verified(0) by default when panelist creates
     * @Local URL : http://localhost/panels/api/web/v1/panelists
     */
    public function actionCreate(){
        if(Yii::$app->user->can('create-panelist')){
            $panelist = new Panelist;
            $post = Yii::$app->request->post();
            if(Yii::$app->request->post('google_id') || Yii::$app->request->post('facebook_id')){
                $panelist->scenario = 'social-login'; 
            }
            else{
                $panelist->scenario = 'panelist'; 
            }
            // Setting post data to model
            $panelist->setAttr($post);

            if($panelist->validate()){
                if ($panelist = $panelist->create()) {
        
                    // Status 201 is for Created
                    Yii::$app->response->statusCode = 201;
                    return ['panelist'=>$panelist->response];
                }
            }
            else{
                // Return all errors
                return $panelist;
            }
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

     /**
     * Delete Panelist API
     * @Method : DELETE
     * @Required Fields: id
     * @Status set to InActive(-1) on Delete
     * @Local URL : http://localhost/panels/api/web/{panel-token}/v1/panelists/{id}
     */
    public function actionDelete($id){
        if(Yii::$app->user->can('delete-panelist')){
            $panelist_id = $id; 
            $panelist = Panelist::findOne(['id'=> (int)$panelist_id,'status'=>['$ne' => Panelist::STATUS_DELETED]]);
            if(!empty($panelist)){
                if($panelist->google_id || $panelist->facebook_id){
                    $panelist->scenario = 'social-login'; 
                } else if($panelist->source == 'coreg' || $panelist->source =='invite' || $panelist->source == 'import') {
                    $panelist->scenario = 'sourceuser-delete'; 
                } 
                else{
                    $panelist->scenario = 'panelist';
                }
                $panelist->deletePanelist(); // call deletePanelist() to inactive the status of panelist
                //$panelist->delete();// for delete
                Yii::$app->response->statusCode = 204; // Status 204 is for No Content
            }
            else
                Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

     /**
     * Update Panelist API
     * @$id indicates Panelist ID
     * @Method : PUT or PATCH
     * @STATUS : Active (st_act) and Inactive (st_slp)
     * @Local URL : http://localhost/panels/api/{panel-token}/web/v1/panelists/{id}
     */
    public function actionUpdate($id){
        if(Yii::$app->user->can('update-panelist')){
            $panelist_id = $id; 
            $panelist = Panelist::findOne(['id'=> (int)$panelist_id,'status'=>['$ne' => Panelist::STATUS_DELETED]]);
            if(!empty($panelist)){
                if($panelist->google_id || $panelist->facebook_id){
                    $panelist->scenario = 'social-login-update'; 
                }
                else{
                    $panelist->scenario = 'panelist';
                }
                $post = Yii::$app->request->post();

                // Setting post data to model
                $panelist->setAttr($post);

                /* In case of activation mail,we get source from profile page and on basis of that we mark user as verified. This code is only for activation mail. */
                $postdata['source'] = "";
                if(isset($post['source']) && $post['source'] == "verifyemail"){
                    $postdata['source'] = $post['source'];
                }
                if($panelist->validate()){
                    if ($panelist = $panelist->updatePanelist($postdata)) {  // call updatePanel() to update panelist info
                        $panelist->saveDefaultAnswers();
                        return ['panelist'=>$panelist->response];
                    }
                }
                else{
                        // Return all errors
                        return $panelist;
                }
            }
            else
                Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * Fetch all panelist with pagesize and page 
     * API : http://localhost/panels/api/web/v1/panelist/{offset}/{limit}
     * Method : GET
     * @return array all panels
     * Fields:
     * pagesize
     * page
     */
    public function actionIndex(){
        if(Yii::$app->user->can('index-panelist')){

            if(Yii::$app->request->isGet){

                $model = new PanelistSearch();
                $requestData = Yii::$app->request->get();
                $attributes = array_keys($model->attributes);

                foreach($requestData as $key=>$value){
                    if(in_array($key, $attributes)){
                        $queryParams['PanelistSearch'][$key] = $value;
                    }
                    else{
                        $queryParams[$key] = $value;
                    }
                }  
                $threshold = isset(Yii::$app->request->queryParams['pagesize'])?Yii::$app->request->queryParams['pagesize']:10;
                
                if((isset($queryParams['type']) && $queryParams['type'] == 'csv' && $threshold <= Yii::$app->params['csvThreshold']) || (!isset($queryParams['type']))){

                    $dataProvider = $model->search($queryParams);
                    if(isset(Yii::$app->request->queryParams['pagesize'])){
                        $dataProvider->pagination->pageSize = Yii::$app->request->queryParams['pagesize'];
                    }
                    $data = $dataProvider->getModels();
                    if(!empty($data)){
                        foreach ($data as $key => $value) {
                            $response[] = $value->response;
                        }
                        Yii::$app->response->statusCode = 200;
                        return [
                            'totalCount' => $dataProvider->pagination->totalCount,
                            'panelists' => $response
                        ]; 
                    }else{
                        Yii::$app->response->statusCode = 404;
                        return[
                            'Not Found'
                        ];
                    }
                }else{
                    if(Yii::$app->params['csvMaxThreshold'] > $threshold){
                        $newJob = $model->createNewJob($queryParams);
                        if($newJob){
                            Yii::$app->response->statusCode = 200;
                            return [
                                'message' => 'Request has been processed. Email will be sent shortly.'
                            ]; 
                        }else{
                            Yii::$app->response->statusCode = 503;
                            return [
                                'message' => 'Some Error Occurred. Please try again!'
                            ];
                        }
                    }else{
                        return [
                            'message' => 'Your request contains data more than 100K panelist. Please ask tech team for this data.'
                        ];
                    }
                    
                }

            }else{
                Yii::$app->response->statusCode = 405; 
            }
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * $id is Panelist Id
     * API : http://localhost/panels/api/web/v1/panelist/{panel_id}
     * Method : GET
     */
    public function actionView($id){
        if(Yii::$app->user->can('view-panelist')){

            if(BaseModel::isValidMongoId($id)) {
                // This is mongo id 
                $enc_pid = new \MongoId($id);
                $panelist = Panelist::findOne(['_id'=> $enc_pid,'status'=>['$ne' => Panelist::STATUS_DELETED]]);
            } else {

                // We need to remove this check after panel id support from admin side
                if(preg_match("/^[0-9]*$/",$id)) {
                    $panelist_id = (int)$id;
                } else {
                    $panelist_id = $id;
                }
                $panelist = Panelist::findOne(['id'=> $panelist_id,'status'=>['$ne' => Panelist::STATUS_DELETED]]);
                if(empty($panelist)) {
                    Yii::$app->response->statusCode = 204;
                    Yii::$app->end();
                }
                // END We need to remove this check after panel id support from admin side

            }
            
            // We are showing only associate panelist with panel_token.
            if(empty($panelist) || ($panelist->panel->panel_token!=Panel::getPanelToken() && !Panel::isAdmin())){
                Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
                Yii::$app->end();
            }
            if(!empty($panelist)){
                return ['panelist'=>$panelist->response];
            }
            else{
                Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
            }
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * Adding answers to a panelist
     * If there is nothing in post request then simply return question and panelist answer json.
     * Required fields : question_id, category_id
     * API : http://localhost/panels/api/web/{panel_token}/v1/panelists/{panelist_id}/answers
     * Method : POST
     */
    public function actionAddanswer($id){
        if(Yii::$app->user->can('add-answer')){
            
            /**
             * Here we validate id and mongoId.
            */
            if(isset($id) && Panelist::isValidMongoId($id)){
                $panelist_id = new \MongoId($id);
                $panelist = Panelist::findOne(['_id'=> $panelist_id,'status'=>['$ne' => Panelist::STATUS_DELETED]]);
            }
            else{
                $panelist_id = isset($id) && preg_match("/^[0-9]*$/",$id)?(int)$id:$id;
                $panelist = Panelist::findOne(['id'=> $panelist_id,'status'=>['$ne' => Panelist::STATUS_DELETED]]);
            }

            if(!empty($panelist) && isset($panelist->panel) && $panelist->validatePanelist($panelist)){
                $panelist->scenario = 'addAnswer';   
                if(empty($panelist->panel->panel_questions)){
                    $panelist->addError('Answers','Panel "'.$panelist->panel->name.'" doesn\'t have any question associate to it.');
                    return $panelist;
                    Yii::app()->end();
                }
                if($post = Yii::$app->request->post()){
                    $profile = $panelist->profile;
                    $profile['answers']['category_id'] = isset($post['question']['category_id'])?$post['question']['category_id']:null;
                    $profile['answers']['question_id'] = isset($post['question']['question_id'])?$post['question']['question_id']:null;
                    $profile['answers']['options'] = isset($post['question']['options'])?$post['question']['options']:null;
                    $profile['answers']['question_key'] = isset($post['question']['question_key'])?$post['question']['question_key']:null;
                    $profile['answers']['question_text'] = isset($post['question']['question_text'])?$post['question']['question_text']:null;
                    $profile['answers']['question_type'] = isset($post['question']['question_type'])?$post['question']['question_type']:null;
                    $profile['answers']['category_text'] = isset($post['question']['category_text'])?$post['question']['category_text']:null;
                    
                    $panelist->profile = $profile;
                    if($panelist->validate() && $panelist->addAnswer()){
                        // Status 201 is for created
                        Yii::$app->response->statusCode = 201;
                        return $panelist->sendQuestionAnswers();
                    }
                    else
                        return $panelist; // This will return validation errors if any.
                }

                Yii::$app->response->statusCode = 200;
                return $panelist->sendQuestionAnswers();
            }
            Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * Check email if exists
     * API : http://localhost/panels/api/web/v1/panelists/email/{email}
     * Method : GET
     */
    public function actionCheckemail($email){
        if(Yii::$app->user->can('checkemail-panelist')){
            $like = isset(Yii::$app->request->queryParams['like'])?Yii::$app->request->queryParams['like']:false;
            $model = new Panelist;

            // If like is true then get panelist by like 
            if($like){
                $panelist = $model->loginPanelistByEmail($email);
            }
            else{
                //Get panelist by (email=email)
                $panelist = $model->getPanelistByEmail($email);
            }

            if($panelist){
                return ['panelist'=>$panelist->response];
                Yii::$app->response->statusCode = 200;
            }
            // else if($this->checkInPC1($email,'','/getUserByEmail')){
            //     Yii::$app->response->statusCode = 200;
            // }
            else{
                Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
                $model->addError('email',"This email is not registered with us.");
                return $model;
                Yii::$app->end();
            }
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * Check email if exists
     * API : http://localhost/panels/api/web/v1/panelists/forgotpassword
     * Method : POST
     */
    public function actionForgotpassword(){
        if(Yii::$app->user->can('forgotpassword-panelist')){
            $email = Yii::$app->request->post('email');
            $model = new Panelist;
            $panelist = $model->getPanelistByEmail($email);

            if(!$panelist){

                // Checking email in PC1.0
                // if($response = $this->checkInPC1($email,'','/forgotPassword')){
                //     $panel = Panel::getPanelByToken();

                //     // User object will be used in forgotpassword email template
                //     $user = new \stdClass();
                //     $user->email = $email;
                //     $user->profile = [
                //                     'name'=>[
                //                         'first'=>$response['first_name'],
                //                         'last'=>$response['last_name'],
                //                     ]
                //         ];
                //     $user->panel = (object)['base_domain'=>$panel->base_domain];
                //     $user->auth_key = '';
                    
                //     $randomPassword = $response['password'];

                //     $username = '';
                //     if(isset($response['first_name']) || isset($response['last_name'])){
                //         $username = $response['first_name']." ".$response['last_name'];
                //     }

                //     // Sending Mail
                //     \Yii::$app->mailer->compose(['html' => 'newPassword-html'], ['user' => $user,'password'=>$randomPassword])
                //     ->setFrom([\Yii::$app->params['supportEmail'] => $panel->name])
                //     ->setTo([$email=>$username])
                //     ->setSubject('Password reset for ' . $panel->name)
                //     ->send();

                //     return ['success'=>'New password is sent to your registered email.'];
                // }
                // else{
                    Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
                    $model->addError('email',"This email is not registered with us.");
                    return $model;
                    Yii::$app->end();
                // }
            }
            // If panelist account is Inactive(status => 0) showing message to user
            if($panelist->status==Panelist::STATUS_INACTIVE) {
                Yii::$app->response->statusCode = 422;
                $model->addError('email',"Account is inactive, please request support team to activate your account.");
                return $model;
            } else {

                if($panelist->sendNewPasswordMail()){
                    return ['success'=>'New password is sent to your registered email.'];
                }
                else{
                    Yii::$app->response->statusCode = 503; // Service Unavailable.
                    return [
                        'error'=>error_get_last(),
                        'message'=>'Mail not sent.'
                    ];
                }
            }
             
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * Login API
     * URL : http://localhost/panels/api/web/{panel_token}/v1/panelists/login
     * Required fields:
     * Email, Password
     *
     */
    public function actionLogin(){

        if(Yii::$app->user->can('login-panelist')){

            $model = new Panelist();
            $email = Yii::$app->request->post('email');
            $password = Yii::$app->request->post('password');

            $auth_key = Yii::$app->request->post('auth_key');
            $source = Yii::$app->request->post('source');
            $hoa_id = Yii::$app->request->post('hoa_id');

            $login_ip = Yii::$app->request->post('login_ip');
            $panelist = $model->loginPanelistByEmail($email);

            $user_device = Yii::$app->request->post('user_device');

            if($panelist){ 
                if($panelist->status!=Panelist::STATUS_INACTIVE && ($panelist->auth_key==$auth_key || ($panelist->password && $password && $model->validatePassword($password)))){
                    /**
                     * If source is from email
                     * that means panelist came from email verification link
                     */


                    /**
                     * We send source equals to Email verified or Email Not Verified 
                     * only in case of if coming source = verifyemail and step value is greater than zero.
                     */
                    
                        if(!$panelist->panelistEmailVerified && $source=="verifyemail"){
                            if($panelist->step > 0){
                                $panelist->setEmailVerified();
                                $source = "Email Verified";
                            }
                            else{
                                 $source = "";
                            }
                        }
                        else{
                            if($source=="verifyemail"){
                                $source = "Email Already Verified";
                            }
                            else{
                                $source = "";  
                            }
                        }
                    $panelist = $model->afterLogin(($login_ip)?$login_ip:"",$user_device);
                    Yii::$app->response->statusCode = 200; 
                    return [
                        'panelist'=>[
                            'id'=>$panelist->id,
                            'first_name'=>$panelist->panelistFirstName,
                            'last_name'=>$panelist->panelistLastName,
                            'email'=>$panelist->email,
                            'step'=>($panelist->step)?$panelist->step:"",
                            'lastaction'=>$panelist->panelistLastActionAt,
                            'lastvisit'=>$panelist->panelistLastVisitAt,
                            'avatar'=>$panelist->panelistAvatar,
                            'auth_key'=>$panelist->auth_key,
                            'source'=>$source,
                            'user_device'=>$panelist->panelistUserDevice
                        ]
                    ];
                }
                else{
                    $model->addError('password','Invalid email or password');
                    return $model;
                    Yii::$app->end();
                }
            }
            else{

                switch ($source) {
                    // If user comes from Old site by Email Invitation
                    // We are direclty log in those users on clicking link
                    // case 'invite':
                    //     $response = $this->checkInPC1($email,'','/getUserByEmail');
                        
                    //     if(isset($response['auth_key']) && $response['auth_key'] == $auth_key){
                            
                    //         $model->auth_key = $auth_key;
                    //         if((int)$response['aid'] == 38 || (int)$response['aid'] == 62){

                    //             $response['offer_id'] = Yii::$app->params['coreg_offer_id']; // get coreg offer id.
                    //             $response['hoa_id'] = $model->genrateHasofferId($response['offer_id'],$response['aid']);
                    //         }
                    //         else{
                    //             $response['aid'] = "";
                    //             $response['hoa_id'] = "";
                    //             $response['aff_sub'] = "";
                    //         }

                    //     }
                    //     break;
                    // If user comes from external sites
                    // We are direclty log in those users on clicking link
                    case 'coreg':
                        $coreg = Coreg::find()->where(['email'=>$email,'status'=>Coreg::STATUS_PENDING])->One();
                        if($coreg && $coreg->auth_key == $auth_key){
                            $response['first_name'] = $coreg->firstname;
                            $response['last_name'] = $coreg->lastname;
                            $response['email'] = $coreg->email;
                            $response['aid'] = $coreg->aid;
                            $response['hoa_id'] = $hoa_id;
                            $response['offer_id'] = $coreg->offer_id;
                            $response['gender'] = $coreg->gender;
                            $response['dob'] = $coreg->dob;
                            $response['zipcode'] = $coreg->zipcode;
                            $response['source'] = 'coreg';
                            // Fetch country name from country collection from shortcode
                            $countryName = Countries::find()->select(["_id"])->where(['ShortCode'=>$coreg->countrycode])->One();
                            if(isset($countryName) && !empty($countryName)){
                                $response['country'] = $countryName['_id'];
                            } else {
                                $response['country'] = "United States";
                            }

                            $model->auth_key = $coreg->auth_key;
                            $response['email_verified'] = Panelistmeta::EMAIL_NOT_VERIFIED;

                        }
                        break;
                    // If we import users from excel sheet
                    // We are direclty log in those users on clicking link
                    case 'import':
                        
                        $importpanelist = ImportPanelist::find()->where(['email'=>$email,'status'=>ImportPanelist::STATUS_PENDING])->One();
                        if($importpanelist && $importpanelist->auth_key == $auth_key){
                            $response['first_name'] = $importpanelist->firstname;
                            $response['last_name'] = $importpanelist->lastname;
                            $response['email'] = $importpanelist->email;
                            $response['gender'] = $importpanelist->gender;
                            $response['dob'] = $importpanelist->dob;
                            $response['zipcode'] = $importpanelist->zipcode;
                            $response['country'] = $importpanelist->country;
                            $response['hoa_id'] = $hoa_id;
                            $response['aid'] = $importpanelist->aid;
                            $response['offer_id'] = $importpanelist->offer_id;
                            $model->auth_key = $importpanelist->auth_key;
                            $response['email_verified'] = Panelistmeta::EMAIL_NOT_VERIFIED;
                            /* if user successfully moved to panelist collection we mark 
                            importpanelist status = 1 */
                            $importpanelist->status = ImportPanelist::STATUS_TRANSFERRED;
                            $importpanelist->save();
                        }
                        break;
                    // We are checking user's email and password in PC1.0 if its preset then 
                    // fetch all details from their and create a panelist in PC1.5
                    // default:
                    //     if($response = $this->checkInPC1($email,$password)){

                    //         $model->setPassword($password);
                    //         $model->generatePasswordResetToken();
                    //         $model->generateAuthKey();
                    //         $source = 'old_pc';
                    //         if((int)$response['aid'] == 38 || (int)$response['aid'] == 62){

                    //             $response['offer_id'] = Yii::$app->params['coreg_offer_id']; // get coreg offer id.
                    //             $response['hoa_id'] = $model->genrateHasofferId($response['offer_id'],$response['aid']);
                    //         }
                    //         else{
                    //             $response['aid'] = "";
                    //             $response['hoa_id'] = "";
                    //             $response['aff_sub'] = "";
                    //         }
                    //     }
                    //     break;
                }
                if(isset($response) && !empty($response)){
                    $model->setAttr($response);
                    $model->step = Panelist::DEFAULT_STEP;
                    $model->status = Panelist::STATUS_ACTIVE;
                    $model->source = $source;
                    $model->setMeta();
                    $meta = $model->meta;
                    $meta['email_verified'] = $response['email_verified'];
                    $meta['registration_ip'] = $login_ip;
                    $model->meta = $meta;

                    // 'False' - we are saving panelist without any validation
                    // because after login this type of panelist will be redirected to profile page where 
                    // panelist need to enter its full profiling information

                    $model->save(false);
                     /**
                       * Delete user from coreg after moving to the panelist collection
                       */
                      if($source=="coreg" && !empty($coreg)){
                         $coreg->delete();
                     }

                    PanelistLoginAttempts::saveLoginAttempts($model->_id,"");

                    if(!$model->panelistEmailVerified && $source=="invite"){
                        $model->setEmailVerified();
                    }

                    // If email is not verified and source is not in coreg and import then send verification mail
                    if(!$model->panelistEmailVerified && ($source!="coreg" && $source!="import")){
                        $model->sendVerificationMail(); 
                    }

                    
                    if($source=="coreg"){
                        // Registration bonus
                        $transaction = new PanelistTransaction;
                        $data['panel_id'] = $model->panel->id;
                        $data['type'] = 'Signup Bonus';
                        $data['message'] = 'Point Bonus on registration';
                        $data['identifier'] = $model->generateRandomString(32);
                        $data['transaction_status'] = 'Complete';
                        $data['points'] = $model->panel->signup_bonus;
                        $transaction->createTransaction($model, $data);

                        // Email verification pixel will fire after profile registration only for coreg                      
                    }
                    else if($source == "import"){
                        $transaction = new PanelistTransaction;
                        $data['panel_id'] = $model->panel->id;
                        $data['type'] = 'Signup Bonus';
                        $data['message'] = 'Point Bonus on registration';
                        $data['identifier'] = $model->generateRandomString(32);
                        $data['transaction_status'] = 'Complete';
                        $data['points'] = $model->panel->signup_bonus;
                        $transaction->createTransaction($model, $data);
                    }
                    // Rewarding old site points who are coming direclty
                    else{
                        $transaction = new PanelistTransaction;
                        $data['panel_id'] = $model->panel->id;
                        $data['type'] = 'Reward';
                        $data['message'] = 'Balance Transfer from old Website';
                        $data['identifier'] = $model->generateRandomString(32);
                        $data['transaction_status'] = 'Complete';
                        $data['points'] = (int)$response['points'];
                        $transaction->createTransaction($model, $data);
                    }
                }
                else {
                    $model->addError('password','Invalid email or password');
                    return $model;
                    Yii::$app->end();
                }
            }
            //set response data if not empty
            return ['panelist'=>$model->response]; 
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }


    // public function checkInPC1($email, $password=null, $api=""){
        
    //     $header = [
    //             Yii::$app->params['pointclub']['usersAPIAccess'],
    //             'Content-Type: application/json'
    //     ];
       
    //     $curl = new curl\Curl();
    //     $curl->setOption(CURLOPT_RETURNTRANSFER,true);
    //     $curl->setOption(
    //                     CURLOPT_POSTFIELDS,
    //                     json_encode(['email'=>$email,'password'=>$password]));
    //     $curl->setOption(CURLOPT_HTTPHEADER, $header);
    //     $response = $curl->post(\Yii::$app->params['pointclub']['api'].'/users'.$api,false);
    //     // $response = $curl->post(\Yii::$app->params['pointclub']['api'].'/users'.$api,false);
        
    //     if($curl->responseCode == 200){
    //      Yii::trace("Pointclub OldUser API: For ".$api." "."Api Response ".$curl->responseCode." "."for ".json_encode(['email'=>$email,'password'=>$password]),'external_api');
    //         return $response;
    //     }
    //     Yii::trace("Pointclub OldUser API: Fails for ".$api." "."Api Response ".$curl->responseCode." "."for ".json_encode(['email'=>$email,'password'=>$password]),'external_api');
    //     return false;
    // }

    /**
     * Reset Password API
     * URL : http://localhost/panels/api/web/{panel_token}/v1/panelists/resetpassword
     * Required fields:
     * Email, Password
     *
     */
    public function actionResetpassword(){
        if(Yii::$app->user->can('resetpassword-panelist')){
            $model = new Panelist;
            $email = Yii::$app->request->post('email');
            $old_password = Yii::$app->request->post('old_password');
            $new_password = Yii::$app->request->post('new_password');
            /* get forgot link in post only when user redirect from forgot link */
            $forgot_link = Yii::$app->request->post('forgot_link');
            $panelist = $model->getPanelistByEmail($email);
            $panelist->scenario = 'panelist'; 
            // if panelist redirect from forgot link we check forgot link ==1 or not
            if(!($forgot_link == 1)){
                if(!$old_password ){
                    $model->addError('old_password','Old password is required');
                    return $model;
                    Yii::$app->end();
                }
            }
            if(!$new_password ){
                $model->addError('new_password','New password is required');
                return $model;
                Yii::$app->end();
            }
            if($panelist){
                // if old password is not validate
                if($old_password && !$model->validatePassword($old_password)){
                    $model->addError('old_password','Please enter a valid old password');
                    return $model;
                    Yii::$app->end();
                }
                if($panelist->updatePassword($new_password)){
                    return [
                            'success'=>"Panelist's password has been updated!"
                    ];
                }
                else{
                    Yii::$app->response->statusCode = 422; // Unprocessable entity
                    return $panelist;
                } 
            }else{
                Yii::$app->response->statusCode = 404; 
            }
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * Get question from innovate admin called by panel's panelist to submit its answer
     * URL : http://localhost/panels/api/web/{panel_token}/v1/panelists/2/question
     *
     */
    public function actionGetquestion(){
        if(Yii::$app->user->can('question-panelist')){
            $panelist_id = $id;

            $panelist = Panelist::findOne(['id'=> (int)$panelist_id,'status'=>['$ne' => Panelist::STATUS_DELETED]]);
            if(!empty($panelist) && isset($panelist->panel) && $panelist->validatePanelist($panelist)){
                $question = $panelist->getNextQuestionId();            
                $question_API_URL = Yii::$app->params['innovateAdmin']['api_url'];

                //API call to get question by id POST request to Innovate Admin
                $curl = new curl\Curl();
                $response = $curl->setOption(
                        CURLOPT_POSTFIELDS,
                        http_build_query(['id'=>$question['question_id']])
                )->post($question_API_URL."/question",false);

                if($curl->responseCode==200){
                    if($response['totalcount']>0){
                        $question = $response['question'][0];

                        // Collect all options in a question
                        $options = [];
                        foreach ($question['QuestionOptions'] as $key => $value) {
                                $options[$key]['option_text'] = $value['OptionText'];
                                $options[$key]['option_id'] = $value['_id'];
                        }

                        // return question details to panel
                        return  [
                                'question'=>[
                                    'category_id'=>$question['Category'],
                                    'text'=>$question['QuestionText'],
                                    'type'=>$question['QuestionType'],
                                    'id'=>$question['id'],
                                    'key'=>$question['QuestionKey'],
                                    'options'=>$options,
                                ]
                        ];
                    }
                }
                else{
                    Yii::$app->response->statusCode = $curl->responseCode;
                    Yii::$app->end();
                }
            }

            Yii::$app->response->statusCode = 404;
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * **DEPRECATED** New Version --> actionGetCount()
     * This API will search all panelist from their qualified answers.
     * In post request we are accepting panelist answers.
     * URL : http://localhost/panels/api/web/{panel_token}/v1/panelists/qualified
     */
    public function actionSearchQualified(){
        if(Yii::$app->user->can('search-panelist')){
                if($post = Yii::$app->request->post()){
                    // After testing remove this log //
                    Yii::trace("SearchQualified API : Post Data of user ".json_encode($post),'external_api');
                    $searchModel = new PanelistSearch();
                    $attributes = array_keys($searchModel->attributes);
                    foreach($post as $key=>$value){
                        if(in_array($key, $attributes)){
                            $queryParams['PanelistSearch'][$key] = $value;
                        }
                        else{
                            $queryParams[$key] = $value;
                        }
                    }

                    $dataProvider = $searchModel->searchQualified($queryParams);
                    
                    if(isset(Yii::$app->request->queryParams['pagesize'])){
                        $dataProvider->pagination->pageSize=Yii::$app->request->queryParams['pagesize'];
                    }
                    
                    // If we have request for only count than return total count
                    if(Yii::$app->request->post('showCountOnly')==true){
                        
                        // Get total count directly
                        $totalCount = $dataProvider->getTotalCount();

                        return ['totalCount'=>$totalCount];
                    }

                    // All Panelist data
                    $panelists = $dataProvider->getModels();
                    $totalCount = $dataProvider->pagination->totalCount;

                    // We are showing only associate panelist with panel_token.
                    foreach ($panelists as $key => $panelist) {
                        if($panelist->panel->panel_token!=Panel::getPanelToken() && !Panel::isAdmin()){
                            unset($panelists[$key]);
                            $totalCount--;
                        }
                    }
                    if(!empty($panelists)){
                        $allPanelist = array();
                        foreach ($panelists as $key => $value) {
                                $allPanelist[] = $value->response;

                        }
                        return [
                                'totalCount'=>$totalCount,
                                'panelists'=>$allPanelist
                                ];
                    }
                    else
                        Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
                }

                Yii::$app->response->statusCode = 404;
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    public function actionGetCount(){
        if(Yii::$app->user->can('get-count-panelist')){
            if($post = Yii::$app->request->post()){

                /**
                 * Normal find queries for GetCount version-3.
                 */
            
                if(Yii::$app->request->post('api')==3){
                    $panelistGetCount = new PanelistGetCountV3;

                    /**
                     * Setting post data to model
                     */
                    $panelistGetCount->setAttr(Yii::$app->request->post());
                    if(!$panelistGetCount->validate()){
                        return $panelistGetCount;
                    }

                    $attributes = array_keys($panelistGetCount->attributes);
                    foreach($post as $key=>$value){
                        if(in_array($key, $attributes)){
                            $queryParams['PanelistGetCountV3'][$key] = $value;
                        }
                        else{
                            $queryParams[$key] = $value;
                        }
                    }

                    if(empty($panelistGetCount->criteria)){
                        $dataProvider =  $panelistGetCount->searchQualified($queryParams);
                        if($response = $panelistGetCount->setResponse($dataProvider,'fullMatch')){
                            return $response;
                        }

                        return $panelistGetCount->response;
                    }

                    foreach ($panelistGetCount->criteria as $criteria => $true) {
                        if($true){
                            $panelistGetCount->criteria = [$criteria=>true];
                            $dataProvider =  $panelistGetCount->searchQualified($queryParams);
                            $panelistGetCount->setResponse($dataProvider, $criteria);
                        }
                    }

                    if(isset($panelistGetCount->criteria['partialMatch']) && $panelistGetCount->criteria['partialMatch']===true){
                        if(empty($panelistGetCount->fullMatch)){
                            $panelistGetCount->criteria = ['fullMatch'=>true];
                            $fullMatch = $panelistGetCount->searchQualified($queryParams);
                            $panelistGetCount->setResponse($fullMatch, 'fullMatch');
                        }
                        if(empty($panelistGetCount->noDataMatch)){
                            $panelistGetCount->criteria = ['noDataMatch'=>true];
                            $noDataMatch = $panelistGetCount->searchQualified($queryParams);
                            $panelistGetCount->setResponse($noDataMatch,'noDataMatch');
                        }
                    }

                    return $panelistGetCount->response;
                }
                /**
                 * Normal find query for GetCount version-4.
                 */
                else if(Yii::$app->request->post('api') == 4){
                    $panelistGetCount = new PanelistGetCountV4;

                    /**
                     * Setting post data to model
                     */
                    
                    $panelistGetCount->setAttr(Yii::$app->request->post());
                    

                    if(!$panelistGetCount->validate()){
                        return $panelistGetCount;
                    }

                    $attributes = array_keys($panelistGetCount->attributes);
                    foreach($post as $key=>$value){
                        if(in_array($key, $attributes)){
                            $queryParams['PanelistGetCountV4'][$key] = $value;
                        }
                        else{
                            $queryParams[$key] = $value;
                        }
                    }
                    
                    $panelistGetCount->modifyPostData();
                    $panelistGetCount->searchQualified($queryParams);
                    
                    if(empty($panelistGetCount->criteria)){
                        $dataProvider =  $panelistGetCount->searchQualified($queryParams);
                        if($response = $panelistGetCount->setResponse($dataProvider,'fullMatch')){
                            return $response;
                        }

                        return $panelistGetCount->response;
                    }

                    if(isset($panelistGetCount->criteria['partialMatch']) && $panelistGetCount->criteria['partialMatch']===true){
                        if(empty($panelistGetCount->fullMatch)){
                            $panelistGetCount->criteria = ['fullMatch'=>true];
                            $fullMatch = $panelistGetCount->searchQualified($queryParams);
                            $panelistGetCount->setResponse($fullMatch, 'fullMatch');
                        }
                        if(empty($panelistGetCount->noDataMatch)){
                            $panelistGetCount->criteria = ['noDataMatch'=>true];
                            $noDataMatch = $panelistGetCount->searchQualified($queryParams);
                            $panelistGetCount->setResponse($noDataMatch,'noDataMatch');
                        }
                        /**
                         * Here we are doing sum of count of size fullMatch, noMatch and post pageSize 
                         * to mange partial match pageSize issue.Basically the issue due to subtracting 
                         * fullMatch and noMatch record with partialMatch.
                         * For Example: Suppose
                         *     No of user with full match criteria = 2
                         *     No of user with partial Match criteria = 4
                         *     And passed pageSize byn post payload = 3 then data provider will get
                         *     three record according to pageSize.
                         *     According to code when we subtract fullMatch with partial match we get 
                         *     only one record instead of getting three record ,still we have partial 
                         *     match record in database
                         * 
                         */
                        if(Yii::$app->request->post('pageSize')){
                           $panelistGetCount->oldPageSize = Yii::$app->request->post('pageSize');
                           $panelistGetCount->pageSize = count($panelistGetCount->fullMatch['panelists']) + count($panelistGetCount->noDataMatch['panelists']) + $panelistGetCount->pageSize; 
                        }
                        $panelistGetCount->criteria = Yii::$app->request->post('criteria');
                    }
                    
                    foreach ($panelistGetCount->criteria as $criteria => $true) {
                        if($true){
                            $panelistGetCount->criteria = [$criteria=>true];
                            $dataProvider =  $panelistGetCount->searchQualified($queryParams);
                            $panelistGetCount->setResponse($dataProvider, $criteria);
                        }
                    }   



                    return $panelistGetCount->response;
                }
                else {

                    /**
                     * Old Queries by Aggregation
                     */
                    if(Yii::$app->request->post('api')==2){
                        $panelistGetCount = new PanelistGetCountV2;
                    }
                    else{
                        $panelistGetCount = new PanelistGetCountV1;
                    }

                    /**
                     * Setting post data to model
                     */
                    $panelistGetCount->setAttr($post);
                    
                    if($panelistGetCount->validate()){
                        if(isset($post['criteria']) && isset($post['criteria']['default']) && $post['criteria']['default'] == true){
                            return $panelistGetCount->getDefault();
                        }
                        else{
                            foreach ($panelistGetCount->criteria as $key => $value) {
                                if(in_array($key, ["fullMatch", "noDataMatch"]) && $value == true){
                                    $panelistGetCount->{"set".ucfirst($key)}();
                                }
                            }
                            if(isset($panelistGetCount->criteria["partialMatch"]) && $panelistGetCount->criteria["partialMatch"]==true){
                                $panelistGetCount->setPartialMatch();
                            }
                        }
                        
                        return $panelistGetCount->response;
                    }
                    else{
                        /**
                         * Send 422 Validation error
                         */
                        return $panelistGetCount;
                    }
                    
                }
            }
            else{
               
                /**
                 * Bad Request
                 */
                Yii::$app->response->statusCode = 400;
            }
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    
    /**
     * This API first validate email and authkey then verify panelist email.
     * In post request we are accepting panelist answers.
     * GET URL http://localhost/panels/api/web/{panel_token}/v1/panelists/verifyemail/{email}/{auth}
     */
    public function actionVerifyEmail($email, $auth){
        if(Yii::$app->user->can('verify-email-panelist')){
            $panelist = Panelist::findOne(['email'=>$email,'status'=>['$ne' => Panelist::STATUS_DELETED]]);

            $transaction_ip = Yii::$app->request->get('transaction_ip');
            
            // Validate panelist and its auth key
            if($panelist && $panelist->validateAuthKey($auth)){
                $panelist->transaction_ip = $transaction_ip;
                // Check if this panelist's email is already verified or not
                if(!$panelist->panelistEmailVerified){
                
                    // Change email verify status to 1
                    $panelist->setEmailVerified();
                    Yii::$app->response->statusCode = 200; // Ok
                    return [
                        'success'=>'true',
                        'message'=>'Your email address has been successfully verified.'
                    ];
                }
                else{
                    Yii::$app->response->statusCode = 400; // Bad request
                    return [
                        'error'=>'true',
                        'message'=>'Email is already verified.'
                    ];
                }
            }
            else{
                Yii::$app->response->statusCode = 404; // Not Found
                return [
                    'error'=>'true',
                    'message'=>'Panelist not found'
                ];
            }
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * This API records transaction details of the panelists.
     * 
     * POST URL http://localhost/panels/api/web/{panel_token}/v1/panelists/{panelist_id}/transaction
     */
    public function actionTransaction($id){
        if(Yii::$app->user->can('transaction-panelist')){
            $model = new Panelist;
            $panelist = $model->getPanelistById();

            if($panelist){
                $transaction = new PanelistTransaction;
                $post = Yii::$app->request->post();
                return $transaction->createTransaction($panelist,$post);
            }
            else{
                Yii::$app->response->statusCode = 404; // Not Found
                return [
                    'error'=>'true',
                    'message'=>'Panelist not found'
                ];
            }
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * This API fetch all transactions of a panelist
     * 
     * GET URL http://localhost/panels/api/web/{panel_token}/v1/panelists/{panelist_id}/transactions
     */
    public function actionViewAllTransactions($id){
        if(Yii::$app->user->can('view-all-transaction-panelist')){

            if(isset($id) && Panelist::isValidMongoId($id)){
                $panelist_id = new \MongoId($id);
                
            }
            else{
                $panelist_id = isset($id) && preg_match("/^[0-9]*$/",$id)?(int)$id:$id;
                
            }
            
            $model = new Panelist;
            $panelist = $model->getPanelistById($panelist_id);
            
            if($panelist && $panelist->matchPanelistWithPanelToken()){
                $transaction = new PanelistTransaction;

                $queryParams = array();
                $get = Yii::$app->request->queryParams;

                $attributes = array_keys($transaction->attributes);
                foreach($get as $key=>$value){
                    if(in_array($key, $attributes)){
                        $queryParams['PanelistTransaction'][$key] = $value;
                    }
                    else{
                        $queryParams[$key] = $value;
                    }
                }
                $queryParams['PanelistTransaction']['panelist_id'] = $panelist->_id;
               
                $dataProvider = $transaction->search($queryParams);
            
                if(isset(Yii::$app->request->queryParams['pagesize'])){
                    $dataProvider->pagination->pageSize=Yii::$app->request->queryParams['pagesize'];
                }

                // All Transaction data
                $transactions = $dataProvider->getModels();
                $totalCount = $dataProvider->pagination->totalCount;
                
                if(!empty($transactions)){
                    $allTsxn = array();
                    foreach ($transactions as $key => $value) {
                            $allTsxn[] = $value->response;
                    }
                    return [
                            'totalCount'=>$totalCount,
                            'transactions'=>$allTsxn
                        ];
                }
            }

            Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * 
     * 
     * POST URL http://localhost/panels/api/web/{panel_token}/v1/panelists/defaultanswers
     */
    public function actionDefaultAnswers(){
        if(Yii::$app->user->can('default-answers-panelist')){
            $defaultAnswers = new DefaultAnswers;
            $defaultAnswers->question_key = Yii::$app->request->post('question_key');
            $defaultAnswers->answer_field = Yii::$app->request->post('answer_field');
            $defaultAnswers->panel_id = Yii::$app->request->post('panel_id');
            if($defaultAnswers->validate() && $defaultAnswers->save()){
                return $defaultAnswers;
            }
            return $defaultAnswers; // return validation errors
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }


    /**
     * [actionAddCard description]
     * @param  int $id Panelist id
     * @return array     API response
     * To add card POST URL http://localhost/panels/api/web/{panel_token}/v1/panelists/{panelist_id}/card
     * To change card PUT URL http://localhost/panels/api/web/{panel_token}/v1/panelists/{panelist_id}/card
     * POST request-
     * sku
     */
    public function actionAddCard($id){
       if(Yii::$app->user->can('add-card-panelist')){
            $sku = Yii::$app->request->post('sku');
            $price = Yii::$app->request->post('price');
            $panelist = (new Panelist)->getPanelistById();
            if($panelist  && $sku && $price){
                $panelist->scenario = 'card';
                $card = Card::getCardRewardBySKUandPrice($sku ,$price);
                if($card){
                    $cardDetails = $card->response;
                    if(!isset($panelist->card)){
                        $cardDetails['created_at'] = new \MongoDate();
                        $cardDetails['updated_at'] = new \MongoDate();

                    }
                    else{
                        $cardDetails['created_at'] = $panelist->card['created_at']; 
                        $cardDetails['updated_at'] = new \MongoDate();
                    }   
                    $panelist->card = $cardDetails;
                    if(isset($panelist->card['rewards'][0]['static_price']) && $panelist->card['rewards'][0]['static_price']==0 && $price){
                        // change multiple price value to one price value i.e searched price in get parameter
                        $card = $panelist->card;
                        $card['rewards'][0]['price'] = (int)$price;
                        $panelist->card = $card;
                    }
                    $panelist->save();
                    Yii::$app->response->statusCode = 201; // Created
                    return [$panelist->card];
                }
            }
            Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }


    /**
     * [actionViewCard description]
     * @param  int $id Panelist id
     * @return array     API response
     */
    public function actionViewCard($id){
        if(Yii::$app->user->can('view-card-panelist')){
            $panelist = (new Panelist)->getPanelistById();
            if($panelist){
                Yii::$app->response->statusCode = 200; // Created
                return $panelist->card;
            }
            Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * [actionGetSurvey survey API to return all survey for panelist]
     * @param  [type] $id [panelist id]
     * @return [array]     [surveys]\
     * URL http://localhost/panels/api/web/{panel_token}/v1/panelists/{panelist_id}/surveys
     */
    public function actionGetSurvey($id){
        if(Yii::$app->user->can('get-survey-panelist')){
            $panelist = (new Panelist)->getPanelistById();
            $getCountry = Yii::$app->request->get('country');
            $userDevice = Yii::$app->request->get('devicetype');

            if($panelist && isset($panelist->profile['answers'])){
                // If survey disabled for any panelist then give empty array in survey index
                if($panelist->status_survey == Panelist::STATUS_SURVEY_INACTIVE){
                    return ['surveys'=>[]];
                }                
                $panelistAnswers = $panelist->profile['answers'];

                // Collection panelist answers for appending as a query string in innovate survey url
                $queryString = [];
                $postData = [];
                foreach($panelistAnswers as $value){
                    /**
                     * If multiple answer question
                     * Multipuch question
                     */
                   	
               		if(count($value['options'])>1){
                        $optionIds = [];
                        foreach ($value['options'] as $optionKey => $optionValue) {
                            $optionIds[] = $optionValue['option_id'];
                        }
                        $answer = $optionIds;
                    }
                    /**
                     * Single Puch question
                     */
                    else if(isset($value['options'][0]['option_id']) && $value['options'][0]['option_id']!=""){
                        $answer = $value['options'][0]['option_id'];
                    }
                    /**
                     * Open Ended question
                     */
                    else{
                        $answer = $value['options'][0]['option_text'];
                    }

                    // Post data for get survey api
                    $postData[$value['question_key']] = $answer;
                    
               	}
                
                // $postData['PID'] = $panelist->id;
                $postData['PID'] = $panelist->_id->{'$id'};
                $postData['panel_id'] = $panelist->panel->id;
                // send user device type in post data
                if(isset($userDevice) && !empty($userDevice)){
                    $postData['deviceType'] = $userDevice;
                }else{
                    $postData['deviceType'] = "";
                }
                //set country for 
                if(isset($getCountry) && !empty($getCountry)){
                    $postData['COUNTRY'] = $getCountry;
                }
                else{
                    $postData['COUNTRY'] = isset($panelist->profile['address']['country'])?$panelist->profile['address']['country']:"";
                }

                $queryString['PID'] = $panelist->_id->{'$id'};
                $queryString['isDashboard'] = 1;
                $queryString = http_build_query($queryString);
                
                $url = Yii::$app->params['innovateAdmin']['api_url']."/getSurveysPOST"; 
                
                $header = [Yii::$app->params['innovateAdmin']['survey_access_key'], 'Content-Type: application/json'];
                $curl = new curl\Curl();
                $curl->setOption(CURLOPT_RETURNTRANSFER,true);
                $curl->setOption(
                                CURLOPT_POSTFIELDS,
                                json_encode($postData));
                $curl->setOption(CURLOPT_HTTPHEADER, $header);
                $response = $curl->post($url,false);
                
                $statuscode = $curl->responseCode;
                
                if($statuscode == 'timeout') {
                	Yii::trace("GetSurveyPost Timeout Error ", 'error');
                	Yii::$app->response->statusCode = 422; // Unprocessable entity
                    return [
                        'message'=>'Request has been timeout.'
                    ];
                }
                
                if(isset($response['apiStatus']) && $response['apiStatus'] == 'success'){
                    Yii::trace("Get survey API : Number of Surveys ".count($response).", "."for panelist_id ".$id.", "."Post Data ".json_encode($postData).", "."API Response ".json_encode($response["apiStatus"]), 'external_api');
                    // Collecting all surveys to return in response
                    $surveys = [];
                    $CPIs = [];
                    $groupNumbers = [];
                    $projectIds = [];
                    $innovate_pully['grp_num'] = "";
                    $innovate_pully['pully_status'] = "";
                    if(isset($panelist->panel->innovate_pully['group_num']) && isset($panelist->panel->innovate_pully['pully_status'])){
                        $innovate_pully['grp_num'] = $panelist->panel->innovate_pully['group_num'];
                        $innovate_pully['pully_status'] = $panelist->panel->innovate_pully['pully_status'];
                    }
                    
                    foreach ($response['group'] as $key => $survey) {
                        // If innovate pully is same as survey group_num and status is not true then continue to next iteration (skip this iteration)
                        if($innovate_pully['grp_num']==$survey['grp_num'] && !$innovate_pully['pully_status']){
                            continue;
                        }

                        $projectIds[] = (string)$survey['grp_num'];
                        $surveyUrlQuery = http_build_query([
                                'survNum'=>$survey['grp_num_enc'],
                                'supCode'=>($panelist->panel->supplier_id)?$panelist->panel->supplier_id:""
                        ]);

                        // Merging with Questions KEYS in survey url
                        $surveyUrlQuery = $surveyUrlQuery."&".$queryString;
                        $points = $panelist->panel->usd_to_panel_currency*$survey['rw'];
                        $surveys[] = [
                            'survey_url' => Yii::$app->params['innovateAdmin']['survey_url']."?".$surveyUrlQuery,
                            'loi' => $survey['LOI'],
                            'points' => $points,
                            'survey_name' => $survey['grp_nm'],
                            'survey_number' => $survey['grp_num'],
                            'survey_number_enc' => $survey['grp_num_enc'],
                            'CPI' => $survey['CPI'],
                            'allow_duplicate' =>isset($survey['allw_dpl_clks'])?$survey['allw_dpl_clks']:0,
                            'innovate_pully'=>($survey['grp_num']==$innovate_pully['grp_num'])?true:false,
                            'pre_survey_message'=>isset($survey['pre_surv_msg'])?$survey['pre_surv_msg']:''
                        ];
                        $CPIs[] = $survey['CPI'];
                        $groupNumbers[] = $survey['grp_num'];
                    }

                    // Filter surveys which are already taken by Panelist
                    $panelistAttendedSurveys = SurveyActivity::getAttendedSurveysByProjectIds($projectIds, $panelist->_id);
                    $attSurveysProjectIds = [];
                    foreach ($panelistAttendedSurveys as $attSurv) {
                        if(isset($attSurv->project['project_id'])){
                            $attSurveysProjectIds[] = $attSurv->project['project_id'];
                        }
                    }
                    foreach ($surveys as $key => $value) {
                        // If allow duplicate is true then we are not filtering those surveys
                        if(!$value['allow_duplicate'] && in_array($value['survey_number'],$attSurveysProjectIds)){
                            unset($surveys[$key]); 
                            unset($CPIs[$key]);
                            unset($groupNumbers[$key]);
                        }
                    }

                    // Sorting with CPI
                    //array_multisort($CPIs, SORT_DESC, $surveys);
                    array_multisort($groupNumbers, SORT_DESC, $surveys);
                    return ['surveys'=>$surveys];
                } else {
                    Yii::trace("Get survey POST API : Response Fails for panelist_id ".$id." Post Data : ".json_encode($postData)." API Response : ".json_encode($response["apiStatus"]), 'external_api');
                }
                 
            }
            Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
            Yii::trace("Get survey API : Fails for panelist_id ".$id, 'external_api');

        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * [actionSendVerificationEmail Send email verification mail to user]
     * @param  [type] $id [panelist id]
     * @return [array]     [error/success message]
     * URL http://localhost/panels/api/web/{panel_token}/v1/panelists/{panelist_id}/sendverificationmail
     */
    public function actionSendVerificationMail($id){
        if(Yii::$app->user->can('send-verification-mail-panelist')){
            $panelist_id = $id;
            $panelist = Panelist::findOne(['id'=> (int)$panelist_id,'status'=>['$ne' => Panelist::STATUS_DELETED]]);
            if(!empty($panelist)){
                
                // If mail is already verified then don't send any mail
                if($panelist->panelistEmailVerified){
                    Yii::$app->response->statusCode = 422; // Unprocessable entity
                    return [
                        'field'=>'email',
                        'message'=>'Email is already verified'
                    ];
                } 
                
                // Send Verification Mail
                if($panelist->sendVerificationMail()){
                     $panelist['email_verf_snd'] = new \MongoDate();
                     $panelist->save(false);
                    return ['success'=>'Verification mail is sent successfully!'];
                }
                else{
                    Yii::$app->response->statusCode = 503; // Service Unavailable.
                    return [
                        'error'=>error_get_last(),
                        'message'=>'Mail not sent.'
                    ];
                } 
            }
            else
                Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
        }
        else{   
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * [actionGetSurveyActivities get all surveys info which are attemted by users]
     * @param  [type] $id [panelist id]
     * @return [array]     [error/success message]
     * URL http://localhost/panels/api/web/{panel_token}/v1/panelists/{panelist_id}/getsurveyactivity
     */
    public function actionGetSurveyActivity($id){
        if(Yii::$app->user->can('survey-activity-panelist')){
            $panelist_id = $id;
            $queryParams = array();
            $get = Yii::$app->request->queryParams;

            $searchModel = new SurveyActivitySearch();
            $attributes = array_keys($searchModel->attributes);
            foreach($get as $key=>$value){
                if(in_array($key, $attributes)){
                    $queryParams['SurveyActivitySearch'][$key] = $value;
                }
                else{
                    $queryParams[$key] = $value;
                }
            }
            $queryParams['SurveyActivitySearch']['panelist_id'] = $id;
            $dataProvider = $searchModel->search($queryParams);
            
            if(isset(Yii::$app->request->queryParams['pagesize'])){
                $dataProvider->pagination->pageSize=Yii::$app->request->queryParams['pagesize'];
            }

            // All survey data
            $surveys = $dataProvider->getModels();
            $totalCount = $dataProvider->pagination->totalCount;
            if(!empty($surveys)){
                $allsurvey = array();
                foreach ($surveys as $key => $value) {
                        $allsurvey[] = $value->response;
                }
                return [
                        'totalCount'=>$totalCount,
                        'surveys'=>$allsurvey
                        ];
            }
            else
                Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
        }
        else{   
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    /**
     * actionFailSurveyCount count all fail surveys attempt by user within last 24-hour or based on specific 
     * date
     * @param $id [panelist id]
     * Method : GET
     * URL http://localhost/panels/api/web/{panel_token}/v1/panelists/{panelist_id}/failsurveycount?date = Y-M-D
     * The date parameter is optional. 
    */

    public function actionFailSurveyCount($id){
        if(Yii::$app->user->can('fail-survey-count')){
            $model = new Panelist;
            $panelist = $model->getPanelistById();
            
            if($panelist){
                $postback = new SurveyActivity();
                $panelist_id = $panelist->_id;

                $panel = $panelist->panel;
                
                $date = Yii::$app->request->get('date');

                $failSurvey = $postback->failSurveyCount($panelist_id,$date);

                $failSurveyCount  = count($failSurvey);//Get count of total fail survey
                $survey = array();
                $survey['total_fail_survey_count'] = $failSurveyCount;
                $survey['max_fail_limit'] = $panel->max_fail;
                $survey['fail_bonus'] = $panel->fail_bonus;
                
                if(isset($failSurvey) && !empty($failSurvey)){
                    foreach($failSurvey as $key=>$value){
                        $survey['project_id'][]= $value->project['project_id'];
                    }
                }
                else{
                    $survey['project_id'] = [];
                }
                
                return $survey;
            }
            else{
                Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
            }
        }       
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }

    public function actionPurgePanelist(){
        if(Yii::$app->user->can('purge-panelist')){

            $panelist = new Panelist;
            $id = Yii::$app->request->get('id');

            if(isset($id) && Panelist::isValidMongoId($id)){
                $panelist_id = new \MongoId($id);
            }
            else{
                $panelist_id = isset($id) && preg_match("/^[0-9]*$/",$id)?(int)$id:$id;
            }

            $panelist = $panelist->getPanelistById($panelist_id);
           
            if($panelist){
                $deleted_information = $panelist->purgePanelist();
                Yii::trace("Panelist deleted ".$panelist->id, 'purge');
                return $deleted_information;
            }
            else{
                return [
                    'Panelist Not Found!'
                ];
            }
            

        }
        else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }
    /**
     * Action is used to get lifetime points of panelist according to condition
     * URL http://localhost/panels/api/web/{panel_token}/v1/panelists/update-lifetime-point
     * Method : GET
    */

    // This api add lifetime points and we have to run this api once on staging and production,therefore we comment this api
    
    // public function actionUpdateLifetimePoint(){

    //     $collection = Yii::$app->mongodb->getCollection('panelist_transaction');
    //     $query = [
    //         [
    //             '$match'=>[
    //                     'type'=>[
    //                         '$nin'=>Yii::$app->params['transaction_type']
    //                     ],
    //                     'transaction_status'=>[
    //                                         '$ne'=>PanelistTransaction::TRANSACTION_STATUS_REVERTED
    //                     ]            
    //             ]                                   
    //         ],
    //         [
    //            '$project' =>[
    //                 '_id'=>0,
    //                 'panelist_id'=>1,
    //                 'points'=>[
    //                     '$abs' => '$incentives.points'
    //                 ],
    //                 'type' => 1,

    //             ]
    //         ],  
    //         [ 
    //             '$group'=> [   
    //                 '_id'=> ["panelist_id" => '$panelist_id'],
    //                 'points'=>['$push' => '$points']
    //             ]

    //         ]
    //     ];
    //     $result['panelist'] = $collection->aggregate(
    //         $query    
    //     );
    //     foreach ($result['panelist'] as $key => $value) {
    //         $userid= $value['_id']['panelist_id'];
    //         $userpoints = abs(array_sum($value['points']));
    //          Panelist::updateAll(['$inc'=>['lifetime_points' => $userpoints]],['_id'=>$userid]);
    //          Yii::info("Update Life Time Points : For panelist - ".$userid.", Life time points earned by panelist - ".$userpoints,'lifetime_points');
    //     } 
    // }

        /**
     * Action is used to get daily streak details of panelists from "panelist_daily_streak" collection
     * URL http://localhost/panels/api/web/{panel_token}/v1/panelists/{id}/dailystreak
     * Method : GET
    */
    public function actionViewAllDailyStreak($id){
        
        if(Yii::$app->user->can('view-all-daily-streak')){

            $get = Yii::$app->request->queryParams;
            $modelDS = new DailyStreakDetails();
            $attributes = array_keys($modelDS->attributes);

            foreach($get as $key=>$value){
                // If we have attribute set in model already
                if(in_array($key, $attributes)){
                    $queryParams['DailyStreakDetails'][$key] = $value;
                }
                // If attribute is not set
                else{
                    $queryParams[$key] = $value;
                }
            }

            $panelistDailyStreak = $modelDS->search($queryParams);

            if(!empty($panelistDailyStreak['daily_streak'])) {
                
                $dailyStreak = array();
                $totalCount = 0;
                foreach($panelistDailyStreak['daily_streak'] as $key => $value) {
                    $dailyStreak[] = $value;
                }
                // Return response in API
                return [
                    'totalCount'=>$panelistDailyStreak['totalcount'],
                    'panelists'=>$dailyStreak
                ];
            } else {
                Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
            }
            
        }else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }
     /**
     * Action is used when admin generate forgot link for particular user
     * URL http://localhost/panels/api/web/{panel_token}/v1/panelists/{id}/forgotpasswordlink?panel_id={panel_qid}
     * Method : GET
    */
    public function actionForgotPasswordLink($id){
        if(Yii::$app->user->can('forgot-link')){
            $model = new Panelist();
            // we check panel id through this scenario
            $model->scenario = 'panel';
            $model->setPanelId();
            if(!$model->validate()){
                return $model;
            }
            if(BaseModel::isValidMongoId($id)) {
                // This is mongo id 
                $enc_pid = new \MongoId($id);
                $panelist = $model->findOne(['_id'=> $enc_pid,'status'=>['$in' => array_values($model->allStatus)]]);
            } else {
                    $panelist_id = (int)$id;
                    $panelist = $model->findOne(['panel_id'=>$model->panel_id,'status'=>['$in' => array_values($model->allStatus)],'id'=> $panelist_id]);
            }
            // if panelist exit we save time at which admin generate forgot link
            if(!empty($panelist)){
                $panelist['pwd_reset'] = new \MongoDate();
                $panelist->save(false);
                // forgot link of user , in this we encoded email and auth_key of user
                $forgotLinkURL = $panelist->panel->base_domain.'/forgotpasswordlink?email='.base64_encode($panelist->email).'&authkey='.base64_encode($panelist->auth_key).'&evchk='.base64_encode($panelist->auth_key.$panelist->pwd_reset->sec);
                // sending mail to user 
                if($panelist->sendForgotLinkMail($forgotLinkURL)){
                    return ['success'=>'Forgot Password link is sent to your registered email.'];
                }
                else{
                    Yii::$app->response->statusCode = 503; // Service Unavailable.
                    return [
                        'error'=>error_get_last(),
                        'message'=>'Mail not sent.'
                    ];
                }
                
            }
            else{
                Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
                Yii::$app->end();
            }
        }else{
            throw new \yii\web\UnauthorizedHttpException("You don't have permission to access this API");
        }
    }
}


