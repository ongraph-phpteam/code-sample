<?php

namespace common\models\panelist;

use Yii;

use common\models\base\BaseModel;
use common\models\panelist\Panelistprofile;
use common\models\panelist\Panelistmeta;
use common\models\panelist\DefaultAnswers;
use common\models\panel\Panel;
use common\models\card\CardRedemption;
use common\models\panelist\PanelistLoginAttempts;
use common\models\panelist\PanelistTransaction;
use mongosoft\mongodb\MongoDateBehavior;
use yii\behaviors\BlameableBehavior;
use yii\db\Expression;
use linslin\yii2\curl;
use yii\web\UploadedFile;
use common\models\Countries;
use common\models\panelist\PanelistTrueSampleInfo;
use common\models\dailystreak\DailyStreak;
use common\models\dailystreak\DailyStreakDetails;

/**
 * This is the model class for collection "panelist".
 *
 * @property \MongoId|string $_id
 */
class Panelist extends BaseModel implements \yii\web\IdentityInterface
{

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const STATUS_DELETED = -1;

    const EMAIL_NOT_BOUNCED = 0;
    
    const STATUS_SURVEY_ACTIVE = TRUE;
    const STATUS_SURVEY_INACTIVE = FALSE;

    const STATUS_CASHOUT_ACTIVE = TRUE;
    const STATUS_CASHOUT_INACTIVE = FALSE;

    const STATUS_EMAIL_ACTIVE = TRUE;
    const STATUS_EMAIL_INACTIVE = FALSE;

    const DEFAULT_STEP = 0;
    const NO_MAIL_SENT = 0;

    const DEFAULT_GAMES_STEP = 0;
    const FIRST_GAMES_STEP = 1;
    const SECOND_GAMES_STEP = 2;

    
    /**
     * Step 1 : when panelist completes his profiling
     * Step 2 : when panelist clicks on profiling survey
     * Step 3 : when panelist successfully completes profiling survey
     * Step 4 : when panelist complete his clubhouse tour
     */
    const FIRST_STEP = 1;
    const SECOND_STEP = 2;
    const THIRD_STEP = 3;
    const FOURTH_STEP = 4;
    const ALLOWED_AGE = 13;

    public $_panelist;
    
    public $created_by;

    public $updated_by;

    public $transaction_ip;
    public $user_device;

    public $daily_streak_transaction = true;
    public $daily_streak_update = false;

    public  $allStatus =[ 'STATUS_ACTIVE' => 1,
                          'STATUS_INACTIVE'=> 0
                        ];

    /**
     * @inheritdoc
     */
    public static function collectionName()
    {
        return 'panelist';
    }

    /**
     * 
     */
    public function attributes()
    {
        return [
            '_id',
            'id',
            'panel_id',
            'email',
            'password',
            'password_reset_token',
            'auth_key',
            'status',
            'status_email',
            'status_survey',
            'status_cashout',
            'hoa_id',
            'aid',
            'offer_id',
            'aff_sub',
            'points',
            'card',
            'step',
            'source',
            'profile',
            'meta',
            'mail_sent_count',
            'welcome_email_sent',
            'google_id',
            'facebook_id',
            'surveys_attended',
            'surveys_completed',
            'truesampleinfo',
            'traffic_source',
            'bounce_email',
            'sweeps_code', // facebook sweep code
            'clubhouse_tour_review', // review given by panelist on clubhouse tour
            'truesample_score_version',
            'daily_streak',
            'notes',
            'last',
            'unsubscribe_date',
            'unsubscribe_by',
            'lifetime_points',
            'status_email_updated_at',
            'status_email_updated_by',
            'email_verf_snd',
            'cshout_speeder',
            'pwd_reset', // password reset field for forgot link
            'sur_speeder_cnt', // survey speeder count
            'invalid_txn_cnt',
            'fb_clk_ovrly', // facebook overlay click
            'fb_cls_ovrly' // facebook overlay closed
        ];
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['password', 'email','panel_id'], 'required'],
            ['panel_id','required', 'on' => 'panel'],
            ['google_id','required', 'when'=>function(){
                return ($this->facebook_id==null);
            },'message'=>'google or facebook cannot be blank'],

            ['facebook_id','required', 'when'=>function(){
                return ($this->google_id==null);
            },'message'=>'google or facebook cannot be blank'],

            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status_email', 'default', 'value' => self::STATUS_EMAIL_ACTIVE],
            ['status_cashout', 'default', 'value' => self::STATUS_CASHOUT_ACTIVE],
            ['status_survey', 'default', 'value' => self::STATUS_SURVEY_ACTIVE],
            [['status_email','status_cashout','status_survey'], 'boolean'],
            [['status'], 'number'],
            [['id'], 'integer'],
            [['email'], 'email'],
            ['email', 'unique', 'message' => 'This email address has already been taken.','filter' => ['panel_id' => (Panel::isAdmin())?$this->panel_id:Panel::getPanelByToken()->_id,"status"=>['$ne'=>self::STATUS_DELETED]]],
            ['meta', 'common\validators\EmbedDocValidator', 'model'=>'\common\models\panelist\Panelistmeta'],
            ['profile', 'common\validators\EmbedDocValidator', 'scenario' => ['addAnswer','panelist','social-login','social-login-update','sourceuser-delete'], 'model'=>'\common\models\panelist\Panelistprofile'],
            ['truesampleinfo', 'common\validators\EmbedDocValidator', 'model'=>'\common\models\panelist\PanelistTrueSampleInfo'],
            ['daily_streak', 'common\validators\EmbedDocValidator', 'model'=>'\common\models\panelist\PanelistDailyStreak'],

            [['id', 'google_id', 'facebook_id', 'panel_id', 'email','source', 'password', 'password_reset_token', 'status','hoa_id', 'aid', 'offer_id', 'points', 'card', 'step', 'meta', 'profile', 'surveys_attended','surveys_completed','truesampleinfo','status_email','status_cashout','status_survey','welcome_email_sent','aff_sub','bounce_email','sweeps_code','clubhouse_tour_review','truesample_score_version','daily_streak','notes','last','unsubscribe_date','unsubscribe_by','lifetime_points','status_email_updated_at','status_email_updated_by','email_verf_snd','cshout_speeder','pwd_reset','sur_speeder_cnt'],'safe'],
        ];
    }

    public function scenarios(){
            $scenarios = parent::scenarios();
            $scenarios['card'] = ['card'];//Scenario Values Only Accepted
            $scenarios['addAnswer'] = ['profile'];//Scenario Values Only Accepted
            $scenarios['social-login'] = ['google_id', 'facebook_id', 'email', 'id', 'panel_id', 'status','hoa_id', 'aid', 'offer_id', 'points', 'step', 'meta', 'profile','status_email','status_cashout','status_survey','traffic_source','aff_sub'];//Scenario Values Only Accepted
            $scenarios['social-login-update'] = ['id', 'panel_id', 'email', 'status','hoa_id', 'aid', 'points', 'step', 'meta', 'profile','status_email','status_cashout','status_survey','traffic_source','aff_sub','daily_streak'];//Scenario Values Only Accepted
            $scenarios['panelist'] = ['id', 'panel_id', 'offer_id', 'email', 'password', 'password_reset_token', 'status','hoa_id', 'aid', 'points', 'step', 'meta', 'profile','surveys_attended','surveys_completed','truesampleinfo','status_email','status_cashout','status_survey','traffic_source','aff_sub','daily_streak','lifetime_points'];//Scenario Values Only Accepted
            $scenarios['sourceuser-delete'] = ['id', 'panel_id', 'offer_id', 'email',  'status','hoa_id', 'aid', 'points', 'step', 'meta', 'profile','surveys_attended','surveys_completed','truesampleinfo','status_email','status_cashout','status_survey','traffic_source','aff_sub'];//Scenario Values Only Accepted
            $scenarios['unsubscribe'] = ['id', 'panel_id', 'email', 'status'];
            $scenarios['panel'] = ['panel_id'];
            return $scenarios;
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            '_id' => 'ID',
            'id' => 'Panelist ID',
            'panel_id' => 'Panel Id',
            'status' => 'Status',
            'password' => 'Password',
            'hoa_id' => 'Has Offer id',
            'aid' => 'Affiliate Id',
            'aff_sub' => 'Affiliate Sub',
            'points' => 'Points',
            'email' => 'Email',
            'meta' => 'Panelist Meta Data',
            'profile' => 'Panelist Profile',
            'surveys_attended'=>'surveys_attended',
            'surveys_completed'=>'surveys_completed' ,
            'truesampleinfo'=>'Panelist True Sample Data'           
        ];
    }

    public function getPanelistEmailVerified(){
        return (isset($this->meta['email_verified']))?$this->meta['email_verified']:"";
    }

    public function getPanelistRegistrationIP(){
        return (isset($this->meta['registration_ip']))?$this->meta['registration_ip']:"";
    }

    /**
     * Get Panelist's Gender
     *
     */
    public function getPanelistGender(){
        return (isset($this->profile['gender']))?$this->profile['gender']:"";
    }

    /**
     * Get Panelist's Date of Birth
     *
     */
    public function getPanelistDob(){
        return (isset($this->profile['dob']))?$this->profile['dob']:"";
    }

    /**
     * Get Panelist's Date of Birth
     *
     */
    public function getPanelistAvatar(){
        return (isset($this->profile['avatar']))?$this->profile['avatar']:"";
    }

    /**
     * Get Panelist's Street
     *
     */
    public function getPanelistStreet(){
        return (isset($this->profile['address']['street']))?$this->profile['address']['street']:"";
    }

    /**
     * Get Panelist's Street
     *
     */
    public function getDailyStreak(){
        return (isset($this->daily_streak))?$this->daily_streak:"";
    }

    /**
     * Get Panelist's ZipCode
     *
     */
    public function getPanelistZipcode(){
        return (isset($this->profile['address']['zipcode']))?$this->profile['address']['zipcode']:"";
    }
    /**
     * Get Panelist's ISOCode
     *
     */
    public function getPanelistISOCode(){
        return (isset($this->profile['address']['iso_code']))?$this->profile['address']['iso_code']:"";
    }

    /**
     * Get Panelist's Created At Mongo time
     */
    public function getPanelistCreatedAt()
    {
        return (isset($this->meta['created_at']))?$this->meta['created_at']:"";
    }

    /**
     * Getter that gets Panelist's Updated At Mongo time
     */
    public function getPanelistUpdatedAt()
    {
        return (isset($this->meta['updated_at']))?$this->meta['updated_at']:"";
    }

    /**
     * Get Created By from DB
     */
    public function getPanelistCreatedBy()
    {
        return (isset($this->meta['created_by']))?$this->meta['created_by']:"";
    }

    /**
     * Get Created By from DB
     */
    public function getPanelistLastActionAt()
    {
        return (isset($this->meta['lastaction_at']))?$this->meta['lastaction_at']:"";
    }

    /**
     * Get Created By from DB
     */
    public function getPanelistLastVisitAt()
    {
        return isset($this->meta['lastvisit_at'])?$this->meta['lastvisit_at']:"";
    }

    /**
     * Get Created By from DB
     */
    public function getPanelistLastVisitIp()
    {
        return isset($this->meta['lastvisit_ip'])?$this->meta['lastvisit_ip']:"";
    }

    /**
     * Get Updated By from DB
     */
    public function getPanelistUpdatedBy()
    {
        return (isset($this->meta['updated_by']))?$this->meta['updated_by']:"";
    }

    /**
     * Get Updated By from DB
     */
    public function getPanelistReferralCode()
    {
        return (isset($this->meta['referral_code']))?$this->meta['referral_code']:"";
    }

    /**
     * Get Panelist user device from DB
     */
    public function getPanelistUserDevice()
    {
        return (isset($this->meta['user_device']))?$this->meta['user_device']:"";
    }

    /**
     * Get First name from DB
     */
    public function getPanelistFirstName()
    {
        return (isset($this->profile['name']['first']))?$this->profile['name']['first']:"";
    }

    /**
     * Get Last name from DB
     */
    public function getPanelistLastName()
    {
        return (isset($this->profile['name']['last']))?$this->profile['name']['last']:"";
    }

    public function getPanelistContactNo()
    {
        return (isset($this->profile['contact_no']))?$this->profile['contact_no']:"";
    }

    public function getPanelistReferredBy()
    {
        return (isset($panelist->meta['referred_by']))?$panelist->meta['referred_by']:"";
    }

    public function getPanelistSupplierId()
    {
        return (isset($this->panel->supplier_id))?$this->panel->supplier_id:"";
    }
    /**
     * Get Lifetime points from DB
     */
    public function getPanelistLifetimePoints()
    {
        return (isset($this->lifetime_points))?$this->lifetime_points:0;
    }

    /**
     * Panel relation
     *
     */
    public function getPanel(){
        return $this->hasOne(Panel::className(), ['_id' => 'panel_id']);
    }
    
    public function getPanelistByReferral($code){
        $panelist = self::find()->where(['meta.referral_code'=>(string)$code,'status'=>['$ne' => Panel::STATUS_DELETED]])->One();
        if(!empty($panelist)){
            return $panelist;
        }

        return false;
    }

    public function getPanelistCountry(){
        return (isset($this->profile['address']['country']))?$this->profile['address']['country']:"";
    }

    public function getPanelistCity(){
        return (isset($this->profile['address']['city']))?$this->profile['address']['city']:"";
    }

    public function getPanelistState(){
        return (isset($this->profile['address']['state']))?$this->profile['address']['state']:"";
    }

    public function getPanelistTrafficSource(){
        return (isset($this->traffic_source))?$this->traffic_source:"";
    }

    public function getPanelistNotes(){
        return (isset($this->notes))?$this->notes:"";
    }

    public function getPanelistTrafficSourceInfo(){
        if($this->traffic_source == 1)
            $this->traffic_source = "Email/Newsletter";
        elseif($this->traffic_source == 2)
            $this->traffic_source = "Facebook";
        elseif($this->traffic_source == 3)
            $this->traffic_source = "Forum or Blog";
        elseif($this->traffic_source == 4)
            $this->traffic_source = "Friend";
        elseif($this->traffic_source == 5)
            $this->traffic_source = "Search Engine";
        elseif($this->traffic_source == 6)
            $this->traffic_source = "TV Commercial";
        elseif($this->traffic_source == 7)
            $this->traffic_source = "Twitter";
        elseif($this->traffic_source == 8)
            $this->traffic_source = "Website";
        elseif($this->traffic_source == 9)
            $this->traffic_source = "YouTube";
        elseif($this->traffic_source == 10)
            $this->traffic_source = "Other";
        return (isset($this->traffic_source))?$this->traffic_source:"";
    }

    public function getPanelistAnswers(){
        return (isset($this->profile['answers']))?$this->profile['answers']:"";
    }

    public function getPanelistCard(){
        return (isset($this->card))?$this->card:"";
    }

    public function setReferralCode(){
        return base_convert(md5(time())."referral", 6, 6); // Random string
    }

    public function setReferredBy(){
        if(!empty($this->r_code)){
            $referred_panelist = $this->getPanelistByReferral($this->r_code);
            if(!empty($referred_panelist)){
                return $referred_panelist->_id;
            }
        }
        return "";
    }

    /**
     * date when user perform some action on our site last time
     *
     */
    public function setLastActionAt(){
        return new \MongoDate(); 
    }

    /**
     * 
     *
     */
    public function setEmailNotVerified(){
        $this->data['Panelist']['meta']['email_verified'] = Panelistmeta::EMAIL_NOT_VERIFIED;
    }

    public function setUserDevice(){
        return isset($this->user_device)?$this->user_device:"";
    }


    public function setCreatedAt(){
        return new \MongoDate();
    }

    public function setUpdatedAt(){
        $this->meta = array_merge($this->meta,['updated_at'=>new \MongoDate()]);
    }

    /** 
     * Set Panelist's Panel Id
     *
     */
    public function setPanelId(){
        /* In case of Panel is admin then if request is GET,DELETE we take panel id in get request else for PUT and POST we take in post request*/
        if(Panel::isAdmin()){
            $panel = (Yii::$app->request->isGet || Yii::$app->request->isDelete)?Panel::getPanelById(Yii::$app->request->get('panel_id')):Panel::getPanelById(Yii::$app->request->post('panel_id'));
            $this->panel_id = isset($panel->_id)?$panel->_id:"";
        }
        else{
            $panel = Panel::getPanelByToken();
            $this->panel_id = isset($panel->_id)?$panel->_id:"";
        }
        return $this->panel_id;
    }


    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

     /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Create Panelist, called by create action
     * Note: Don't Change the order for setting attributes.
     */
    public function create(){
        $this->setMeta();
        $this->setTrueSampleInfo();
        $this->generateAuthKey();
        $this->step = self::DEFAULT_STEP;
        $this->save();
        $this->SignUpTransaction();//Create new Transaction for user who register but not complete profile infomartion.
        PanelistLoginAttempts::saveLoginAttempts($this->_id,$this->panelistRegistrationIP,$this->PanelistUserDevice);
        return $this;
    }

    /**
     *Create Transation for new Register user.
    */
    public function SignUpTransaction(){
        if($this->panel->signup_bonus && $this->panel->signup_bonus>0){
           $transaction = new PanelistTransaction;
            $data['panel_id'] = $this->panel->id;
            $data['type'] = 'Signup Bonus';
            $data['message'] = 'Point Bonus on Registration';
            $data['identifier'] = $this->generateRandomString(32);
            $data['transaction_status'] = 'Complete';
            $data['points'] = $this->panel->signup_bonus;//Points = 2000
            $data['transaction_ip'] = $this->transaction_ip;
            $transaction->createTransaction($this, $data); 
        }
    }

    /**
     * Create Transation for the users who are in games.pointclub.com and they are creating transaction by API
     */
    public function GamesPCTransaction($data) {
        $panelInfo = Panel::getPanelByToken();
        if(isset($panelInfo->games_adv_points) && $panelInfo->games_adv_points > 0) {
            $transaction = new PanelistTransaction;
            $data['panel_id'] = $this->panel->id;
            $data['type'] = 'Games Adv Points';
            $data['message'] = 'Point Bonus for Games Adv Points';
            $data['identifier'] = $this->generateRandomString(32);
            $data['transaction_status'] = 'Complete';
            $data['points'] = $this->panel->games_adv_points;
            $tranResponse = $transaction->createTransaction($this, $data);
            return $tranResponse;
        }
    }

    /**
     * Create Daily streak Transation
     * These transaction will be given only one time to panelist on first day of level only
    */
    public function DailyStreakTransaction($levelBonus){
        if(isset($this->daily_streak) && !empty($this->daily_streak)) {

            $panelistTransactionModel = new PanelistTransaction();

            $transactionMessage = "Daily Streak Bonus Reward for ".$this->daily_streak['day']." day";
            // Calling function to get all info to create a new daily streak type transaction
            $dataBonusTransaction = $this->levelBonus($levelBonus,Yii::$app->params['daily_streak']['transaction_type'],$transactionMessage);

            // Check if transaction is already created for same day
            $panelistTransactions = $panelistTransactionModel->getTransactionsByMessage($this->_id->{'$id'},$transactionMessage);
            
            if(empty($panelistTransactions)) {
                // Create a new transaction for daily streak type
                $panelistTransactionModel->createTransaction($this,$dataBonusTransaction);
            }
        }

    }

    public function levelBonus($levelBonusPoints,$transactionType,$transactionMessage) {

        $panelistTransactionModel = new PanelistTransaction();

        $dataBonusTransaction['allow_duplicate'] = "";
        $dataBonusTransaction['project_id'] = "";
        $dataBonusTransaction['points'] = $levelBonusPoints; 
        $dataBonusTransaction['survey_points'] = "";
        $dataBonusTransaction['transaction_status'] = "Complete";
        $dataBonusTransaction['panel_id'] = $this->panel_id->{'$id'};
        $dataBonusTransaction['enc_pid'] = $this->_id->{'$id'};
        $dataBonusTransaction['type'] = $transactionType;
        $dataBonusTransaction['message'] = $transactionMessage;
        $dataBonusTransaction['identifier'] = $panelistTransactionModel->generateRandomString(32);
        $dataBonusTransaction['transaction_ip'] = $this->transaction_ip;
        $dataBonusTransaction['user_device'] =isset($this->meta['user_device'])?$this->meta['user_device']:"";

        return $dataBonusTransaction;
    }

    /**
     *Create Transation for Update Registered user and we fire SOI pixel in this case on Point Club Side.
    */
    public function SOITransaction($post){
        if($this->panel->profile_bonus && $this->panel->profile_bonus>0){
            $transaction = new PanelistTransaction;
            $data['panel_id'] = $this->panel->id;
            $data['type'] = 'Profile Bonus';
            $data['message'] = 'Point Bonus on Update Profile';
            $data['identifier'] = $this->generateRandomString(32);
            $data['transaction_status'] = 'Complete';
            $data['points'] = $this->panel->profile_bonus; //Points = 1000
            $data['transaction_ip'] = $this->transaction_ip;
            $data['traffic_source'] = isset($post['traffic_source'])?(int)$post['traffic_source']:"";
            $data['country'] = isset($post['country'])?$post['country']:"";
            $transaction->createTransaction($this, $data);
        }
    }

    /**
     *Create Transation when user click on Profiling Survey.
    */

    public function ProfilingTransaction(){
        if($this->panel->profiling_question_bonus && $this->panel->profiling_question_bonus>0){
            $transaction = new PanelistTransaction;
            $data['panel_id'] = $this->panel->id;
            $data['type'] = 'Profiling Question Bonus';
            $data['message'] = 'Point Bonus on submitting profiling questions';
            $data['identifier'] = $this->generateRandomString(32);
            $data['transaction_status'] = 'Complete';
            $data['points'] = $this->panel->profiling_question_bonus; //Points = 1000
            $data['transaction_ip'] = $this->transaction_ip;
            $data['user_device'] = $this->user_device;
            $transaction->createTransaction($this, $data);
        }
    }


    public function beforeSave($insert){
        if (parent::beforeSave($insert)) {
            if($this->isNewRecord)
                $this->id = $this->getNextId(__CLASS__);
            else {

                /**
                 * Here we are updating lastaction_at and updated_at date field on every update request.
                 * If there is any call from admin side we only update update_at field.
                 * OtherWise we update both date field. 
                 */
                if(Panel::isAdmin()){
                   $this->meta = array_merge($this->meta,['updated_at'=>new \MongoDate()]);
                }
                else{

                    // We will only update lastaction_at if we are not updating daily_streak
                    if($this->daily_streak_update == false) {
                        $this->meta = array_merge($this->meta,['lastaction_at'=>new \MongoDate()]);    
                    }

                    $this->meta = array_merge($this->meta,['updated_at'=>new \MongoDate()]);

                    /** 
                     * We will update daily streak if survey status is true for panelist
                     * Daily streak info will be update only when step value is 4 (means user is on clubaccount page and completed all profiling questions)
                     */
                    if( ($this->status_survey == true)  && ($this->step == self::FOURTH_STEP) ) {
                        
                        $this->manageDailyStreak();

                        $dailyStreakInfo = DailyStreak::find()->where(['day'=>$this->daily_streak['day']])->one();
                        if($dailyStreakInfo['extra_bonus'] != "") {

                            $panelistTransactionModel = new PanelistTransaction();

                            $maxDatePST = $this->getPSTDate(date('Y-m-d',$this->daily_streak['max_day_date']->sec));
                            $streakDatePST = $this->getPSTDate(date('Y-m-d',$this->daily_streak['daily_streak_date']->sec));
                            
                            if($maxDatePST['date'] <= $streakDatePST['date']) {
                                /**
                                 * Here we are managing DailyStreakTransaction call with one parameter
                                 * (daily_streak_transaction).Because before save is called everytime while save method
                                 * is called. So due to multiple call we face some issue which is in current scenario we are 
                                 * updating data on different server but read operation from differet server.In this api 
                                 * it creates two transaction because at the time of reading data not reflect on 
                                 * server. 
                                 */
                                if($this->daily_streak_transaction == true){
                                    $this->DailyStreakTransaction($dailyStreakInfo['extra_bonus']);
                                }
                            }
                        }
                    }

                }

            }
            return true;
        } else {
            return false;
        }
    }

    /**
     * This function is used to manage panelist DailyStreak Info
     * If panelist last daily streak date is not in current day then we will increase day by one. The level will be increases automatically on basis of day number from daily_streak collection
     * If user last_action date is before 48 hours then we will decrease level by one and the day number will be the first day of new level
     */
    public function manageDailyStreak(){

        $currentDate = Date('Y-m-d H:i:s');
        $currentPSTDate = $this->getPSTDate($currentDate);

        if(is_object($this->daily_streak['daily_streak_date'])) {
            $dailyStreakDate = $this->daily_streak['daily_streak_date'];
            $convertToDate = date('Y-m-d H:i:s',$dailyStreakDate->sec);
            $dailyStreakPSTDate = $this->getPSTDate($convertToDate);    
        } else {
            $dailyStreakPSTDate = $this->getPSTDate($this->daily_streak['daily_streak_date']);
        }

        $oneDaysBeforeDate = date('Y-m-d', strtotime('-1 day', strtotime($currentPSTDate['date'])));

        // The streak day will be set only for new users (that registered after implementing this functionality(27 sept 2016))
        if(isset($this->daily_streak['day'])) {

            // We will allow to update streak info once in a day
            if($dailyStreakPSTDate['date'] < $currentPSTDate['date']) {
                // If user level and day is not zero then we are adding his day and level(level will be added on basis of day number)
                if($this->daily_streak['level'] != 0 && $this->daily_streak['day'] != 0) {

                    // if user last streak date is equal to currentdate-one day (means user is allowed to update streak info)
                    if($dailyStreakPSTDate['date'] == $oneDaysBeforeDate) {

                        /** 
                         * We are increasing user day by one.
                         * We will increase user level on basis of panelist daily streak day number
                         */
                        $dailyStreakInfo = DailyStreak::find()->where(['level'=>$this->daily_streak['level']])->orderBy('day')->all();
                        
                        $streakDay = $this->daily_streak['day']+1;
                        $streakLevel = $this->daily_streak['level'];
                        // If panelist daily streak day number is greater than to last day in level then we are increasing level by one
                        if($streakDay > end($dailyStreakInfo)->day) {
                            // if last day of level reach to 100 then we not increase streak level
                            if(end($dailyStreakInfo)->day == Yii::$app->params['daily_streak']['streak_last_day']){
                                    $streakLevel = $this->daily_streak['level'];
                                }
                                else{
                                    $streakLevel = $this->daily_streak['level']+1;
                                }
                        }
                        
                        $this->setDailyStreak($streakLevel,$streakDay,new \MongoDate());

                    } 
                    // If user daily streak date is before one day then we are subtracting level by one and updating day from first day of new level
                    else {
                        
                        // The user cant back to level zero after come into level one, User minimum level will be one and day will be one in such case.
                        if($this->daily_streak['level'] != 1) {
                            
                            $streakLevel = $this->daily_streak['level']-1;
                            $dailyStreakInfo = DailyStreak::find()->where(['level'=>$streakLevel])->orderBy('day')->all();
                            
                            $streakDay = $dailyStreakInfo[0]->day;
                            $this->setDailyStreak($streakLevel,$streakDay,new \MongoDate());  
                        } else {
                            $this->setDailyStreak($this->daily_streak['level'],1,new \MongoDate());
                        }   
                    }     
                } 
                // If user comes to our site first time after registration then we are updating his daily streak info
                else {
                    $this->setDailyStreak(1,1,new \MongoDate());
                }
            } 
        } 
        // For existing users where daily streak is not updated
        else {
            $this->setDailyStreak(0,0,new \MongoDate());
        }
    }

    // Set panelist Daily streak in attribute daily_streak 
    public function setDailyStreak($level,$day,$streakDate) {

        $max_day = $day;
        $max_day_date = $streakDate;
        if(isset($this->daily_streak['max_day'])) {

            $maxDayPSTDate = $this->getPSTDate(date('Y-m-d',$this->daily_streak['max_day_date']->sec)); 
            $streakPSTDate = $this->getPSTDate(date('Y-m-d',$max_day_date->sec)); 

            if($this->daily_streak['max_day'] < $day) {
                $max_day =  $day;
                $max_day_date = new \MongoDate();
            } else {
                $max_day =  $this->daily_streak['max_day'];
                $max_day_date = $this->daily_streak['max_day_date'];
            }
        }

        $dailyStreak = [
            'level' => $level,
            'day' => $day,
            'daily_streak_date' => $streakDate,
            'max_day' => $max_day,
            'max_day_date' => $max_day_date
        ];
        $this->daily_streak = $dailyStreak;

        // save daily streak info in seperate collection
        $DailyStreakDetails = new DailyStreakDetails();
        $DailyStreakDetails->saveDailyStreak($this->id,$this->_id,$this->daily_streak);
    }

    /**
     * Update a panelist, called by update action
     */
    public function updatePanelist($postdata){
        
        $this->setUpdatedAt();
        if(is_object($this->panelistAvatar)){
            $profile = $this->profile;
            $profile['avatar'] = $this->upload('panelistAvatar','pid');
            $this->profile = $profile;
        }

        $this->save();

        if($this->source =="coreg" || $this->source =="import" || $this->facebook_id != "" || $this->google_id != "" || ($this->step >0 && $postdata['source'] != "")){
            // Email verication reward - DOI
            $this->setEmailVerified();
        }
        return $this;
    }

    /**
     * Delete a panelist, called by delete action
     */
    public function deletePanelist(){
        $this->status = self::STATUS_DELETED;
        $this->setUpdatedAt();
        $this->save();
    }

    public function addAnswer(){
        $profile = $this->profile;
        
        // Converting option_id string to int
        // It will give exact result when searching panelist's profiling answers
        // As we are passing int value for option_id in qualified api request payload
        $newOpt = [];
        foreach ($profile['answers']['options'] as $optKey=>$optValue) {
            $newOpt[$optKey]['option_text'] = $optValue['option_text'];
            if(isset($optValue['option_id'])){
                $newOpt[$optKey]['option_id'] = (int)$optValue['option_id'];
            }
        }
        $profile['answers']['options'] = $newOpt;

        // Find question information from question_id and panel_id
        $question = Panel::getPanelQuestionById($profile['answers']['question_id'],$this->panel->_id);
      
        // Checking if we have question_text, category_text, question_type in post request.
        // As question_text, category_text, question_type are not mendatory fields
        if(!isset($question['question_text']) && isset($profile['answers']['question_text'])){
            $question['question_text'] = $profile['answers']['question_text'];
        }else if(!isset($question['question_text'])){
            $question['question_text'] = "";
        }

        if(!isset($question['category_text']) && isset($profile['answers']['category_text'])){
            $question['category_text'] = $profile['answers']['category_text'];
        }else if(!isset($question['category_text'])){
            $question['category_text'] = "";
        }

        if(!isset($question['question_type']) && isset($profile['answers']['question_type'])){
            $question['question_type'] = $profile['answers']['question_type'];
        }else if(!isset($question['question_type'])){
            $question['question_type'] = "";
        }
        // end
        
        $answers = [
                                'question_id'=>$profile['answers']['question_id'],
                                'question_key'=>$profile['answers']['question_key'],
                                'question_text'=>$question['question_text'],
                                'question_type'=>$question['question_type'],
                                'category_id'=>$profile['answers']['category_id'],
                                'category_text'=>$question['category_text'],
                                'options'=>$profile['answers']['options'],
                                
                            ];

        if(isset($profile['answers'])){
            // Updating new answers
            foreach ($profile['answers'] as $key => $value) {
                if(isset($value['question_id']) && isset($value['question_key']) && ($value['question_id']==$answers['question_id']  && $value['question_key']==$answers['question_key'])){
                    unset($profile['answers'][$key]);
                }
            }
        }
        else{
            $profile['answers'] = array();
        }

        $profile['answers'] = array_merge(
                                            $profile['answers'], 
                                            [$answers]
                                        );
        unset($profile['answers']['question_id']);
        unset($profile['answers']['category_id']);
        unset($profile['answers']['question_key']);
        unset($profile['answers']['options']);
        unset($profile['answers']['question_type']);
        unset($profile['answers']['question_text']);
        unset($profile['answers']['category_text']);
        $this->profile = $profile;
                    
        return $this->save(false);
    }

    /**
     * @inheritdoc
     */
    function behaviors()
    {
        return [
            //BlameableBehavior::className(),
        ];
    }

    /**
     * Setting only those values which can be send by panelist.
     * Rest all will be set in create method
     *
     */
    public function setAttr($post){

        $profile = $this->profile;
        
        $this->email = isset($post['email'])?strtolower($post['email']):$this->email;
       
        if(isset($post['transaction_ip']) && !empty($post['transaction_ip'])){
            $this->transaction_ip = $post['transaction_ip'];
        }
        else{
            if(isset($post['registration_ip']) && !empty($post['registration_ip'])){
                $this->transaction_ip = $post['registration_ip'];
            }
            else{
                $this->transaction_ip = $this->userIpAddress();
            }
        }

        $this->user_device = isset($post['user_device'])?$post['user_device']:$this->PanelistUserDevice;
       
        $this->google_id = isset($post['google_id'])?$post['google_id']:$this->google_id;
        $this->facebook_id = isset($post['facebook_id'])?$post['facebook_id']:$this->facebook_id;

        if((!$this->password || empty($this->password) || $this->password=="") && isset($post['password']) && $post['password']!=""){
            $this->password = $post['password']; // We are not allowing user to update his password
            $this->setPassword($this->password);
            $this->generatePasswordResetToken();        
        } 

        // If there is new record that is to be inserted
        if($this->isNewRecord){
            $this->setPanelId();
            $this->hoa_id = isset($post['hoa_id'])?$post['hoa_id']:$this->hoa_id;
            $this->aid = isset($post['aid'])?(int)$post['aid']:$this->aid;
            $this->offer_id = isset($post['offer_id'])?(int)$post['offer_id']:$this->offer_id;
            $this->aff_sub = isset($post['aff_sub'])?$post['aff_sub']:"";
            $meta = $this->meta;
            $meta['registration_ip'] = isset($post['registration_ip'])?$post['registration_ip']:$this->panelistRegistrationIP;
            $this->meta = $meta;

            /***
             * Field mail_sent_count and welcome_email are used for cron.
             * There is no value from post request ; it will set as default.
            */
            $this->mail_sent_count = self::NO_MAIL_SENT;
            $this->welcome_email_sent = self::STATUS_INACTIVE;
            $this->bounce_email = self::EMAIL_NOT_BOUNCED;
            // When coreg login then we add  status_email , status_cashout and status_survey fields
            if(isset($post['source']) && $post['source'] == 'coreg'){
                $this->status_survey = self::STATUS_SURVEY_ACTIVE;
                $this->status_email = self::STATUS_EMAIL_ACTIVE;
                $this->status_cashout = self::STATUS_CASHOUT_ACTIVE;
            }
            $this->cshout_speeder = 0;
            $dailyStreak = [
                'level' => 0,
                'day' => 0,
                'daily_streak_date' => new \MongoDate(),
                'max_day' => 0,
                'max_day_date' => new \MongoDate()
            ];
            $this->daily_streak = $dailyStreak;
        }
        // If we are updating panelist
        else{

            // True sample Array
            $this->truesampleinfo = isset($post['truesampleinfo'])?$post['truesampleinfo']:$this->truesampleinfo;
            
            if($this->step==self::DEFAULT_STEP && isset($post['step']) && $post['step']==self::FIRST_STEP){
                /**
                 * If user is either from Google or Facebook we are not sending them PC email verification email.
                 * Bcoz they are already verified if they are coming from Facebook or Google.
                 */
                if($this->facebook_id == "" && $this->google_id == "" && !in_array($this->source,Yii::$app->params['notSendVerifyEmail'])){
                    $this->sendVerificationMail();
                } 
                $this->step = self::FIRST_STEP;
                $this->SOITransaction($post); 
            }
            else if($this->step==self::FIRST_STEP && isset($post['step']) && $post['step']==self::SECOND_STEP){
                $this->step = self::SECOND_STEP;
            } else if($this->step==self::SECOND_STEP && isset($post['step']) && $post['step']==self::THIRD_STEP){
                $this->step = self::THIRD_STEP;
                $this->ProfilingTransaction(); 
            } else if($this->step==self::THIRD_STEP && isset($post['step']) && $post['step']==self::FOURTH_STEP){
                $this->step = self::FOURTH_STEP;
            }

            // GET image details from POST/PUT request
            $avatar = UploadedFile::getInstanceByName('avatar');
            // If we have any image files to be uploaded
            if($avatar){
                $profile['avatar'] = $avatar;
            }
            
            /**
             * If the value of fraudscore is greater then or equal to 10 then the value of status_survey,
             * status_email and status_cashout will be false otherwise it will be true.
            */
            if(isset($post['truesampleinfo']['fraudscore']) && $post['truesampleinfo']['fraudscore'] >= Yii::$app->params['fraud_score_value']){

                $this->status_survey = self::STATUS_SURVEY_INACTIVE;
                $this->status_email = self::STATUS_EMAIL_INACTIVE;
                $this->status_cashout = self::STATUS_CASHOUT_INACTIVE;
                $this->status_email_updated_at = new \MongoDate();
                if(isset($post['status_email_updated_by'])){
                    $this->status_email_updated_by = $post['status_email_updated_by'];
                }else{
                    $this->status_email_updated_by = (Panel::isAdmin())?Panel::getPanelByToken()->_id:$this->panel['_id']->{'$id'};
                }
            }
            else if(isset($post['truesampleinfo']['fraudscore']) && !empty($post['truesampleinfo']['fraudscore'])){
               
                $this->status_survey = self::STATUS_SURVEY_ACTIVE;
                $this->status_email = self::STATUS_EMAIL_ACTIVE;
                $this->status_cashout = self::STATUS_CASHOUT_ACTIVE;
            }
            /**
             * Here we are updating user status_survey,status_email and status_cashout field.
            */
            else if(isset($post['status_survey']) || isset($post['status_email']) || isset($post['status_cashout'])){

                $this->status_survey = isset($post['status_survey'])?$post['status_survey']:$this->status_survey;
                if(isset($post['status_email'])){
                    
                    $this->status_email = $post['status_email'];
                    $this->status_email_updated_at = new \MongoDate();
                    if(isset($post['status_email_updated_by'])){
                        $this->status_email_updated_by = $post['status_email_updated_by'];
                    }else{
                        $this->status_email_updated_by = (Panel::isAdmin())?Panel::getPanelByToken()->_id:$this->panel['_id']->{'$id'};
                    }
                }
                $this->status_cashout = isset($post['status_cashout'])?$post['status_cashout']:$this->status_cashout;
            }
            

            if(isset($post['traffic_source']) && $post['traffic_source'] != "") {
                // We will update traffic source value if we have only numeric value in traffic_source
                if(is_numeric($post['traffic_source']))
                    $this->traffic_source = (int)$post['traffic_source'];
            }   
            else {
                
                $this->traffic_source = (int)$this->panelisttrafficsource;
            }

            $this->clubhouse_tour_review = isset($post['clubhouse_tour_review'])?$post['clubhouse_tour_review']:$this->clubhouse_tour_review;

            if(isset($post['truesample_score_version'])){
               $this->truesample_score_version = $post['truesample_score_version'];
            }

            if(isset($post['daily_streak'])){
                if(isset($post['daily_streak']['daily_streak_update']) && ($post['daily_streak']['daily_streak_update'] == true)) {
                    $this->daily_streak_update = true;
                }
            }
            $this->pwd_reset = isset($post['pwd_reset'])?$post['pwd_reset']:$this->pwd_reset; 

            if(isset($post['fb_click'])){
                $this->fb_clk_ovrly = new \MongoDate;
            }
            if(isset($post['fb_close'])){
                $this->fb_cls_ovrly = new \MongoDate;
            }
        }

        $profile['address']['street'] = isset($post['street'])?$post['street']:$this->panelistStreet;
        $profile['address']['city'] = isset($post['city'])?$post['city']:$this->panelistCity;
        $profile['address']['state'] = isset($post['state'])?$post['state']:$this->panelistState;
        $profile['address']['country'] = isset($post['country'])?$post['country']:$this->panelistCountry;
        $profile['address']['zipcode']= isset($post['zipcode'])?$post['zipcode']:$this->panelistZipcode;
        /* Get shortcode of country from countries collection*/
        if((isset($post['country'])) || (!isset($profile['address']['iso_code']))){
            $countryName = Countries::find()->select(["ShortCode"])->where(['_id'=>$profile['address']['country']])->One();
             $profile['address']['iso_code'] = $countryName['ShortCode'];
        }
        $profile['name']['first'] = isset($post['first_name'])?$post['first_name']:$this->panelistFirstName;
        $profile['name']['last'] = isset($post['last_name'])?$post['last_name']:$this->panelistLastName;
        
        $profile['gender'] = isset($post['gender'])?$post['gender']:$this->panelistGender;
        $profile['contact_no'] = isset($post['contact_no'])?trim($post['contact_no']):$this->panelistContactNo;
        $profile['dob'] = isset($post['dob'])?$post['dob']:$this->panelistDob;
        
        $this->profile = $profile;

        if(isset($post['notes'])){
            $notes = $this->notes;
            $notes['text'] = $post['notes'];
            $notes['date'] = $this->setCreatedAt();
            $this->notes = $notes;
        }
        

        // Allowing to update status only for active and inactive
        if(isset($post['status']) && $status = $post['status']){
            if(in_array($status,['st_act','st_slp'])) {

                // We will use 'unsubscribe' scenario if we are unsubscribing user or deleting user
                if($status == 'st_slp') {
                    $this->scenario = 'unsubscribe'; 
                    $this->unsubscribe_date = $this->setCreatedAt();
                    if(isset($post['unsubscribe_by'])){
                        $this->unsubscribe_by = $post['unsubscribe_by'];
                    
                    }else{
                        $this->unsubscribe_by = (Panel::isAdmin())?Panel::getPanelByToken()->_id:$this->panel['_id']->{'$id'};
                    }
                }

                // If we have multiple variable in post request then we will not update panelist status
                if(count($post) > 1) {
                    $this->status = $this->status;
                }
                $this->status = self::getInverseStatus($post['status']);
            }
        }
    }

    public function setMeta(){
        $meta = $this->meta;
        $meta['email_verified'] = Panelistmeta::EMAIL_NOT_VERIFIED;
        $meta['lastaction_at'] = $this->setLastActionAt();
        $meta['lastvisit_at'] = "";
        $meta['referral_code'] = $this->setReferralCode();
        $meta['referred_by'] = $this->setReferredBy();
        $meta['created_at'] = $this->setCreatedAt();
        $meta['user_device'] = $this->setUserDevice();
        $this->meta = $meta;        
    }

    // Set panelist true sample data
    public function setTrueSampleInfo(){
        $this->truesampleinfo;
    }


    /**
     * Logs in a user using the provided username and password.
     *
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            return Yii::$app->user->login($this->getPanelist(), 3600 * 24);
        } else {
            return false;
        }
    }

    

    public function afterLogin($login_ip,$device=""){
            $lastLogin = PanelistLoginAttempts::getLastLoginAttempts($this->_panelist->_id);

            $this->_panelist->setLastActionAt();
            $this->_panelist->setUpdatedAt();
            $meta = $this->_panelist->meta;
            $meta['lastvisit_at'] = $lastLogin['lastvisit_at'];
            $meta['lastvisit_ip'] = $lastLogin['lastvisit_ip'];
            $meta['user_device'] = isset($lastLogin['user_device'])?$lastLogin['user_device']:"";
            $this->_panelist->meta = $meta;
            PanelistLoginAttempts::saveLoginAttempts($this->_panelist->_id,$login_ip,$device);
            $this->_panelist->save(false);
            return $this->_panelist;
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getPanelist()
    {
        if ($this->_panelist == false) {
            $panelists = self::find()->where(["email"=>$this->email,'status'=>['$ne' => Panelist::STATUS_DELETED]])->all();
            foreach ($panelists as $key => $panelist) {
                // We are showing only associate panelist with panel_token.
                if($panelist->panel->panel_token==Panel::getPanelToken() || Panel::isAdmin()){
                    $this->_panelist = $panelist;
                    return $this->_panelist;
                }
            }
        }
        return false;
    }


    public function sendNewPasswordMail(){


        if (!empty($this->getPanelist())) {
            if (!self::isPasswordResetTokenValid($this->_panelist->password_reset_token)) {
                $this->_panelist->generatePasswordResetToken();
            }
            $this->_panelist->scenario = 'panelist';
            $username = '';
            if(isset($this->profile['name']['first']) || isset($this->profile['name']['last'])){
                $username = $this->profile['name']['first']." ".$this->profile['name']['last'];
            }

            // Setting random password
            $randomPassword = $this->generateRandomString(8);
            if ($this->_panelist->updatePassword($randomPassword)) {
                return \Yii::$app->mailer->compose(['html' => 'newPassword-html', 'text' => 'newPassword-text'], ['user' => $this->_panelist,'password'=>$randomPassword])
                    ->setFrom([\Yii::$app->params['supportEmail'] => $this->_panelist->panel->name])
                    ->setTo([$this->email=>$username])
                    ->setReplyTo([\Yii::$app->params['supportEmail'] => $this->_panelist->panel->name])
                    ->setSubject('Password reset for ' . $this->_panelist->panel->name)
                    ->send();
            }
        }

        return false;
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['panelist.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        return $timestamp + $expire >= time();
    }


    /** Below are the 5 abstract method of user Interface **/

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }


    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['access_token' => $token]);
        // throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }


    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public function getPanelistByEmail($email){
        $this->email = $email;
        return $this->getPanelist();
    }

    /**
    * Method  is used for find  panelist by email (LIKE). 
    */
    public function loginPanelistByEmail($email){
        // i -- Case insensitivity to match upper and lower cases.
        if ($this->_panelist == false && $email) {
            $panelists = self::find()->where(['email' => ['$regex' => $email ,'$options' => 'i'] ,'status'=>['$ne' => Panelist::STATUS_DELETED]])->all();
            foreach ($panelists as $key => $panelist) {
                // We are showing only associate panelist with panel_token.
                if($panelist->panel->panel_token==Panel::getPanelToken() || Panel::isAdmin()){
                    $this->_panelist = $panelist;
                    return $this->_panelist;
                }
            }
        }
        return false;

    }


    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->_panelist->password);
    }

    public function getNextQuestionId(){
        $answers = self::getAnswers();
        $panel = new Panel;
        $questions = $panel->getQuestions();
        // No answer has given
        if(empty($answers)){
            $nextQues = array_shift(array_slice($questions, 0, 1));
            return $nextQues;
        }
        else{
            
        }
    }

    public function getAnswers(){
        $panelist = self::getPanelistById();
        if(isset($panelist->answers)){
            return $panelist->answers;
        }

        return [];
    }

    public function getPanelistById($panelistId=null){

        if(is_object($panelistId)){
            return self::find()->where(['_id'=>$panelistId,'status'=>['$ne' => Panelist::STATUS_DELETED]])->One();
        }
        else{
            return self::find()->where(['id'=>(int)(isset($panelistId)?$panelistId:Panelist::getPanelistId()),'status'=>['$ne' => Panelist::STATUS_DELETED]])->One();
        }
    }

    public static function getPanelistId(){
        return isset(Yii::$app->request->queryParams['id'])?Yii::$app->request->queryParams['id']:null;
    }

    /**
     * Validate Panelist
     *
     */
    public function validatePanelist($panelist){
        // We are showing only associate panelist with panel_token.
        if($panelist->panel->panel_token!=Panel::getPanelToken() && !Panel::isAdmin()){
            Yii::$app->response->statusCode = 404; // Status 404 is for Not Found
            Yii::$app->end();
        }
        return true;
    }

    public function updatePassword($newPassword){
        $this->setPassword($newPassword);
        return $this->save();
    }


    /**
     * Make a response to answer api which need to send to panel
     *
     */
    public function sendQuestionAnswers(){
        // Fetch panelist's answers if any
        $userAnswers = isset($this->profile['answers'])?$this->profile['answers']:array();
        
        
        // Generate response 
        $qaArr = [];
        $qaArrNotInPanel = [];
        foreach ($this->panel->panel_questions as $key => $value) {
            $options = [];
            if(!empty($value['options'])){
                foreach ($value['options'] as $key3 => $value3) {
                        $options[$key3]['option_text'] = $value3['OptionText'];
                        $options[$key3]['option_id'] = $value3['id'];
                        $options[$key3]['selectOnlyOption'] = $value3['selectOnlyOption'];  /* we get selectOnlyOption from admin*/ 
                }
            }
            $qaArr[$key]['question_type'] = $value['question_type'];
            $qaArr[$key]['question_text'] = $value['question_text'];
            $qaArr[$key]['question_key'] = $value['question_key'];
            $qaArr[$key]['question_id'] = $value['question_id'];
            $qaArr[$key]['category_id'] = $value['category_id'];
            $qaArr[$key]['category_text'] = $value['category_text'];                     
            $qaArr[$key]['options'] = $options;
            foreach ($userAnswers as $key2 => $value2) {
                // Match with panel questions and user's question in post request
                if($value['question_id']==$value2['question_id'] && strtoupper($value['question_key'])==strtoupper($value2['question_key'])){
                    // Collect all options in a question
                    $qaArr[$key]['user_answers'] = $value2['options'];
                }
                else {  
                    $qaArrNotInPanel[$key]['question_type'] = isset($value2['question_type'])?$value2['question_type']:"";
                    $qaArrNotInPanel[$key]['question_text'] = $value2['question_text'];
                    $qaArrNotInPanel[$key]['question_key'] = $value2['question_key'];
                    $qaArrNotInPanel[$key]['question_id'] = $value2['question_id'];
                    $qaArrNotInPanel[$key]['category_id'] = $value2['category_id'];
                    $qaArrNotInPanel[$key]['category_text'] = $value2['category_text'];                     
                    $qaArrNotInPanel[$key]['options'] = "";
                    $qaArrNotInPanel[$key]['user_answers'] = $value2['options'];
                }
            }
        }
        // Get unique Array 
        $qaArrNotInPanel = array_unique($qaArrNotInPanel, SORT_REGULAR);

        // Merge both panel and non panel questions
        $qaArr = array_merge($qaArr,$qaArrNotInPanel);

        return $qaArr;
    }


    public function getResponse(){
        
       // Changing status to readable format i.e st_act,st_slp 
        $status = Panelist::getStatus($this->status);
        return [
                    'id'=>$this->id,
                    'enc_pid'=>$this->_id->{'$id'},
                    'panel_id'=>$this->panel['id'],
                    'email'=>$this->email,
                    'mail_sent_count'=>$this->mail_sent_count,
                    'welcome_email_sent'=>$this->welcome_email_sent,
                    'bounce_email'=>$this->bounce_email,
                    'first_name'=>$this->panelistFirstName,
                    'last_name'=>$this->panelistLastName,
                    'gender'=>$this->panelistGender,
                    'dob'=>$this->panelistDob,
                    'points'=>isset($this->points)?$this->points:0,
                    'lifetime_points'=>$this->panelistLifetimePoints,
                    'contact_no'=>$this->panelistContactNo,
                    'street'=>$this->panelistStreet,
                    'city'=>$this->panelistCity,
                    'country'=>$this->panelistCountry,
                    'iso_code'=>$this->PanelistISOCode,
                    'state'=>$this->panelistState,
                    'status'=>$status,
                    'traffic_source'=>$this->panelisttrafficsourceInfo,
                    'status_cashout'=>$this->status_cashout,
                    'status_email'=>$this->status_email,
                    'status_survey'=>$this->status_survey,
                    'step'=>$this->step,
                    'source'=>($this->source)?$this->source:"",
                    'zipcode'=>$this->panelistZipcode,
                    'hoa_id'=>($this->hoa_id)?$this->hoa_id:"",
                    'aid'=>($this->aid)?$this->aid:"",
                    'offer_id'=>($this->offer_id)?$this->offer_id:"",
                    'aff_sub'=>($this->aff_sub)?$this->aff_sub:"",
                    'google_id'=>($this->google_id)?$this->google_id:"",
                    'facebook_id'=>($this->facebook_id)?$this->facebook_id:"",
                    'supplier_id'=>$this->panelistSupplierId,
                    'answers'=>$this->panelistAnswers,
                    'card'=>$this->panelistCard,
                    'auth_key'=>$this->auth_key,
                    'avatar'=>$this->panelistAvatar,
                    'isPasswordSet'=>($this->password)?'true':'false',
                    'email_verified'=>Panelistmeta::getStatus($this->panelistEmailVerified),
                    'surveys_attended'=>($this->surveys_attended)?$this->surveys_attended:0,
                    'surveys_completed'=>($this->surveys_completed)?$this->surveys_completed:0,
                    'referral_code'=>$this->panelistReferralCode,
                    'user_device'=>$this->PanelistUserDevice,
                    'referred_by'=>$this->panelistReferredBy,
                    'created_at'=>$this->panelistCreatedAt,
                    'lastaction_at'=>$this->PanelistLastActionAt,
                    'updated_at'=>$this->PanelistUpdatedAt,
                    'lastvisit_at'=>$this->panelistLastVisitAt,
                    'lastvisit_ip'=>$this->panelistLastVisitIp,
                    'registration_ip'=>$this->panelistRegistrationIP,
                    'truesampleinfo'=>$this->truesampleinfo,
                    'sweeps_code'=>$this->sweeps_code,
                    'clubhouse_tour_review'=>$this->clubhouse_tour_review,
                    'truesample_score_version'=>isset($this->truesample_score_version)?$this->truesample_score_version:0,
                    'daily_streak'=>isset($this->daily_streak)?$this->daily_streak:0,
                    'notes'=> $this->PanelistNotes,
                    'last' => isset($this->last)?$this->last:"",
                    'unsubscribe_date' => isset($this->unsubscribe_date)?$this->unsubscribe_date:"",
                    'unsubscribe_by' => isset($this->unsubscribe_by)?$this->unsubscribe_by:"",
                    'status_email_updated_at'=>isset($this->status_email_updated_at)?$this->status_email_updated_at:"",
                    'status_email_updated_by'=>isset($this->status_email_updated_by)?$this->status_email_updated_by:"",
                    'email_verf_snd'=>isset($this->email_verf_snd)?$this->email_verf_snd:"",
                    'cshout_speeder'=>isset($this->cshout_speeder)?$this->cshout_speeder:0,
                    'pwd_reset'=>isset($this->pwd_reset)?$this->pwd_reset:"",
                    'sur_speeder_cnt'=> isset($this->sur_speeder_cnt)?$this->sur_speeder_cnt:"",
                    'invalid_txn_cnt'=> isset($this->invalid_txn_cnt)?$this->invalid_txn_cnt:"",
                    'fb_clk_ovrly'=> isset($this->fb_clk_ovrly)?$this->fb_clk_ovrly:"",
                    'fb_cls_ovrly'=> isset($this->fb_cls_ovrly)?$this->fb_cls_ovrly:"",
            ];
    }

    /**
     * getResponseGetCount Response for GetCount
     * @return Array Response Array
     */
    public function getResponseGetCount() {        
       // Changing status to readable format i.e st_act,st_slp 
        $status = Panelist::getStatus($this->status);
        return [
                    'id'=>$this->id,
                    'enc_pid'=>$this->_id->{'$id'},
                    'panel_id'=>$this->panel['id'],
                    'email'=>$this->email,
                    'first_name'=>$this->panelistFirstName,
                    'last_name'=>$this->panelistLastName,
                    'status'=>$status,
                    'supplier_id'=>$this->panelistSupplierId,
                    'auth_key'=>$this->auth_key,
                    'created_at'=>$this->panelistCreatedAt,
                    'lastvisit_at'=>$this->panelistLastVisitAt,
            ];
    }

    public function sendVerificationMail(){
        $username = '';
        if(isset($this->profile['name']['first']) || isset($this->profile['name']['last'])){
           $username = $this->profile['name']['first']." ".$this->profile['name']['last'];
        }
        

        return \Yii::$app->mailer->compose(['html' => 'verifyEmail-html', 'text' => 'verifyEmail-text'], ['user' => $this])
                ->setFrom([\Yii::$app->params['supportEmail'] => $this->panel->name])
                ->setTo([$this->email=>$username])
                ->setReplyTo([\Yii::$app->params['supportEmail'] => $this->panel->name])
                ->setSubject('What are you waiting for, '.$this->profile['name']['first'].'... Confirm your email')
                ->send();
    }

   
    public function setEmailVerified(){
        if(!$this->panelistEmailVerified){
            $meta = $this->meta;
            $meta['email_verified'] = Panelistmeta::EMAIL_VERIFIED;
            $this->meta = $meta;
            $this->save();
            
            if($this->panel->show_psu == 'true' && isset($this->status_email) && $this->status_email == true && $this->profile['address']['country'] == 'United States'){
                
                    $this->sendPaidSurveyMail();
               
            }
            if($this->panel->email_verification_bonus && $this->panel->email_verification_bonus>0){
                $transaction = new PanelistTransaction;
                $data['panel_id'] = $this->panel->id;
                $data['type'] = 'Email Verification Bonus';
                $data['message'] = 'Point Bonus on email verification';
                $data['identifier'] = $this->generateRandomString(32);
                $data['transaction_status'] = 'Complete';
                $data['transaction_ip'] = $this->transaction_ip;
                $data['points'] = $this->panel->email_verification_bonus;
                $transaction->createTransaction($this, $data);
            }
        }
    }

    /**
     * Sending email for Paid Survey Update (PSU) Invitation Email
     */
    public function sendPaidSurveyMail() {

        if (!empty($this->getPanelist())) {

            $username = '';
            if(isset($this->profile['name']['first']) || isset($this->profile['name']['last'])){
                $username = $this->profile['name']['first']." ".$this->profile['name']['last'];
            }

            return \Yii::$app->mailer->compose(['html' => 'PSUwelcome-html'], ['user' => $this->_panelist])
                ->setFrom([\Yii::$app->params['supportEmail'] => $this->_panelist->panel->name])
                ->setTo([$this->email => $username])
                ->setReplyTo([\Yii::$app->params['supportEmail'] => $this->_panelist->panel->name])
                ->setSubject('A special offer to our new PointClub members')
                ->send();
        }

        return false;
    }

    /**
     * match panelist panel and given panel token
     * if they match successfully that means panelist is valid
     * With this check valid panelist can only see its transaction activities.
     */
    public function matchPanelistWithPanelToken(){
        return ($this->panel->panel_token==Panel::getPanelByToken()->panel_token || Panel::isAdmin());
    }

    public function getPanelistFullName(){
        if(isset($this->profile['name']['first']) && isset($this->profile['name']['last'])){
            return $this->profile['name']['first']." ".$this->profile['name']['last'];
        }
        return "";
    }

    public function getTransactions(){
        return $this->hasMany(PanelistTransaction::className(), ['panelist_id' => '_id']);
    }

    public function isFirstSurvey($type){
        $transactions = PanelistTransaction::find()->where(['type'=>$type,'panelist_id'=>$this->_id,'transaction_status'=>PanelistTransaction::TRANSACTION_STATUS_COMPLETE])->all();
        if(count($transactions)==1){
            return true;
        }
        return false;
    }

    /**
     * If there are answers mapped in default answer collections
     * then safe those answers in panelist's answers collections 
     * so we don't ask them again at the time when user attempt those questions.
     *
     */
    public function saveDefaultAnswers(){
        $defaultAnswers = DefaultAnswers::find()->where(['panel_id'=>$this->panel->id])->All();

        // Checking if there are any default answer present
        if(!empty($defaultAnswers)){
            $panelQuestions = $this->panel->panel_questions;

            // Checking if there are any panel question exists
            if(!empty($panelQuestions) || $panelQuestions!=""){
                foreach ($defaultAnswers as $key => $default) {
                    $i=0;
                    foreach ($panelQuestions as $question) {

                        // If question key present in default question collection match with panel's question key
                        if($default['question_key']==$question['question_key']){

                            // If there is no value
                            if(!is_array($default['answer_field']) && isset($this->profile[$default['answer_field']]) && $this->profile[$default['answer_field']]==""){
                                continue;
                            }
                            $options = [];

                            // For Open Ended questions
                            if(empty($question['options'])){

                                // For AGE we need to calculate age from dob
                                if($question['question_key']=='AGE'){
                                    $options[$i]['option_text'] = (int)self::getAge($this->profile['dob']);
                                }
                                else{
                                    $options[$i]['option_text'] = isset($this->$default['answer_field'])?$this->$default['answer_field']:(isset($this->profile[$default['answer_field']])?$this->profile[$default['answer_field']]:(isset($this->profile['address'][$default['answer_field']])?$this->profile['address'][$default['answer_field']]:""));
                                }
                            }
                            // For single punch and multipunch question
                            else{

                                $profile = null;
                                $answerAdded = false;
                                // If question key directly match in profile document
                                if(isset($this->profile[strtolower($question['question_key'])])){
                                    $profile = $this->profile[strtolower($question['question_key'])];
                                }
                                // else check if question match in profile->address document
                                else if(isset($this->profile['address'][strtolower($question['question_key'])])){
                                    if($this->profile['address'][strtolower($question['question_key'])]==""){
                                        continue;
                                    }
                                    /**
                                     * We have states in lower case but in question's options states are coming in upper case
                                     * thats why we have added strtoupper
                                     */
                                    $profile = strtoupper($this->profile['address'][strtolower($question['question_key'])]);
                                }

                                // This condition will only get true when these is any value for $profile
                                if($profile){
                                    $defaultOption = [];
                                    foreach ($question['options'] as $option) {
                                        if(in_array($option['OptionText'], $default['answer_field']) 
                                            && $profile==$option['OptionText']
                                        ){
                                            $options[$i]['option_text'] = $option['OptionText'];
                                            $options[$i]['option_id'] = (int)$option['id'];
                                            $answerAdded = true;
                                            break;

                                        }
                                        // Getting Default answer
                                        else if($option['OptionText']=="NOT APPLICABLE"){
                                                $defaultOption['option_text'] = $option['OptionText'];
                                                $defaultOption['option_id'] = (int)$option['id'];
                                        }
                                    }

                                    // Adding Default answer only when there is no matched answer 
                                    if(!$answerAdded && !empty($defaultOption)){
                                        $options[] = $defaultOption;
                                    }
                                }
                            }

                            $profile = $this->profile;
                            $profile['answers']['category_id'] = $question['category_id'];
                            $profile['answers']['question_id'] = $question['question_id'];
                            $profile['answers']['question_key'] = $question['question_key'];
                            $profile['answers']['options'] = $options;
                            $this->profile = $profile;
                            $this->addAnswer();
                            break;
                            $i++;
                        }
                    }
                }
            }
        }
    }

    // Calculate the age from date of birth
    public static function getAge($then) {
        $then = date('Ymd', strtotime(str_replace('-', '/', $then)));
        $diff = date('Ymd') - $then;
        return substr($diff, 0, -4);
    }

    /**
     * Update panelist points.
     * @param  int $points points to update
     * @return boolean         [description]
     * Get lifetime points of panelist according to give transaction type, If transaction type match then we add points
     */
    public function updatePoints($points,$type){
        if(isset($this->points)){
           $this->points = (int)($this->points+$points);
            if(in_array($type, Yii::$app->params['transaction_type'])) {
                if($type == "Innovate Surveys" && $points < 0){
                    $this->lifetime_points = (int)($this->lifetime_points+$points);  
                }else{
                    $this->lifetime_points = (int)($this->lifetime_points+abs($points));
                }   
            }
        }
        else{
            $this->lifetime_points = $points;
            $this->points = $points;
        }
        $this->save(false); 
    }

    public function removeCard(){
        $this->scenario = 'card';
        $this->card = "";
        $this->save();
    }
    /***
     * This method is used for genrate new has offer id for panelist.
    */
    public function genrateHasofferId($offer_id,$aid){

        $hasOfferServerSideURL = Yii::$app->params['hasOfferServerSideURL'].'?offer_id='.$offer_id.'&aff_id='.$aid.'&format=json';
        $hasOfferServerSideCurl = new curl\Curl();
        $hasOfferServerSideCurl->setOption(CURLOPT_RETURNTRANSFER,true);
        $hasOfferresponse = $hasOfferServerSideCurl->get($hasOfferServerSideURL);

        if($hasOfferServerSideCurl->responseCode == 200){
            if(!empty($hasOfferresponse)){
                $hasOfferServerSideResponse = json_decode($hasOfferresponse);
                return $hasOfferServerSideResponse->response->data->transaction_id;
            }
        }
        else{
            return "";
        }
    }

    public function purgePanelist(){

        $panelist_transaction = PanelistTransaction::deleteAll(["panelist_id"=>$this->_id]);
        $panelist_login_attempts = PanelistLoginAttempts::deleteAll(["panelist_id" => $this->_id]);

        $card_redemption = CardRedemption::deleteAll(["panelist_id"=>$this->_id]);

        return[
            'panelist_id'=>$this->id,
            'panelist_transaction' => $panelist_transaction,
            'panelist_login_attempts' => $panelist_login_attempts,
            'card_redemption' => $card_redemption,
            'panelist' =>$this->delete()
        ]; 
    }
    /**
     * Sending email to those users whose forgot link generated by admin
    */
    public function sendForgotLinkMail($forgotLinkURL){
        $username = '';
        if(isset($this->profile['name']['first']) || isset($this->profile['name']['last'])){
           $username = $this->profile['name']['first']." ".$this->profile['name']['last'];
        }
        return \Yii::$app->mailer->compose(['html' => 'forgotLink-html'], ['user' => $this->panelist,'url'=>$forgotLinkURL])
            ->setFrom([\Yii::$app->params['supportEmail'] => $this->panel->name])
            ->setTo([$this->email=>$username])
            ->setReplyTo([\Yii::$app->params['supportEmail'] => $this->panel->name])
            ->setSubject('Please reset your password')
           ->send();   
   }
}
