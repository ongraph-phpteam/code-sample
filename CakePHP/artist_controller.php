<?php 
class ArtistController extends AppController 
{ 
var $name = 'Artist'; 
var $helpers = array('Html','Form', 'Time', 'Number','javascript','ajax','Paginator'); 
	
	function index()
    {
    $this->layout = "user_default";
	$url = (!empty($_SERVER['HTTPS'])) ? "https://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'] : "http://".$_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'];
	$checkUrl = strpos($url,'?');
	if($checkUrl === false) {
	 $url;
	} else {
	 $urlEx = explode("?",$url);
	 $url = $urlEx[0];
	}
	$artist_subdomain = explode('/',$url);
	$count = count($artist_subdomain);
	$subdomain_url = $artist_subdomain[$count-1]; 
    $subdomain  = explode(".",$subdomain_url);
    $subdomain = $subdomain[0];	
	$query2 = mysql_query("select * from `artists` where `SUBDOMAIN`='$subdomain' OR `ID`='$subdomain'");
	$rowCount = mysql_num_rows($query2); 
	if($rowCount > 0) {
						while($row2 = mysql_fetch_array($query2))
						{
						    $artist_lname =  $row2['LASTNAME'];
							$artist_fname =  $row2['FIRSTNAME']; 
							$art_id = $row2['ID'];
						}
		App::import('Model', 'Artist');
		$this->Artist = new Artist;
		$this->set('artistdata', $this->Artist->read(null, $art_id));
		
		
		$artist_data = $this->Artist->read(null, $art_id);
		$artistFN = $artist_data['Artist']['FULL_NAME'];
		if($artistFN == "" ) {
			$artistFN = $artist_data['Artist']['FIRSTNAME']." ".$artist_data['Artist']['LASTNAME'];;
		} else {
			// do nothing
		}
		$checkOfferRequest = mysql_query("select * from `exhibition_artworks` where (`TYPE`='Offer' || `TYPE`='Request') AND `STATUS`='Online' AND `ARTIST_ID`=$art_id");
		$count = mysql_num_rows($checkOfferRequest);
		if($count > 0) {
			$addIt = " &#8211; artwork for sale";
			$addItt = ", ".$artistFN." artwork for sale";
		} else {
			$addIt = "";
			$addItt = "";
		}
		
		
		
		/***********getting group of artist start ************/
						
		$co_to_id = $art_id;
		$get_group_query = mysql_query("SELECT count(*) as groupcount FROM exhibition_artists as ea 	JOIN exhibitions as e ON e.ID = ea.EXHIBITION_ID where ea.ARTIST_ID='$co_to_id' AND e.GROUPFLAG =1") OR die(mysql_error());
		$fetch_group_data = mysql_fetch_assoc($get_group_query);
		$group_exbitions = $fetch_group_data['groupcount'];
		/***********getting group of artist end ************/
		/***********getting solo of artist start ************/
		$get_solo_query = mysql_query("SELECT count(*) as solocount FROM exhibition_artists as ea		JOIN exhibitions as e ON e.ID = ea.EXHIBITION_ID where ea.ARTIST_ID='$co_to_id' AND e.GROUPFLAG	=0") OR die(mysql_error());
		$fetch_solo_data = mysql_fetch_assoc($get_solo_query);
		$solo_exbitions = $fetch_solo_data['solocount'];
		/***********getting solo of artist end ************/
		/*total exbitions in artist info start**/
		$total_exbitions_in_artistinfo = $group_exbitions+$solo_exbitions;
		/*total exbitions in artist info end**/
		
			/****for G option artist info records start**********/
				$all_exbition_id = mysql_query("select EXHIBITION_ID from  exhibition_artists where ARTIST_ID ='$co_to_id'");
				$starting_year = array();
				$ending_year = array();
				while($all_records_of_exbition = mysql_fetch_array($all_exbition_id))
				{
					$exhibition_id = $all_records_of_exbition['EXHIBITION_ID'];
					$getting_year = mysql_query("select BEGIN_YEAR,BEGIN_MONTH,END_YEAR,END_MONTH from exhibitions where ID ='".$exhibition_id."'");
					while($all_record_date = mysql_fetch_array($getting_year))
					{
						array_push($starting_year,strtotime('01'.'-'.$all_record_date['BEGIN_MONTH'].'-'.$all_record_date['BEGIN_YEAR']));
						array_push($ending_year,strtotime('01'.'-'.$all_record_date['END_MONTH'].'-'.$all_record_date['END_YEAR']));
					}
				}
				$maximum_ending_year =   date('M Y',(max($ending_year)));
				$minimum_starting_year =   date('M Y',(min($starting_year)));
				//echo $maximum_ending_year.'<br>'.$minimum_starting_year;die;
			/****for G option artist info records end**********/
		
		
		$page_des = $artistFN.$addIt.": ".$total_exbitions_in_artistinfo." exhibitions from ".$minimum_starting_year." - ".$maximum_ending_year.", exhibition venues worldwide of artist ".$artistFN.$addIt.", Exhibition History, Summary of artist-info.com records, Solo/Group Exhibitions, Visualization, Biography, Artist-Portfolio, Artwork Offers, Artwork Requests, Exhibition Announcements";
		
	$this->set('description_for_layout',$page_des);
   // $this->set('keywords_for_layout',$page_keyword);
	$this->set('total_exbitions_in_artistinfo',$total_exbitions_in_artistinfo);
	$this->set('solo_exbitions',$solo_exbitions);
	$this->set('group_exbitions',$group_exbitions);
	$this->set('maximum_ending_year',$maximum_ending_year);
	$this->set('minimum_starting_year',$minimum_starting_year);
		
		
    } else {
	$this->redirect(array('controller' => 'users','action' => 'main_home'));
	}
 }
}
?>