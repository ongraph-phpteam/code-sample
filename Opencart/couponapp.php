<?php
class ControllerApiCouponApp extends Controller {
	public function index() {
		
		// Load and get coupan data
		$this->load->language('api/couponapp');
		$coupon = $this->request->post['coupon'];
		$customer_id = $this->request->post['customer_id'];
		$cart_total = $this->request->post['cart_total'];
		
		// If no data in coupan,customerId and cartTotal then add errors 
		$error_list = array();       
		if (! $coupon) { array_push($error_list, "Missing Coupon"); }
		if (! $customer_id) { array_push($error_list, "Missing Customer Id"); }
		if (! $cart_total) { array_push($error_list, "Missing Cart Total Amount"); }
		
		// If any errors then return data with error message
		if ( ! empty($error_list) ){
			header("HTTP/1.1 400 OK");
			$return   = array("status"=>"false","message"=>"Parameters missing","data"=>$error_list);
			echo json_encode($return);
		}else{
			// If all required parameters is ok
			$coupon_data = "SELECT * FROM `oc_coupon` WHERE code = '".$coupon."'";
			$query_brand = $this->db->query($coupon_data);
			
			// Check if coupon exists
			if($query_brand->num_rows >= 1){
				$coupon_info = $query_brand->rows[0];
				if(!empty($coupon_info)){
					$coupondata = array();
					$current_date = date("Y/m/d");
						
					$start_date = $coupon_info['date_start'];
					$end_date = $coupon_info['date_end'];
					$start_ts = strtotime($start_date);
					$end_ts = strtotime($end_date);
					$user_ts = strtotime($current_date);
					
					// Check coupon date is expired or not
					if(($user_ts >= $start_ts) && ($user_ts <= $end_ts)){
						$coupon_id = $coupon_info['coupon_id'];
						// Check coupon usable limit
						if($coupon_info['uses_total'] >= 1){
							$coupon_check = "SELECT* FROM `oc_coupon_history` WHERE coupon_id = '".$coupon_id."'";
							$query_brandfff = $this->db->query($coupon_check);						
							$newQuery = $query_brandfff->rows;
							$numrows = $query_brandfff->num_rows;
							// check coupon expired or not
							if($numrows < $coupon_info['uses_total']){
								
								$coupon_user_check = "SELECT* FROM `oc_coupon_history` WHERE coupon_id = '".$coupon_id."' AND customer_id = '".$customer_id."'";
								$user_cou = $this->db->query($coupon_user_check);						
								$rrr = $user_cou->rows;
								$user_numrows = $user_cou->num_rows;
								
								// check specific user coupon limit
								if($coupon_info['uses_customer'] > $user_numrows){
									if($cart_total >= $coupon_info['total']){
										//Coupon valid
										$return  = array("status"=>true, "message"=>"Coupon Valid", "discount_price"=>(float)$coupon_info['discount'], "coupon_name"=>$coupon_info['name'], "coupon_id"=>$coupon_id,"coupon_code"=>$coupon_info['code']);
									}else{
										// coupon error
										$return  = array("status"=>false, "message"=>"The amount of cart must be reach ".(float)$coupon_info['total']." before using the coupon", "discount_price"=>(float)$coupon_info['discount'], "coupon_name"=>$coupon_info['name']);
									}	
								}else{
									//user limit complete
									$return  = array("status"=>false, "message"=>"User Coupon limit Complete", "discount_price"=>(float)$coupon_info['discount'], "coupon_name"=>$coupon_info['name']);
								}
							}else{
								//coupon expired 10 user already applied
								$return  = array("status"=>false, "message"=>"Coupon Expired", "discount_price"=>(float)$coupon_info['discount'], "coupon_name"=>$coupon_info['name']);
							}
						}else{
							// Backend validation
							$return  = array("status"=>false, "message"=>"Coupon valid but it's 0 times usable", "discount_price"=>(float)$coupon_info['discount'], "coupon_name"=>$coupon_info['name']);
						}
					}else{
						// coupon expired by date
						$return  = array("status"=>false, "message"=>"Coupon Date Expired", "discount_price"=>(float)$coupon_info['discount'], "coupon_name"=>$coupon_info['name']);
					}
				}
			}else{
				// Adding coupan not exists message 
				$return  = array("status"=>false, "message"=>"Coupon doesn't exists");
			}
			header("HTTP/1.1 200 OK");	
			echo json_encode($return);
		}
	}
}
