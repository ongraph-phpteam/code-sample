<?php
class ControllerApiAffiliatelogin extends Controller {
    private $error = array();

    public function index() {

		// Load affiliate
        $this->load->language('affiliate/login');
        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('affiliate/affiliate');

		// Check email and password validation
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && isset($this->request->post['email']) && isset($this->request->post['password']) && $this->validate()) {
            // Add to activity log
            if ($this->config->get('config_customer_activity')) {
                $this->load->model('affiliate/activity');
                $activity_data = array(
                    'affiliate_id' => $this->affiliate->getId(),
                    'name'         => $this->affiliate->getFirstName() . ' ' . $this->affiliate->getLastName(),
                    'phone'        => $this->affiliate->getTelephone(),
                    'address'        => $this->affiliate->getAddress(),
                    'zone'        => $this->affiliate->getZone(),
                );
                $this->model_affiliate_activity->addActivity('login', $activity_data);
            }
        }

		// For error or warning
        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

		// If redirect set in post then redirect else create session
        if (isset($this->request->post['redirect'])) {
            $data['redirect'] = $this->request->post['redirect'];
        } elseif (isset($this->session->data['redirect'])) {
            $data['redirect'] = $this->session->data['redirect'];

            unset($this->session->data['redirect']);
        } else {
            $data['redirect'] = '';
        }

		// Check for success
        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

		// Get email
        if (isset($this->request->post['email'])) {
            $data['email'] = $this->request->post['email'];
        } else {
            $data['email'] = '';
        }
		// Get password
        if (isset($this->request->post['password'])) {
            $data['password'] = $this->request->post['password'];
        } else {
            $data['password'] = '';
        }

		// Set the data
        $data['affiliate_id'] = $this->affiliate->getId();
        $data['affiliate_name'] = $this->affiliate->getFirstName(). " ". $this->affiliate->getLastName();
        $data['tacking_code'] = $this->affiliate->getCode();
        $data['phone'] = $this->affiliate->getTelephone();
        $data['address'] = $this->affiliate->getAddress();
        $data['zone'] = $this->affiliate->getZone();

        echo json_encode($data);
    }

	//  Function for validation
    protected function validate() {
        // Check how many login attempts have been made.
        $login_info = $this->model_affiliate_affiliate->getLoginAttempts($this->request->post['email']);

        if ($login_info && ($login_info['total'] >= $this->config->get('config_login_attempts')) && strtotime('-1 hour') < strtotime($login_info['date_modified'])) {
            $this->error['warning'] = $this->language->get('error_attempts');
        }

        // Check if affiliate has been approved.
        $affiliate_info = $this->model_affiliate_affiliate->getAffiliateByEmail($this->request->post['email']);

        if ($affiliate_info && !$affiliate_info['approved']) {
            $this->error['warning'] = $this->language->get('error_approved');
        }

        if (!$this->error) {
            if (!$this->affiliate->login($this->request->post['email'], $this->request->post['password'])) {
                $this->error['warning'] = $this->language->get('error_login');

                $this->model_affiliate_affiliate->addLoginAttempt($this->request->post['email']);
            } else {
                $this->session->data['success'] = "success";
                $this->model_affiliate_affiliate->deleteLoginAttempts($this->request->post['email']);
            }
        }

        return !$this->error;
    }
}