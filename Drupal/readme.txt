sessions.install -> This file is run the first time when module is enabled, and is used to run setup procedures as required by the module and is creating database tables and fields.

sessions.info -> This file is used to declare module information like module version, dependencies etc.

sessions.module -> This is mail logical file of session module, it handle all business logic for recording GUAC session call and desply to Admin with appropriate result, also provide functionality to kill Running GUAC session from Admin screen.