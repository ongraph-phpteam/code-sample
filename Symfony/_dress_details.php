<div class="details">
	<h3><?php echo __('Sale details') ?></h3>
	<ul>
		<li>
			<h4><?php echo __('seller')?></h4>

			<div class="row">
				<div class="label"><?php echo __('dress_id') ?></div>

				<?php echo $dress->getId() ?>
			</div>

			<div class="row">
				<div class="label"><?php echo __('type_of_seller') ?></div>

				<?php echo __($dress->getUser()->getSellerType()->getName(),null,'traits') ?>
			</div>

			<?php if ('individual' != trim($dress->getUser()->getSellerType()->getName()) && $dress->getUser()->getShowName()) : ?>
			<div class="row">
				<div class="label"><?php echo __('name_of_seller') ?></div>

				<?php echo $dress->getUser()->getCompanyName() ?>
			</div>
			<?php endif ?>

			<?php if ('individual' == trim($dress->getUser()->getSellerType()->getName())) : ?>
					<?php if ($dress->getPurchaseYear()) : ?>
					<div class="row">
						<div class="label"><?php echo __('purchase_year') ?></div>

						<?php echo $dress->getPurchaseYear()?>
					</div>
                    <?php endif ?>
				<div class="row">
					<div class="label"><?php echo __('location') ?></div>

					<?php include_partial('dress/location', array('location' => $dress->getUser(), 'show_full_address' => true)) ?>
				</div>
			<?php else : ?>
				<div class="row">
					<div class="label"><?php echo __('locations') ?></div>

					<?php include_partial('dress/locations', array('locations' => $dress->myGetLocations())) ?>
				</div>
			<?php endif ?>

			<div class="row">
				<div class="label"><?php echo __('locals_can_try') ?></div>
				
				<?php $locations = $dress->myGetLocations() ?>
				<?php echo $locations[0]->getCanTry() ? __('yes') : __('no')?>
			</div>
		<?php
			$isincouture = $dress->getUser()->getInCouture();
			if($isincouture == 1)
			{
				if($dress->getDesigner()->getPhone() != '') :?>	
					<div class="row">
						<div class="label"><?php echo __('Phone:') ?></div>
						<?php echo $dress->getDesigner()->getPhone() ?>

					</div>
				<?php endif;
				if($dress->getDesigner()->getEmail() != '') :?>	
					<div class="row">
						<div class="label"><?php echo __('Email:') ?></div>
						<a href="mailto:<?php echo $dress->getDesigner()->getEmail()?>?subject=Inquiry / Anfrage MARRYJim Listing No.<?php echo $dress->getId()?>"><?php  echo $dress->getDesigner()->getEmail() ?></a>
					</div>
				<?php endif;
				$googlecampaigne = $dress->getDesigner()->getGoogleCampaigne();
				if($googlecampaigne != '')
				{
						if(strpos($googlecampaigne,"http") === false) {
							$googlecampaigne = "http://".$googlecampaigne;
						}
					?>	
						<div class="row">
							<div class="label"><?php echo __('Website:') ?></div>
							<a href="<?php echo $googlecampaigne;?>" class="link-red" target="_blank"><?php  echo $dress->getDesigner()->myGetWebsite() ?></a>
						</div>
					<?php	
				}
				else
				{
					?>	
						<div class="row">
							<div class="label"><?php echo __('Website:') ?></div>
							<a href="<?php echo $dress->getDesigner()->myGetWebsite()?>" class="link-red" target="_blank"><?php  echo $dress->getDesigner()->myGetWebsite() ?></a>
						</div>
					<?php
				}
				?>
	<?php } ?>
		</li>

		<?php if ($dress->getSellingPrice() > 0): ?>
		<li>
			<h4><?php echo __('price')?></h4>

			<div class="row">
				<div class="label"><?php echo __('selling_price') ?></div>

				<?php echo my_currency($dress->getSellingPrice(), $sf_user->getAttribute('actcur'), $dress->getSellingCurrencyTrait()->getName()) ?>
			</div>

			<?php if ($dress->getOriginalPrice() > 0): ?>
				<div class="row">
					<div class="label"><?php echo __('original_price') ?></div>

					<?php echo my_currency($dress->getOriginalPrice(), false, $dress->getOriginalCurrencyTrait()->getName()) ?>
				</div>
			<?php else: ?>
			<?php endif; ?>
			

			<div class="row">
				<div class="label"><?php echo __('price_negociable') ?></div>

				<?php echo $dress->getPriceNegociable() ? __('yes') : __('no') ?>
			</div>
			<?php if ('individual' == trim($dress->getUser()->getSellerType()->getName())): ?>
			<?php endif; ?>
		</li>
		<?php endif; ?>
		
		<li>
			<h4><?php echo __('terms_of_sale_descritption') ?></h4>

			<div class="row">
				<div class="label"><?php echo __('item_shipping_from') ?></div>

				<?php include_partial('dress/locations', array('locations' => $dress->myGetLocations())) ?>
			</div>

			<?php if ($dress->getSingleDetail('will_ship_to')): ?>
				<div class="row">
					<div class="label"><?php echo __('will_ship_to')?></div>

					<?php echo ($dress->getSingleDetail('will_ship_to')=='country') ? format_country($dress->getUser()->getCountry()) :  __($dress->getSingleDetail('will_ship_to')) ?>
				</div>
			<?php endif;?>

			<?php if ($dress->getSingleDetail('shipping_costs')):?>
				<div class="row">
					<div class="label"><?php echo __('shipping_costs') ?></div>

					<?php echo __($dress->getSingleDetail('shipping_costs'))?>
				</div>
			<?php endif;?>

			<?php if ($dress->getDescription($sf_user->getAttribute('actlang'),'terms')) : ?>
				<div class="row">
					<div class="label"><?php echo __("Terms of sale") ?></div>

					<?php echo $dress->getDescription($sf_user->getAttribute('actlang'),'terms')?>
				</div>
			<?php endif ?>

			<?php if ($dress->getDesigner()->getStars()) : ?>
			<div class="row vip">
				<div class="label"><?php echo __('VIP') ?></div>

				<?php echo __('Stars loving her dresses are: ') ?>
				<?php echo $dress->getDesigner()->getStars() ?>
			</div>
			<?php endif; ?>
		</li>
	</ul>
</div>