<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class dressActions extends sfActions {
	
  /*
  * Execute google maps
  */  
  public function executeGoogleMaps(sfWebRequest $request) {
    $this->actcountry=Doctrine_Core::getTable('Countries')->getCountry($this->getUser()->getAttribute('actcountry'));
    $dress = DressTable::getInstance()->find($this->getRequestParameter('id'));
    $user = $this->getUser();
    $this->forward404Unless($dress);
    if (!$dress->getIsActive()||!$dress->getIsVisible()){
        if ($user->getUserId()!=$dress->getUser()->getId()){
            $this->getUser()->setFlash('error', __('dress_not_found(not_active,not_visible,no_exist)'));
            $this->redirect('dress/searchResultsMap');
        }
    }
    $this->id = $request->getParameter('id', 0);
    if($this->id == 0){
		$this->loc_arr = array();
		$this->map = $_SESSION['map'];
		$_SESSION['map'] = array();
		foreach($this->map as $key=>$value)
		{
			$q = Doctrine_Query::create()
			->from('Location2Dress u')
			->where('u.dress_id = '.$value );
			$locations = $q->fetchArray();
			foreach($locations as $loc) $this->loc_arr[] = $loc['location_id'];
		}
		$q = Doctrine_Query::create()
		->from('DressLocation u')
		->whereIn('u.id', $this->loc_arr);

		$this->locations = $q->fetchArray();
    }else{
		$q = Doctrine_Query::create()
		->from('Location2Dress u')
		->leftJoin('u.DressLocation dl')
		->where('u.dress_id = '.$this->id )
		->andWhere('dl.show_on_map = ?', true)
		;

		$locations = $q->execute();
		$this->locations = array();
		$this->dress = $locations[0]->getDress();
      
			foreach ($locations as $location)
			{
				if ( $location->getDressLocation()->getLatitude()   == '0.000000' || $location->getDressLocation()->getLongtitude() == '0.000000'
				){
					$location->getDressLocation()->updateLatAndLng();
				}
				$this->locations[] = $location->getDressLocation();
			}
		}
	}
	
	/*
	* Execute Advanced Search 
	*/ 
	public function executeAdvancedSearch() {
		$this->getResponse()->setTitle(__('advanced_search_title',null,'metas'));
		$this->getResponse()->addMeta('keywords',__('advanced_search_keywords',null,'metas'));
		$this->getResponse()->addMeta('description',__('advanced_search_description',null,'metas'));
		$this->_prepareCriteria();
		if ($this->getRequestParameter('letters')) $this->criteria['designer_letters']=$this->getRequestParameter('letters');
		$q = DressTable::prepareSearchQuery($this->criteria);
		$this->designers = DesignerTable::getInstance()->getDesignersForDresses($q);
		$amount = $q->count();
		$list = array();
		if ($amount) {
			$list = $q->execute();
		}
		$this->amount = $amount;
		$this->dresses = $list;
		$this->actcountry=Doctrine_Core::getTable('Countries')->getCountry($this->getUser()->getAttribute('actcountry'));

		$this->your_location_widget = new sfWidgetFormI18nChoiceCountryOwn(array('culture' => $this->getUser()->getCulture(), 'add_countries' => sfConfig::get('app_register_default_countries'),  'add_empty' => __('choose', null, 'search')));
		$choices = array('' => sfContext::getInstance()->getI18N()->__('choose', null, 'dress_details'));
		$trimmings = array();
		foreach (DressTable::getInstance()->getUsedTrimmings() as $key => $value)
		{
			$trimmings[$key] = sfContext::getInstance()->getI18N()->__($value, null, 'traits');
		}
		$trimmings = $choices + $trimmings;
		$this->trimming_widget = new sfWidgetFormChoice(array(
		'choices'   => $trimmings
		));

		$fabrics = $choices + DressTable::getInstance()->getUsedFabrics();
		$this->fabric_widget = new sfWidgetFormChoice(array(
		'choices'   => $fabrics
		));

		if ($this->getRequest()->isXmlHttpRequest()) {
			$tmpResult = '';
			if ($amount > 0) {
			sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');
			$list = $q->execute();
			foreach ($list as $dress) {
			  $tmpResult .= get_partial('quick_preview_dress', array('dress' => $dress,'actcountry'=>$this->actcountry));
			}
			$list = $tmpResult;
			}
			return $this->renderText(json_encode(array('list' => $tmpResult, 'amount' => $amount)));
		} else if ($this->getRequest()->getMethod() == sfRequest::POST) {
			$this->redirect('dress/advancedSearchResults');
		}
	}
} 