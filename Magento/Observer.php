<?php
Class Webkul_Mpassignproduct_Model_Observer
{
	/**
	 * cartUpdate Cart update observer , used for managing the seller assign product stock
	 * @param  Object $observer Containes the cart data
	 * @var Object $db Data base connection object
	 * @var Object $cart_data Containes cart items
	 * @var Object $item Cart item object
	 * @var Object $info Conatines the updated cart item information
	 * @var Int $current_qty Updated item quote quantity
	 * @var Int $productId Updated item product id
	 * @var Int $mpassignproductId Updated item assign product id
	 * @var Array $temp Containes item options [description]
	 * @var Object $productDetail Assign product model object
	 * @var Float $price Assign product price
	 * @var Float $avlqty Assign product available qty
	 * @var Int $diff Difference between available qty and cart item quote quantity
	 * @var Int $seller Assign product vendot id
	 * @var Array $options Configurable assign product associated products option
	 * @var Int $adminAvlQty Admin product available qty
	 */
	public function cartUpdate($observer) {
		$db = Mage::getSingleton('core/resource')->getConnection('core_read');
		$cart_data =  Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems();
		$info = $observer->getInfo();
		foreach($cart_data as $item) {
			$current_qty = $info[$item->getId()]['qty'];
			$productId = $item->getProductId();
			$mpassignproductId = 0;
			foreach($item->getOptions() as $option) {
				$temp = unserialize($option['value']);
				if(isset($temp['mpassignproduct_id']))
					$mpassignproductId=$temp['mpassignproduct_id'];
			}
			

            if (Mage::helper('core')->isModuleOutputEnabled('Webkul_Mpbiddingaddons')) {
				$auction_product_collection=Mage::getModel('catalog/product')->load($productId);
	            $prodst = explode(',',$auction_product_collection->getAuctionProduct());
	            $prauctval = $auction_product_collection->getAttributeText('auction_product');
			}

            if(!(is_array($prauctval) || 'Auction'==$prauctval)){
				if($mpassignproductId) {
					$productDetail = Mage::getModel('mpassignproduct/mpassignproduct')->load($mpassignproductId);
					if($productDetail->getProductType() != "configurable") {
						$price = $productDetail->getPrice();
						$avlqty = $productDetail->getQty();
						$seller = $productDetail->getSellerId();

						$diff = $avlqty-$current_qty;
						if($diff > 0) {
							$item->setCustomPrice($price);
							$item->setOriginalCustomPrice($price);
							$item->getProduct()->setIsSuperMode(true);
						} else if($diff <= 0) {
							if($current_qty > $avlqty) {
								$item->setCustomPrice($price);
								$item->setOriginalCustomPrice($price);
								$item->getProduct()->setIsSuperMode(true);
								$item->setQty($avlqty);
								Mage::getSingleton('core/session')->addNotice('Number of quantity not available.');
							}
						}
					} else {
						$options = unserialize($productDetail->getConfigOptions());
						$id = Mage::getModel('catalog/product')->getIdBySku($item->getSku());
						$price = $options['products'][$id]['price'];
						$avlqty = $options['products'][$id]['qty'];
						$seller = $productDetail->getSellerId();
						$diff = $avlqty-$item->getQty();
						if($diff > 0) {
							$item->setCustomPrice($price);
							$item->setOriginalCustomPrice($price);
							$item->getProduct()->setIsSuperMode(true);
						} else if($diff <= 0) {
							if($item->getQty() > $avlqty) {
								$item->setCustomPrice($price);
								$item->setOriginalCustomPrice($price);
								$item->getProduct()->setIsSuperMode(true);
								$item->setQty($avlqty);
								Mage::getSingleton('core/session')->addNotice('Number of quantity not available.');
							}
						}
					}
				} else {
					$item->setCustomPrice($item->getProduct()->getFinalPrice());
					$item->setOriginalCustomPrice($item->getProduct()->getFinalPrice());
					$item->getProduct()->setIsSuperMode(true);

					$id = Mage::getModel('catalog/product')->getIdBySku($item->getSku());
					$_product = Mage::getModel('catalog/product')->load($productId);
					if($_product->getTypeId() != 'configurable') {
						$adminAvlQty = Mage::getModel('mpassignproduct/mpassignproduct')->getAssignProDetails($productId);
					} else {
						$adminAvlQty = Mage::getModel('mpassignproduct/mpassignproduct')->getConfigAssignProDetails($productId,$id);
					}
					if($item->getQty() >  $adminAvlQty['sellerqty'] && count($adminAvlQty)) {
						$item->setQty($adminAvlQty['sellerqty']);
						Mage::getSingleton('core/session')->addNotice('Number of quantity not available.');
					}
				}
			}
		}
	}
	
	/**
	 * productCart Cart product add observer , used for managing the seller assign product stock
	 * @param  Object $obs Containes the cart data
	 * @var Object $db Data base connection object
	 * @var Object $cart_data Containes cart items
	 * @var Object $item current product added Cart item object
	 * @var Object $info Conatines the updated cart item information
	 * @var Int $current_qty Updated item quote quantity
	 * @var Int $productId Updated item product id
	 * @var Int $mpassignproductId Updated item assign product id
	 * @var Array $temp Containes item options [description]
	 * @var Object $productDetail Assign product model object
	 * @var Float $price Assign product price
	 * @var Float $avlqty Assign product available qty
	 * @var Int $diff Difference between available qty and cart item quote quantity
	 * @var Int $seller Assign product vendot id
	 * @var Array $options Configurable assign product associated products option
	 * @var Int $adminAvlQty Admin product available qty
	 */
	public function productCart(Varien_Event_Observer $obs) {
		$walletProductId='';
		$cart_data =  Mage::getSingleton('checkout/session')->getQuote()->getAllVisibleItems();
		$item = $obs->getQuoteItem();

		$item = ( $item->getParentItem() ? $item->getParentItem() : $item );

		$db = Mage::getSingleton('core/resource')->getConnection('core_read');
		$productId = $item->getProductId();			
		foreach ($cart_data as $cart) { 
			if($cart->getProductId()==$productId){
				$quantity=$cart->getQty();
			}
		}
		$productId = $item->getProductId();
		$mpassignproductId = 0;
		foreach($item->getOptions() as $option) {
			$temp = unserialize($option['value']);
			if(isset($temp['mpassignproduct_id']))
				$mpassignproductId = $temp['mpassignproduct_id'];

		}
		if (Mage::helper('core')->isModuleOutputEnabled('Webkul_Mpbiddingaddons')) {
			$auction_product_collection=Mage::getModel('catalog/product')->load($productId);
            $prodst = explode(',',$auction_product_collection->getAuctionProduct());
            $prauctval = $auction_product_collection->getAttributeText('auction_product');
		}
		if (Mage::helper('core')->isModuleOutputEnabled('Webkul_WalletSystem')) {
			$walletProductId = Mage::helper('walletsystem')->getWalletProductId();
		}
		
        if($productId!=$walletProductId){
			if($mpassignproductId) {
				$productDetail = Mage::getModel('mpassignproduct/mpassignproduct')->load($mpassignproductId);
				if($productDetail->getProductType() != "configurable") {
					$price = $productDetail->getPrice();
					$avlqty = $productDetail->getQty();
					$seller = $productDetail->getSellerId();

					$diff = $avlqty-$quantity;
					if($diff > 0) {
						$item->setCustomPrice($price);
						$item->setOriginalCustomPrice($price);
						$item->getProduct()->setIsSuperMode(true);
					} else if($diff <= 0) {
						if($quantity == $avlqty) {
							$item->setCustomPrice($price);
							$item->setOriginalCustomPrice($price);
							$item->getProduct()->setIsSuperMode(true);
							$item->setQty($avlqty);
						} elseif($quantity > $avlqty) {
							$item->setCustomPrice($price);
							$item->setOriginalCustomPrice($price);
							$item->getProduct()->setIsSuperMode(true);
							$item->setQty($avlqty);
							Mage::getSingleton('core/session')->addNotice('Number of quantity not available.');
						}
					}
				} else {
					$options = unserialize($productDetail->getConfigOptions());
					$price = $options['products'][$obs->getQuoteItem()->getProductId()]['price'];
					$avlqty = $options['products'][$obs->getQuoteItem()->getProductId()]['qty'];
					$seller = $productDetail->getSellerId();
					$diff = $avlqty-$quantity;
					if($diff > 0) {
						$item->setCustomPrice($price);
						$item->setOriginalCustomPrice($price);
						$item->getProduct()->setIsSuperMode(true);
					} else if($diff <= 0) {
						if($quantity == $avlqty) {
							$item->setCustomPrice($price);
							$item->setOriginalCustomPrice($price);
							$item->getProduct()->setIsSuperMode(true);
							$item->setQty($avlqty);
						} elseif($quantity > $avlqty) {
							$item->setCustomPrice($price);
							$item->setOriginalCustomPrice($price);
							$item->getProduct()->setIsSuperMode(true);
							$item->setQty($avlqty);
							Mage::getSingleton('core/session')->addNotice('Number of quantity not available.');
						}
					}
				}
			} else {
				if(!(is_array($prauctval) || 'Auction'==$prauctval)){
					$id = Mage::getModel('catalog/product')->getIdBySku($item->getSku());
					$adminAvlQty = array();
					$_product = Mage::getModel('catalog/product')->load($productId);
					if($_product->getTypeId() != 'configurable') {
						$item->setCustomPrice($item->getProduct()->getFinalPrice());
						$item->setOriginalCustomPrice($item->getProduct()->getFinalPrice());
						$adminAvlQty = Mage::getModel('mpassignproduct/mpassignproduct')->getAssignProDetails($productId);
					} else {
						$item->setCustomPrice($item->getProduct()->getFinalPrice());
						$item->setOriginalCustomPrice($item->getProduct()->getFinalPrice());
						$item->getProduct()->setIsSuperMode(true);
						$adminAvlQty = Mage::getModel('mpassignproduct/mpassignproduct')->getConfigAssignProDetails($productId,$id);
					}
					if(isset($adminAvlQty['sellerqty'])){
						if($quantity >  $adminAvlQty['sellerqty'] && count($adminAvlQty)) {
							if($adminAvlQty['sellerqty'] == 0) {
								Mage::throwException("Number of quantity not available.");
								
							} else {
								$item->setQty($adminAvlQty['sellerqty']);
								Mage::getSingleton('core/session')->addNotice('Number of quantity not available.');
							}
						}
					}
				}
			}
		}
	}
	
	/**
	 * afterPlaceOrder1 After place order observer
	 * @param Object $observer Last placed order information
	 * @var Int $lastOrderId Last order id
	 * @var Object $order Sales Order model object
	 * @var Object $ordered_items Containes order all items
	 * @var Object $item Sales Order Item object
	 * @var Array $options Item options
	 * @var Int $productId Order item product id
	 * @var Int $mpassignproductId Order Item Assign product id
	 * @var Object $collection Marketplace Order sale list collection object
	 * @var Int $slqty Marketplace product ordered quantity
	 * @var Int $saleslistId Marketplace_Saleslist primary key
	 * @var Object $mpassignModel Assign product model object
	 * @var Int $asqt Assign product available quantity
	 * @var Array $options Configurable assign product associated products option
	 * @var Int $nasqt Assign product remaining quantity
	 */
	public function afterPlaceOrder1($observer) { 
		$lastOrderId = $observer->getOrder()->getId();
		$order = Mage::getModel('sales/order')->load($lastOrderId);
    	$ordered_items = $order->getAllVisibleItems();
    	
		$checkout = Mage::getSingleton('checkout/session')->getQuote();
		$billAddress = $checkout->getBillingAddress();
		$billCity = $billAddress->getCity(); 
		Mage::log('ordered items purchase are'.json_encode($ordered_items));
		Mage::log('city name of address is'.$billAddress->getCity());

		foreach ($ordered_items as $item) {
		    $orderedProductIds = $item->getData('product_id');
		}
		Mage::log("ordered product id is".$orderedProductIds);
		$users = Mage::getModel('mpassignproduct/mpassignproduct')->getCollection()->addFieldToFilter('product_id',array('eq'=>$orderedProductIds));
		Mage::log("marketplace product details are:-".json_encode($users));
		$sellerid = Array();
	    foreach ($users as $value) 
	    {
	        $sellerid[] = $value->getSellerId();
	    }		
	    Mage::log("marketplace seller ids are:".json_encode($sellerid));
		$city = Array();
		$customer_data = Array();
		$address = Array();
		$userlist = Array();

		foreach($sellerid as $id)
		{
			$customer_data[] = Mage::getModel('customer/customer')->load($id);
			$address[] = Mage::getModel('customer/address')->load($id)->getData();
			$userlist[]= Mage::getModel('marketplace/userprofile')->getCollection()->addFieldToFilter('mageuserid',array('eq'=>$id));

		}

		foreach($userlist as $user)
		{
			$data[] = $user->getData();
		}
		//print_r($data[0][0]['complocality']);die;
		if(isset($data))
		{
			foreach($data as $user)
			{
				$city[] = $user[0]['complocality'];
			}
			//print_r($location);die;

			if(isset($city))
			{
				$length = count($city);
			}

			//$city = "Adelanto";
	       // $address = $dlocation; // Google HQ
	        $prepAddr = str_replace(' ','+',$billCity);
	        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
	        Mage::log("contents are".json_encode($geocode));
	        $output= json_decode($geocode);
	        if(isset($output->results[0]))
	        {
		        $latitud = $output->results[0]->geometry->location->lat;
		        $longitud = $output->results[0]->geometry->location->lng;
		        $latitude = Array();
		        $longitude = Array();
		        $miles = Array();
		        $dist = Array();
				for($i=0;$i<$length;$i++)
				{
			       // $address = $dlocation; // Google HQ
			        $prepAddr = str_replace(' ','+',$city[$i]);
			        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
			        $output= json_decode($geocode);
			        $latitude[$i] = $output->results[0]->geometry->location->lat;
			        $longitude[$i] = $output->results[0]->geometry->location->lng;			
				}

				for($j=0;$j<$length;$j++)
				{
					$theta = $longitude[$j] - $longitud;
					$dist[$j] = sin(deg2rad($latitude[$j])) * sin(deg2rad($latitud)) +  cos(deg2rad($latitude[$j])) * cos(deg2rad($latitud)) * cos(deg2rad($theta));
					$dist[$j] = acos($dist[$j]);
					$dist[$j] = rad2deg($dist[$j]);
					$miles[$j] = $dist[$j] * 60 * 1.1515;			
				}
				//Mage::log("distance are".$miles);
				Mage::log("distance are".json_encode($miles));
				//print_r($miles);die;

				for ($i=0; $i<count($miles); $i++)
				{
					if ($miles[$i] == min($miles)) 
					{
			        	$min = $miles[$i];
			        	$id = $sellerid[$i];
			    	}
				}
				if(isset($id))
				{
					$sellerId = $id;
					Mage::log("seller id is:".$sellerId);
			        $val = Mage::getModel('mpassignproduct/mpassignproduct')->getCollection()->addFieldToFilter('seller_id',array('eq'=>$sellerId));
			        $mpasproductId = $val->getData()[0]['mpassignproduct_id'];
			        Mage::log("mp assign product id is".$mpasproductId);		
				}
	        }
	        else
	        {
	        	Mage::throwException('you have entered invalid city address');
	        }			
		}

		//$sellerId = 2;
		
		//Mage::log("sellerid is".$id);


    	foreach ($ordered_items as $item) {
    		$options = $item->getProductOptions();
    		$productId = $item->getProductId();
    		$mpassignproductId = 0;
    			if(isset($mpasproductId))
    			{
    				$mpassignproductId = $mpasproductId;
    				Mage::log("inside observer nearest vendor mpassignproduct_id is".$mpassignproductId);
    			}
    			else if(isset($options['info_buyRequest']['mpassignproduct_id']))
    			{
    				$mpassignproductId = $options['info_buyRequest']['mpassignproduct_id'];
    				Mage::log("inside simple one mpassignproductId is".$mpassignproductId);
    			}
    			Mage::log("mpassignproduct_id is".$mpassignproductId);
    		if($mpassignproductId) {
    			$collection = Mage::getModel('marketplace/saleslist')->getCollection()
				    			->addFieldToFilter('mageproid',array('eq'=>$productId))
				    			->addFieldToFilter('mageorderid',array('eq'=>$lastOrderId));
    			foreach($collection as $val) {
				    $slqty = $val['magequantity'];
				    $saleslistId = $val['autoid'];
				}
				$mpassignModel = Mage::getModel('mpassignproduct/mpassignproduct')->load($mpassignproductId);

				if($saleslistId) {
					$saleslistModel = Mage::getModel('marketplace/saleslist')->load($saleslistId);
					$saleslistModel->setMageproownerid($mpassignModel->getSellerId());
					$saleslistModel->setMpassignproductId($mpassignproductId);
					$saleslistModel->save();

					$mpOrderModel = Mage::getModel('marketplace/order')
											->getCollection()
											->addFieldToFilter('order_id',$lastOrderId);

					foreach ($mpOrderModel as $mp_order_value) {
						$item_ids = explode(",", $mp_order_value->getItemIds());
						if(in_array($productId,$item_ids))
							$mp_order_id = $mp_order_value->getId();
					}

					if($mp_order_id){
						Mage::getModel('marketplace/order')
								->load($mp_order_id)
								->setItemId($mpassignproductId)
								->setSellerId($mpassignModel->getSellerId())
								->save();
					}

					if($mpassignModel->getProductType() != "configurable") {
						$asqt = $mpassignModel->getQty();
						$nasqt = $asqt-$slqty;
						$mpassignModel->setQty($nasqt);
						$mpassignModel->save();
					} else {
						$options = unserialize($mpassignModel->getConfigOptions());
						$id = Mage::getModel('catalog/product')->getIdBySku($item->getSku());
						$avlqty = $options['products'][$id]['qty'];

						$nasqt = $avlqty-$slqty;
						$options['products'][$id]['qty'] = 	$nasqt;					
						$mpassignModel->setConfigOptions(serialize($options));
						$mpassignModel->save();
					}
				} 
			}
    	}
	}
	
	/**
	 * deleteProductCheck If seller delete their product then product will assign to another seller
	 * @param Object $observer Deleting product information
	 * @var Int $productId eleting product id
	 * @var Object $collection Assign product collection object
	 * @var Object $collectionpro Marketplace Collection object
	 * @var Array $data Assign product details
	 * @var Object $stockItem  catalog Product stock item
	 * @var Object $_product catalog product model object
	 * @var Object $savedStock saved catalog Product stock item
	 */
	public function deleteProductCheck($observer) {
		$productId = $observer->getId();
		$collection = Mage::getModel('mpassignproduct/mpassignproduct')
						->getCollection()
						->addFieldToFilter('product_id',array('eq'=>$productId))
						->addFieldToFilter('qty',array('gt'=>0));
		$collection->setOrder("price","ASC");
		if(count($collection) > 0) {
			foreach($collection as $assignproduct) {
				$collectionpro = Mage::getModel('marketplace/product')->getCollection()
						->addFieldToFilter('mageproductid',array('eq'=>$productId));
				foreach($collectionpro as $row) {
					$row->setUserid($assignproduct->getSellerId());
					$row->save();
					$data = Mage::getModel('mpassignproduct/mpassignproduct')->getAssignProDetails($productId);
					$_product = Mage::getModel('catalog/product')->load($productId);
					$_product->setPrice($assignproduct->getPrice());
					$_product->save();

					$stockItem = Mage::getModel('cataloginventory/stock_item')->loadByProduct($productId);
					$stockItem->setProductId($productId)->setStockId(1);
					$savedStock = $stockItem->save();
					$stockItem->load($savedStock->getId())->setQty($data['assignqty'])->save();
					$stockItem->setData('is_in_stock', 1); 
					$savedStock = $stockItem->save();
				}
				Mage::getSingleton('customer/session')->setIsAssing($assignproduct->getMpassignproductId());
				$assignproduct->delete();
				break;
			}
		} else {
			Mage::getSingleton('customer/session')->setIsAssing(0);
		}
	}

	/**
	 * DeleteProduct If admin delete base product then delete all assign products
	 * @param Object $observer Deleted product information
	 * @var Object $collection Assign product collection object
	 */
	public function DeleteProduct($observer) {
		$collection = Mage::getModel('mpassignproduct/mpassignproduct')->getCollection()
								->addFieldToFilter('product_id',$observer->getProduct()->getId());
		foreach($collection as $data){			
			Mage::getModel('mpassignproduct/mpassignproduct')->load($data['mpassignproduct_id'])->delete();			
		}		
	}
}
