<?php

require_once 'Mage/Checkout/controllers/OnepageController.php';

class Toov_Checkout_OnepageController extends Mage_Checkout_OnepageController
{
    /**
     * Save checkout billing address
     */
    public function saveBillingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('billing', array());
            $data["street"][0] = "Area/Street Name: ".$data["street"][0]."  ";
            $data["street"][1] = "Villa/Building Number: ".$data["street"][1]."  ";
            $data["street"][2] = "Landmark: ".$data["street"][2]."  ";
            $customerAddressId = $this->getRequest()->getPost('billing_address_id', false);

            if (!isset($data['postcode'])) {
                $data['postcode'] = "";
            }

            $billingAddress = $this->getOnepage()->getQuote()->getBillingAddress();
            $billingAddress->setData('phone_is_verified', false);
            $result = $this->getOnepage()->saveBilling($data, $customerAddressId);

            if (!isset($result['error'])) {
                if ($this->getOnepage()->getQuote()->isVirtual()) {
                    $result['goto_section'] = 'payment';
                    $result['update_section'] = array(
                        'name' => 'payment-method',
                        'html' => $this->_getPaymentMethodsHtml()
                    );
                    Mage::getSingleton('checkout/session')->setActivestep('payment');
                    $result['totals_html'] = $this->_getTotalsHtml();
                } else {
                    $result = $this->getOnepage()->selectShippingMethod();
                    if (!$result) {
                        $this->getOnepage()->getQuote()->setTotalsCollectedFlag(false)->collectTotals();
                        $this->getOnepage()->initPayment();

                        $result['goto_section'] = 'payment';
                        $result['update_section'] = array(
                            'name' => 'payment-method',
                            'html' => $this->_getPaymentMethodsHtml()
                        );

                        Mage::getSingleton('checkout/session')->setActivestep('payment');
                        $result['totals_html'] = $this->_getTotalsHtml();
                    }
                    $this->getOnepage()->getQuote()->collectTotals()->save();
                }
            }

            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Shipping address save action
     */
    public function saveShippingAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if ($this->getRequest()->isPost()) {
            $data = $this->getRequest()->getPost('shipping', array());
            $data["street"][0] = "Area/Street Name: ".$data["street"][0]."  ";
            $data["street"][1] = "Villa/Building Number: ".$data["street"][1]."  ";
            $data["street"][2] = "Landmark: ".$data["street"][2]."  ";
            // print_r($data);die;
            $customerAddressId = $this->getRequest()->getPost('shipping_address_id', false);

            if (isset($data['email'])) {
                $data['email'] = trim($data['email']);
            }

            if (!isset($data['postcode'])) {
                $data['postcode'] = "";
            }

            $shippingAddress = $this->getOnepage()->getQuote()->getShippingAddress();
            $shippingAddress->setData('phone_is_verified', false);
            $result = $this->getOnepage()->saveShipping($data, $customerAddressId);

            if (!isset($result['error'])) {
                $defaultCountries = Mage::helper('toov_checkout')->getDefaultCountriesData();
                $shippingCountryId = $shippingAddress->getCountryId();
                if (isset($defaultCountries[$shippingCountryId])) {
                    $targetWebsiteCode = $defaultCountries[$shippingCountryId];

                    if ($targetWebsiteCode != Mage::app()->getWebsite()->getCode()) {
                        $targetWebsite = Mage::getModel('core/website')->load($targetWebsiteCode);
                        $targetStoreCode = Mage::helper('toov_navigation')->getStoreWithSameLocale($targetWebsite)->getCode();
                        $url = Mage::app()->getStore($targetStoreCode)->getUrl('checkout/onepage');
                        $result['redirect'] = $url;
                    }
                }
                if (isset($data['use_for_billing']) && $data['use_for_billing'] == 1) {
                    Mage::getSingleton('checkout/session')->setActivestep('payment');
                    if (!isset($result['redirect'])) {
                        $result = $this->getOnepage()->selectShippingMethod();
                        if (!$result) {
                            $this->getOnepage()->getQuote()->setTotalsCollectedFlag(false)->collectTotals();
                            $this->getOnepage()->initPayment();

                            $result['goto_section'] = 'payment';
                            $result['update_section'] = array(
                                'name' => 'payment-method',
                                'html' => $this->_getPaymentMethodsHtml()
                            );

                            $result['totals_html'] = $this->_getTotalsHtml();
                        }
                        $this->getOnepage()->getQuote()->collectTotals()->save();
                    }
                } else {
                    Mage::getSingleton('checkout/session')->setActivestep('billing');
                    if (!isset($result['redirect'])) {
                        $result = $this->getOnepage()->selectShippingMethod();
                        if (!$result) {
                            $result['goto_section'] = 'billing';
                        }
                    }
                }
            }
            $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
        }
    }

    /**
     * Save payment ajax action
     *
     * Sets either redirect or a JSON response
     */
    public function savePaymentAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        $result = array();
        try {
            if (!$this->getRequest()->isPost()) {
                $this->_ajaxRedirectResponse();
                return;
            }

            $data = $this->getRequest()->getPost('payment', array());
            $result = $this->getOnepage()->savePayment($data);

            // get section and redirect data
            $redirectUrl = $this->getOnepage()->getQuote()->getPayment()->getCheckoutRedirectUrl();
            if (empty($result['error']) && !$redirectUrl) {
                $result['totals_html'] = $this->_getTotalsHtml();
            }
            if ($redirectUrl) {
                $result['redirect'] = $redirectUrl;
            }
        } catch (Mage_Payment_Exception $e) {
            if ($e->getFields()) {
                $result['fields'] = $e->getFields();
            }
            $result['error'] = $e->getMessage();
        } catch (Mage_Core_Exception $e) {
            $result['error'] = $e->getMessage();
        } catch (Exception $e) {
            Mage::logException($e);
            $result['error'] = $this->__('Unable to set Payment Method.');
        }
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Get totals (order review) HTML
     *
     * @return string
     */
    protected function _getTotalsHtml()
    {
        $html = $this->getLayout()->createBlock('checkout/cart_totals')
            ->setTemplate('checkout/onepage/review/totals.phtml')
            ->toHtml();
        return $html;
    }

    /**
     * Save coupon ajax action
     */
    public function applyCouponAction()
    {
        if ($this->_expireAjax()) {
            return;
        }
        if (!$this->getRequest()->isPost()) {
            $this->_ajaxRedirectResponse();
            return;
        }
        if (!$this->getOnepage()->getQuote()->getItemsCount()) {
            //$this->_goBack();
            return;
        }
        $couponCode = (string) $this->getRequest()->getParam('coupon_code');
        if ($this->getRequest()->getParam('remove') == 1) {
            $couponCode = '';
        }

        $result = array();
        $result['status'] = 'ERROR';
        $result['msg'] = $this->__('Cannot apply the coupon code.');
        try {
            $this->getOnepage()->getQuote()->getShippingAddress()->setCollectShippingRates(true);
            $this->getOnepage()->getQuote()->setCouponCode(strlen($couponCode) ? $couponCode : '')
                ->collectTotals()
                ->save();

            if (strlen($couponCode)) {
                if ($couponCode == $this->getOnepage()->getQuote()->getCouponCode()) {
                    $result['status'] = 'SUCCESS';
                    $result['msg'] = $this->__('Coupon code "%s" was applied.',
                        Mage::helper('core')->htmlEscape($couponCode));

                } else {
                    $result['status'] = 'ERROR';
                    $result['msg'] = $this->__('Coupon code "%s" is not valid.',
                        Mage::helper('core')->htmlEscape($couponCode));
                }
            } else {
                $result['status'] = 'SUCCESS';
                $result['msg'] = $this->__('Coupon code was canceled.');
            }

        } catch (Mage_Core_Exception $e) {

        } catch (Exception $e) {

        }
        $result['totals_html'] = $this->_getTotalsHtml();
        $this->getResponse()->setBody(Mage::helper('core')->jsonEncode($result));
    }

    /**
     * Get one page checkout model
     *
     * @return Toov_Checkout_Model_Checkout_Type_Onepage
     */
    public function getOnepage()
    {
        return Mage::getSingleton('checkout/type_onepage');
    }

    /**
     * Checkout page
     */
    public function indexAction()
    {
        if (!Mage::helper('checkout')->canOnepageCheckout()) {
            Mage::getSingleton('checkout/session')->addError($this->__('The onepage checkout is disabled.'));
            $this->_redirect('checkout/cart');
            return;
        }
        $quote = $this->getOnepage()->getQuote();
        if (!$quote->hasItems() || $quote->getHasError()) {
            $this->_redirect('checkout/cart');
            return;
        }
        if (!$quote->validateMinimumAmount()) {
            $error = Mage::getStoreConfig('sales/minimum_order/error_message') ?
                Mage::getStoreConfig('sales/minimum_order/error_message') :
                Mage::helper('checkout')->__('Subtotal must exceed minimum order amount');

            Mage::getSingleton('checkout/session')->addError($error);
            $this->_redirect('checkout/cart');
            return;
        }
        Mage::getSingleton('checkout/session')->setCartWasUpdated(false);
        Mage::getSingleton('customer/session')->setBeforeAuthUrl(Mage::getUrl('*/*/*', array('_secure' => true)));
        try {
            $this->getOnepage()->initCheckout();
        } catch (Exception $e) {

        }

        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->getLayout()->getBlock('head')->setTitle($this->__('Checkout'));
        $this->renderLayout();
    }

    /**
     * Create order action
     */
    public function saveOrderAction()
    {
        
        parent::saveOrderAction();
        $checkout = Mage::getSingleton('checkout/session');
        $checkout->setActivestep('');
        if (is_array($checkout->getStepData())) {
            foreach ($checkout->getStepData() as $step => $data) {
                $checkout->setStepData($step, 'complete', false);
            }
        }
    }
}