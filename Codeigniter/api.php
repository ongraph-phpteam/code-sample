<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Api extends CI_Model{
    function __construct() {
    }
	/*
	 * insertTickerData()
	 *
	 * insert Ticker feed data in Database
	 *
	 * @param (array) $data array of ticker data
	 * @return (void)
	 */
	public function insertTickerData($data){
		$this->db->insert('ticker', $data);
	}
	/*
	 * insertExchangeData()
	 *
	 * insert Exchange feed data in Database
	 *
	 * @param (array) $data array of exchange data
	 * @return (void)
	 */
	public function insertExchangeData($data){
		$this->db->insert('rateexchange', $data);
	}
	/*
	 * fetchTicker()
	 *
	 * Fetch Ticker data from database 
	 *
	 * @param (string) $key unique identifier key for each feed
	 * @return (array) Query result
	 */
	public function fetchTicker($key){
		$this->db->select('*');
		$this->db->from('ticker');
		$this->db->where('identifiers', $key); 
		$this->db->limit(1);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get();
		if($query->num_rows())
			return $query->row();
		else
			return array('identifiers'=>$key, 'price'=>0);
	}
	/*
	 * fetchCurrency()
	 *
	 * Fetch Exchange rate data from database
	 *
	 * @param (string) $key unique identifier key for each feed
	 * @return (array) Query result
	 */
	public function fetchCurrency($key){
		$this->db->select('*');
		$this->db->from('rateexchange');
		$this->db->where('identifiers', $key); 
		$this->db->limit(1);
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get(); 
		if($query->num_rows())
			return $query->row();
		else
			return array('identifiers'=>$key, 'price'=>0);
	}
}
