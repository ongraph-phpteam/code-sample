<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Apis extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model('api');//Load DB model
	}
	public function index()
	{	//Load ticker result disply
		$this->load->view('index');
	}
	/*
	 * ticker()
	 *
	 * Load all ticker data from model and for different feeds
	 *
	 * @param void
	 * @return (jason)
	 */
	public function ticker(){
		$PriceFeedData = array();
		$PriceFeedData['btc_usd']['blockchainbtcusd'] = $this->api->fetchTicker('blockchainbtcusd');	//load blockchain BTC to USD data from model
		$PriceFeedData['btc_usd']['coindeskbtcusd'] = $this->api->fetchTicker('coindeskbtcusd');	//load coindesk BTC to USD data from model
		$PriceFeedData['btc_usd']['hitbtcbtcusd'] = $this->api->fetchTicker('hitbtcbtcusd');	//load hitbtc BTC to USD data from model
		$PriceFeedData['btc_eur']['blockchainbtceur'] = $this->api->fetchTicker('blockchainbtceur');	//load blockchain BTC to EUR data from model
		$PriceFeedData['btc_eur']['coindeskbtceur'] = $this->api->fetchTicker('coindeskbtceur');	//load coindesk BTC to EUR data from model
		$PriceFeedData['btc_eur']['hitbtcbtceur'] = $this->api->fetchTicker('hitbtcbtceur');	//load hitbtc BTC to EUR data from model
		$PriceFeedData['eur_usd']['fixereurusd'] = $this->api->fetchCurrency('fixereurusd');	//load fixer EUR to USD data from model
		$PriceFeedData['eur_usd']['bitstampeurusd'] = $this->api->fetchCurrency('bitstampeurusd');	//load bitstamp EUR to USD data from model
		$PriceFeedData['eur_usd']['appspoteurusd'] = $this->api->fetchCurrency('appspoteurusd');	//load appspot EUR to USD data from model
		$result = new stdClass();
		$result->success = TRUE;
		$result->data = $PriceFeedData;
		echo json_encode($result); //Return jason result
		exit;
	}
	/*
	 * gettickersfromblockchain()
	 *
	 * Call Blockchain bicoin feed API and pass latest price valuve to model
	 * Call Bitstamp exchange rate feed API and pass latest rate valuve to model
	 */
	public function gettickersfromblockchain(){
		$this->getDatafromTickersFeed('https://blockchain.info/ticker', 'blockchain');
		$this->getDatafromExchangeFeed('https://www.bitstamp.net/api/v2/ticker/eurusd', 'bitstampeurusd');
	}
	/*
	 * gettickersfromcoindesk()
	 *
	 * Call Coindesk bicoin feed API and pass latest price valuve to model
	 * Call Fixer exchange rate feed API and pass latest rate valuve to model
	 */
	public function gettickersfromcoindesk(){
		$this->getDatafromTickersFeed('http://api.coindesk.com/v1/bpi/currentprice.json', 'coindesk');
		$this->getDatafromExchangeFeed('http://api.fixer.io/latest?symbols=USD', 'fixereurusd');
	}
	/*
	 * gettickersfromhitbtc()
	 *
	 * Call Hitbtc bicoin feed API and pass latest price valuve to model
	 * Call Appspot exchange rate feed API and pass latest rate valuve to model
	 */
	public function gettickersfromhitbtc(){
		$this->getDatafromTickersFeed('http://api.hitbtc.com/api/1/public/ticker', 'hitbtc');
		$this->getDatafromExchangeFeed('http://rate-exchange-1.appspot.com/currency?from=EUR&to=USD', 'appspoteurusd');
	}
	/*
	 * getDatafromExchangeFeed()
	 *
	 * Make cURL request for Exchange rate feed URI and pass result to model
	 *
	 * @param (string) $feedUrl URL of rate Exchange API
	 * @param (string) $identifiers Unigue identifiers key for each feed API
	 * @return (void)
	 */
	function getDatafromExchangeFeed($feedUrl, $identifiers){
		$curlResult = $this->getTickerDataCurl($feedUrl);
		$PriceFeedData = array('identifiers'=>$identifiers, 'price'=>'');
		if($curlResult){
			$PriceFeedData  = $this->parseResult($identifiers , $curlResult);
		}
		//Transfering data to Model
		$this->api->insertExchangeData($PriceFeedData);
	}
	/*
	 * getDatafromTickersFeed()
	 *
	 * Make cURL request for BiTcoint ticker feed URI and pass result to model
	 *
	 * @param (string) $feedUrl URL of rate Bitcoin ticker API
	 * @param (string) $identifiers Unigue identifiers key for each feed API
	 * @return (void)
	 */
	function getDatafromTickersFeed($feedUrl, $identifiers)
	{
		$curlResult = $this->getTickerDataCurl($feedUrl);
		
		$PriceFeedData = array('identifiers'=>$identifiers, 'price'=>'');
		if($curlResult){
			$PriceFeedData  = $this->tickersParser($identifiers , json_decode($curlResult));
		}
		//Loop through USD and EUR feed result
		foreach($PriceFeedData as $feed){
			//Transfering data to Model
			$this->api->insertTickerData($feed);
		}
		
	}
}	